const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

const { platform } = require( "../../models/products/models" );

module.exports = {
  "index": async ( req, res ) => {
    const findPlatform = await platform.findAll( {
      "attributes": [ "id", "name", "status", "created_at" ],
      "where": {
        "status": {
          "$ne": 0
        }
      }
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": findPlatform } );
  }
};
