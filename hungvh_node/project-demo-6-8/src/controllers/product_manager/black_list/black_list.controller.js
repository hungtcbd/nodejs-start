const { BlackListRepository } = require( "../../../models/product_manager/black_list/black_list.model" );
const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );

class BlackListController {
  constructor() {
        
  }

  async list( request, response ) {
    let blackLists, data, isExport = false, pagination, page = 1, limit = 10, total = 0,
      blackListRepository = new BlackListRepository();

    data = RequestHelper.only( request, [ "type", "username", "userId", "ip" ], false );
    try {
      blackLists = await blackListRepository.list( data, [], true );
      limit = RequestHelper.input( request, "perPage", 10 );
      page = RequestHelper.input( request, "page", 1 );
      isExport = RequestHelper.input( request, "export", "false" );
      total = blackLists.length;

      if ( isExport === "false" ) {
        const offset = ( page - 1 ) * limit,
          endOffset = parseInt( offset ) + parseInt( limit );

        blackLists = blackLists.slice( offset, endOffset );
      }
      pagination = {
        "total": total,
        "perPage": limit,
        "page": page
      };
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "blackList": blackLists,
          "pagination": pagination
        }
      } );
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
    }
  }

  async create( request, response ) {
    let data, blackList, rules,
      blackListRepository = new BlackListRepository(),
      validator = new Validator();
    
    data = RequestHelper.only( request, [
      "username",
      "ip",
      "userId",
      "reason",
      "expiredTime"
    ], true );
    rules = {
      "username": [ "requiredIfNotHave:ip", "exist:users,username" ],
      "ip": [ "requiredIfNotHave:username" ],
      "reason": [ "required" ]
    };
    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      let filter = {};

      if ( data.username ) {
        filter.username = data.username;
      } else {
        filter.ip = data.ip;
      }
      blackList = await blackListRepository.list( filter );
      if ( blackList.length > 0 ) {
        blackList = blackList[ 0 ];
        let updateData = data;

        if ( !data.expiredTime ) {
          updateData.expiredTime = ( new Date() ).getTime() + 10 * 24 * 365 * 3600000;
        }
        await blackListRepository.update( blackList, updateData );
        await blackList.reload();
        response.status( 200 ).json( {
          "CODE": ErrorCode.SUCCESS,
          "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
          "DATA": {
            "blackList": blackList
          }
        } );
        return;
      }
      blackList = await blackListRepository.create( data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "blackList": blackList
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
      return;
    }
  }

  async update( request, response ) {
    let data, blackList, rules = {}, blackListId,
      blackListRepository = new BlackListRepository(),
      validator = new Validator();
  
    blackListId = RequestHelper.param( request, "blackListId" );
    data = RequestHelper.only( request, [
      "username",
      "ip",
      "userId",
      "reason"
    ], true );
    rules = {
    };

    blackList = await blackListRepository.getBlackListById( blackListId );

    if ( !blackList ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "blacklist not found"
        }
      } );
      return;
    }

    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      blackList = await blackListRepository.update( blackList, data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "blackList": blackList
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

  async show( request, response ) {
    let blackListId, blackList, blackListRepository = new BlackListRepository();

    blackListId = RequestHelper.param( request, "blackListId" );
    blackList = await blackListRepository.getBlackListById( blackListId );
    if ( !blackList ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "blackList": blackList
        }
      } );
      return;
    }
    
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "blackList": blackList
      }
    } );
    return;
  }

  async delete( request, response ) {
    let blackList, blackListId, blackListRepository = new BlackListRepository();

    blackListId = RequestHelper.param( request, "blackListId" );
    blackList = await blackListRepository.getBlackListById( blackListId );
    if ( !blackList ) {
      response.status( 404 ).end();
      return;
    }
    await blackListRepository.delete( blackList );

    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS
    } );
    return;
  }
}

module.exports = new BlackListController();
