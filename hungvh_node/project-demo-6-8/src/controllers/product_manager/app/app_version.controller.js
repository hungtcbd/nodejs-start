const { AppRepository } = require( "../../../models/product_manager/app/app/app.model" );
const { AppVersionRepository } = require( "../../../models/product_manager/app/app_version/app_version.model" );

const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );

class AppVersionController {
  constructor() {
        
  }

  async list( request, response ) {
    let apps, pagination, isExport = 0, page = 1, limit = 10,
      appId, data, appVersionRepository = new AppVersionRepository(),
      app, appRepository = new AppRepository();

    data = RequestHelper.only( request, [ "name", "status" ], false );
    appId = RequestHelper.param( request, "appId" );
    app = await appRepository.getAppById( appId );
    if ( app ) {
      data.appId = appId;
    } else {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "app not found"
        }
      } );
      return;
    }
   
    try {
      apps = await appVersionRepository.list( data, [], true );
      limit = RequestHelper.input( request, "perPage", 10 );
      page = RequestHelper.input( request, "page", 1 );
      isExport = RequestHelper.input( request, "export", "false" );
      pagination = {
        "total": apps.length,
        "perPage": limit,
        "page": page
      };
      if ( isExport === "false" ) {
        const offset = ( page - 1 ) * limit,
          endOffset = parseInt( offset ) + parseInt( limit );

        apps = apps.slice( offset, endOffset );
      }
     
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "appVersions": apps,
          "pagination": pagination
        }
      } );
    } catch ( e ) {
      console.log( e );
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
    }
  }

  async create( request, response ) {
    let data, appId, app, rules,
      appVersionRepository = new AppVersionRepository(),
      validator = new Validator();

    appId = RequestHelper.param( request, "appId" );
    data = RequestHelper.only( request, [
      "name",
      "version",
      "description",
      "changeLog",
      "file"
    ], true );
    rules = {
      "name": [ "required", "notExist:app_version,name" ],
      "version": [ "required" ],
      "file": [ "required", "json" ]
    };
    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    app = await appVersionRepository.list( {
      "appId": appId,
      "version": data.version
    } );

    if ( app.length > 0 ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": {
            "version": `${data.version} existed`
          }
        }
      } );
      return;
    }
    try {
      data.appId = appId;
      app = await appVersionRepository.create( data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "app": app
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
      return;
    }
  }

  async update( request, response ) {
    let data, app, appVersion, rules = {}, appId, appVersionId,
      appVersionRepository = new AppVersionRepository(),
      appRepository = new AppRepository(),
      validator = new Validator();
  
    appId = RequestHelper.param( request, "appId" );
    appVersionId = RequestHelper.param( request, "appVersionId" );
    data = RequestHelper.only( request, [
      "name",
      "version",
      "description",
      "changeLog",
      "file"
    ], false );
    rules = {
      "name": [ ],
      "version": [ ],
      "file": [ ]
    };
    app = await appRepository.getAppById( appId );
    appVersion = await appVersionRepository.getAppVersionById( appId, appVersionId );
    if ( !app || !appVersion ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "app or app version not exist"
        }
      } );
      return;
    }

    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    // eslint-disable-next-line one-var
    let filter = {
      "id": {
        "$ne": parseInt( appVersionId )
      },
      "status": 1
    };

    if ( data.name && data.name !== appVersion.name ) {
      if ( data.version && data.version !== appVersion.version ) {
        filter.$or = {
          "name": data.name,
          "version": data.version
        };
      } else {
        filter.name = data.name;
      }
      const existVersions = await appVersionRepository.list( filter );

      if ( existVersions.length > 0 ) {
        response.status( 200 ).json( {
          "CODE": ErrorCode.INVALID_PARAM,
          "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
          "DATA": {
            "error": "existed"
          }
        } );
        return;
      }
    }
   
    try {
      appVersion = await appVersionRepository.update( appVersion, data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "appVersion": appVersion
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

  async show( request, response ) {
    let appId, app,
      appVersion,
      appVersionId,
      appRepository = new AppRepository(),
      appVersionRepository = new AppVersionRepository();

    appId = RequestHelper.param( request, "appId" );
    appVersionId = RequestHelper.param( request, "appVersionId" );
    app = appRepository.getAppById( appId );
    appVersion = appVersionRepository.getAppVersionById( appId, appVersionId );
    if ( !app || !appVersion ) {
      response.status( 404 );
      return;
    }
    
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "appVersion": appVersion
      }
    } );
    return;
  }

  async status( request, response ) {
    let app, appId, status, appVersion, appVersionId,
      appVersionRepository = new AppVersionRepository(),
      appRepository = new AppRepository();

    appId = RequestHelper.param( request, "appId" );
    appVersionId = RequestHelper.param( request, "appVersionId" );
    app = await appRepository.getAppById( appId );
    appVersion = await appVersionRepository.getAppVersionById( appId, appVersionId );
    if ( !app || !appVersion ) {
      response.status( 404 );
      return;
    }
    status = RequestHelper.input( request, "status" );
    if ( !status ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": {
            "status": "status is required"
          }
        }
      } );
      return;
    }
    
    try {
      appVersion = await appVersionRepository.update( appVersion, { "status": status } );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "appVersion": appVersion
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

  async delete( request, response ) {
    let app, appId, appVersion, appVersionId,
      appVersionRepository = new AppVersionRepository(),
      appRepository = new AppRepository();

   
    appId = RequestHelper.param( request, "appId" );
    appVersionId = RequestHelper.param( request, "appVersionId" );
    app = await appRepository.getAppById( appId );
    appVersion = await appVersionRepository.getAppVersionById( appId, appVersionId );
    if ( !app || !appVersion ) {
      response.status( 404 );
      return;
    }
    
    await appVersionRepository.delete( appVersion );

    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS
    } );
    return;
  }
}

module.exports = new AppVersionController();
