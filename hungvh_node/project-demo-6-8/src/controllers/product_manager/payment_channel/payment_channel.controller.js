const { payment_channel } = require( "../../../models/products/models" );
const code = require( "../../../configs/ErrorCode" );
const msg = require( "../../../configs/ErrorMessage" );
const logger = require( "../../../helpers/services/log.service" );

module.exports = {
  "index": async ( req, res ) => {
    const findPaymentChannel = await payment_channel.findAll();

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": findPaymentChannel } );
  },
  "create": async ( req, res ) => {
    const newPaymentChannel = await payment_channel.create( { "channel_code": req.body.channel_code, "name": req.body.name, "slug": req.body.slug, "description": req.body.description, "status": 1 } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": newPaymentChannel } );
  },

  "update": async ( req, res ) => {
    const updatePaymentChannel = await payment_channel.update( req.body,
      {
        "returning": true,
        "where": {
          "id": req.params.id
        }
      }
    ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": updatePaymentChannel } );
  },

  "delete": async ( req, res ) => {
    await payment_channel.destroy( {
      "where": {
        "id": req.params.id
      }
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": null } );
  }

};
