const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

const { report_daily_overview, doi_tac, device_type, platform, report_user_by_region, region, report_hourly_ccu } = require( "../../models/products/models" );
const { sumPropertyValue, sumArrPropertyValue } = require( "../../helpers/utils/functions/function" );


const moment = require( "moment" ),

  // Handle format date to query condition yyyy-mm-dd
  formatDate = ( date, numDay ) => {
    const resDate = moment( date.getTime() - numDay * 24 * 3600 * 1000 ).format( "YYYY-MM-DD" );

    return resDate;
  },

  // find result by condition
  findByCondition = async ( res, info, attr, con ) => {
    con.date = {
      "$between": [ formatDate( new Date(), 6 ), formatDate( new Date(), 0 ) ]
    };
    const data = await report_daily_overview.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  },

  // find result by condition region
  findByConditionRegion = async ( res, info, attr, con ) => {
    con.date = {
      "$between": [ formatDate( new Date(), 6 ), formatDate( new Date(), 0 ) ]
    };
    const data = await report_user_by_region.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  },

  // find result by condition ccu
  findByConditionCCU = async ( res, info, attr, con ) => {
    con.date = formatDate( new Date(), 0 );
    const data = await report_hourly_ccu.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  };


module.exports = {
  /** ***********************************************START TAB USER****************************************************************** **/

  /**
   * Statistic general ccu
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "ccu": async ( req, res ) => {
    let attr = [ "id", "date", "time", "peak_ccu", "avg_ccu", "created_at", "updated_at" ],
      con = {},
      date = new Date(),
      dataResponse = [];

    const dataResult = await findByConditionCCU( res, req.query, attr, con );

    for ( let j = date.getHours(); j >= 0; j-- ) {
      const result = {
        "time": `${j < 10 ? "0" : ""}${j}:00`,
        // eslint-disable-next-line no-loop-func
        "peak_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 0 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "peak_ccu" ),
        // eslint-disable-next-line no-loop-func
        "avg_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 0 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "avg_ccu" )
      };

      dataResponse.push( result );
    }
    for ( let j = 23; j > 24 - ( 24 - date.getHours() ); j-- ) {
      const result = {
        "time": `${j < 10 ? "0" : ""}${j}:00`,
        // eslint-disable-next-line no-loop-func
        "peak_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 1 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "peak_ccu" ),
        // eslint-disable-next-line no-loop-func
        "avg_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 1 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "avg_ccu" )
      };

      dataResponse.push( result );
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new user by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newUserByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_new_user", "created_at", "updated_at" ],
      con = {},
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < 7; i++ ) {
      dataResponse[ `DAY_${ 7 - i }` ] = {
        "date": formatDate( new Date(), i ),
        // eslint-disable-next-line no-loop-func
        "total_new_user": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( new Date(), i ) ), "total_new_user" )
      };
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general active user by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeUserByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_active_user", "created_at", "updated_at" ],
      con = {},
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < 7; i++ ) {
      dataResponse[ `DAY_${ 7 - i }` ] = {
        "date": formatDate( new Date(), i ),
        // eslint-disable-next-line no-loop-func
        "total_active_user": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( new Date(), i ) ), "total_active_user" )
      };
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new user by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newUserByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_new_user", "created_at", "updated_at" ],
      con = {},
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_new_user": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_new_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general active user by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeUserByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "total_active_user", "created_at", "updated_at" ],
      con = {},
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.device_type_id === item.id ), "total_active_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new user by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeUserByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_active_user", "created_at", "updated_at" ],
      con = {},
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_active_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /** ***********************************************END TAB USER****************************************************************** **/

  /** ************************************************START TAB DEVICE***************************************************************** **/
  /**
   * Statistic general open app by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "openApp": async ( req, res ) => {
    let attr = [ "id", "date", "total_open_app", "created_at", "updated_at" ],
      con = {},
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < 7; i++ ) {
      dataResponse[ `DAY_${ 7 - i }` ] = {
        "date": formatDate( new Date(), i ),
        // eslint-disable-next-line no-loop-func
        "total_open_app": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( new Date(), i ) ), "total_open_app" )
      };
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newDevice": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "total_new_user", "created_at", "updated_at" ],
      con = {},
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPlatform.map( async ( item ) => {
        return {
          "name_platform": item.name,
          "id_platform": item.id,
          "total_new_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === item.id ), "total_new_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new device by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newDeviceByDay": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "total_new_user", "created_at", "updated_at" ],
      con = {},
      dataResponse = {},
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < 7; i++ ) {
      // eslint-disable-next-line no-loop-func
      dataResponse[ `DAY_${ 7 - i }` ] = await Promise.all( findPlatform.map( ( item ) => {
        return {
          "date": formatDate( new Date(), i ),
          "name_platform": item.name,
          "id_platform": item.id,
          "total_new_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === item.id ).filter( ( prop ) => prop.date === formatDate( new Date(), i ) ), "total_new_user" )
        };
      } ) );
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new device by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newDeviceByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "platform_id", "total_new_user", "created_at", "updated_at" ],
      con = {},
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        const newDevice = await Promise.all( findPlatform.map( async ( child ) => {
          return {
            "name_platform": child.name,
            "id_platform": child.id,
            "total_new_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.dt_id === item.id ), "total_new_user" )
          };
        } ) );

        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_new_user": sumPropertyValue( dataResult.filter( ( prod ) => prod.dt_id === item.id ), "total_new_user" ),
          "new_device": newDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new device by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeDeviceByDay": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "total_active_user", "created_at", "updated_at" ],
      con = {},
      dataResponse = {},
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < 7; i++ ) {
      // eslint-disable-next-line no-loop-func
      dataResponse[ `DAY_${ 7 - i }` ] = await Promise.all( findPlatform.map( ( item ) => {
        return {
          "date": formatDate( new Date(), i ),
          "name_platform": item.name,
          "id_platform": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === item.id ).filter( ( prop ) => prop.date === formatDate( new Date(), i ) ), "total_active_user" )
        };
      } ) );
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general new device by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeDeviceByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "platform_id", "total_active_user", "created_at", "updated_at" ],
      con = {},
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        const activeDevice = await Promise.all( findPlatform.map( async ( child ) => {
          return {
            "name_platform": child.name,
            "id_platform": child.id,
            "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.device_type_id === item.id ), "total_active_user" )
          };
        } ) );

        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( prod ) => prod.device_type_id === item.id ), "total_active_user" ),
          "active_device": activeDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  "activeDeviceByRegion": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "region_id", "total_active_user", "created_at", "updated_at" ],
      con = {},
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      findRegion = await region.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByConditionRegion( res, req.query, attr, con ),
      dataResponse = await Promise.all( findRegion.map( async ( item ) => {
        const activeDevice = await Promise.all( findPlatform.map( async ( child ) => {
          return {
            "name_platform": child.name,
            "id_platform": child.id,
            "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.region_id === item.id ), "total_active_user" )
          };
        } ) );

        return {
          "name_region": item.name,
          "id_region": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( prod ) => prod.region_id === item.id ), "total_active_user" ),
          "active_device_region": activeDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /** ************************************************END TAB DEVICE***************************************************************** **/


  /** ************************************************START TAB REVENUE***************************************************************** **/

  /**
   * Statistic general revenue by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "revenueByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_subs_sms_income", "total_retail_income", "total_charge", "created_at", "updated_at" ],
      con = {},
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < 7; i++ ) {
      dataResponse[ `DAY_${ 7 - i }` ] = {
        "date": formatDate( new Date(), i ),
        // eslint-disable-next-line no-loop-func
        "total_revenue": sumArrPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( new Date(), i ) ), [ "total_subs_sms_income", "total_retail_income", "total_charge" ] )
      };
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  "revenueByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_subs_sms_income", "total_retail_income", "total_charge", "created_at", "updated_at" ],
      con = {},
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_revenue": sumArrPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), [ "total_subs_sms_income", "total_retail_income", "total_charge" ] )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  }

  /** ************************************************END TAB REVENUE***************************************************************** **/
};
