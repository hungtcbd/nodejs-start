/**
 * Module controller API Search User
 * Creator: hocpv
 * Editor:
 * CreateAt: 04/09/19
 * UpdateAt:
 *
 */
const { user, login_fail } = require( "../../models/products/models" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

module.exports = {
  /**
   * Search user logged in
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "logged": async ( req, res ) => {
    let dataUser = null,
      condition = {},
      params = [ "id", "phone", "name", "email" ];

    // Check params
    if ( Object.keys( req.body ).length === 0 || ( Object.keys( req.body ).filter( ( arr1Item ) => params.includes( arr1Item ) ) ).length === 0 || req.body.id.trim() === "" && req.body.name.trim() === "" && req.body.phone.trim() === "" && req.body.email.trim() === "" ) {
      return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
    }
    if ( req.body.id && req.body.id.trim() !== "" ) {
      condition.user_uuid = req.body.id;
    }
    if ( req.body.phone && req.body.phone.trim() !== "" ) {
      condition.phone_number = req.body.phone;
    }
    if ( req.body.name && req.body.name.trim() !== "" ) {
      condition.username = req.body.name;
    }
    if ( req.body.email && req.body.email.trim() !== "" ) {
      condition.email = req.body.email;
    }

    // handle process with try catch
    try {
      dataUser = await user.findOne( {
        "attributes": [ "user_uuid", "username", "phone_number", "last_login", "email" ],
        "where": {
          "$and": condition
        }
      } );
    } catch ( e ) {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataUser } );
  },

  /**
   * Search user not logged in
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "notLogged": async ( req, res ) => {
    let dataUser = null;

    // Check params
    if ( !req.body.keyword ) {
      return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
    }

    // handle process with try catch
    try {
      dataUser = await login_fail.findOne( {
        "where": {
          "$or": [ { "device_id": req.body.keyword } ]
        }
      } );
    } catch ( e ) {
      logger.log( "Có vấn đề khi truy vấn tới database!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataUser } );
  }
};
