/**
 * Module controller search transaction
 * Creator: hocpv
 * Editor:
 * CreateAt: 09/09/19
 * UpdateAt:
 *
 */

const { getTransactionInMonth } = require( "../../models/products/models/transaction/transactionRaw" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );
const exportExcel = require( "../../helpers/utils/functions/export_excel.helper" );
const fs = require( "fs" );
const path = require( "path" );

module.exports = {
  /**
   *  Handle all in here
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "index": async ( req, res ) => {
    let offSet = 0;

    // Check params
    if ( !req.body.month || typeof parseFloat( req.body.month ) !== "number" || !req.body.year || typeof parseFloat( req.body.year ) !== "number" ) {
      return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
    }

    if ( req.query.perPage && req.query.page ) {
      if ( typeof parseInt( req.query.perPage ) !== "number" || typeof parseInt( req.query.page ) !== "number" ) {
        return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
      }
      offSet = ( parseInt( req.query.page ) - 1 ) * parseInt( req.query.perPage );
    }

    // handle params
    // eslint-disable-next-line no-return-assign
    Object.keys( req.body ).map( ( k ) => req.body[ k ] = typeof req.body[ k ] == "string" ? req.body[ k ].trim() : req.body[ k ] );

    const transactions = await getTransactionInMonth( `transaction_${ ( `0${ req.body.month }` ).slice( -2 ) }_${req.body.year}`, req.body );

    if ( transactions === undefined ) {
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    }
    if ( req.query.export === "true" ) {
      return res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "TOTAL": transactions.length, "DATA": transactions } );
    }
    // eslint-disable-next-line one-var
    const pagination = {
        "total": transactions.length,
        "perPage": req.query.perPage ? parseInt( req.query.perPage ) : 10,
        "page": req.query.page ? parseInt( req.query.page ) : 1
      },
      dataRes = {
        "transactions": transactions.slice( offSet ).slice( 0, req.query.perPage ? parseInt( req.query.perPage ) : 10 ),
        "pagination": pagination
      };

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataRes } );

  },

  /**
   * Export data to csv or xlsx
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "export": async ( req, res ) => {
    let exportDir = "Export",
      filename = "Export-Transaction",
      today = `${( new Date() ).getDate() }-${ ( new Date() ).getMonth() + 1 }-${ ( new Date() ).getFullYear()}`;

    // Check params
    if ( Object.keys( req.query ) [ 0 ] !== "type" || req.query.type.trim() !== "csv" && req.query.type.trim() !== "xlsx" || !req.body.month || typeof parseFloat( req.body.month ) !== "number" || !req.body.year || typeof parseFloat( req.body.year ) !== "number" ) {
      return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
    }

    // Get data
    const transaction = await getTransactionInMonth( `transaction_${ ( `0${ req.body.month }` ).slice( -2 ) }_${req.body.year}`, req.body );

    if ( transaction === undefined ) {
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    }

    filename += `-${today}`;
    if ( !fs.existsSync( exportDir ) ) {
      fs.mkdir( "Export", ( e ) => {
        console.log( e );
        logger.log( "Có vấn đề khi xuất dữ liệu!", { "INFO": req.body, "ERROR": e } );
      } );
    }

    fs.readdirSync( exportDir ).forEach( ( file ) => {
      if ( path.basename( file ) === filename ) {
        filename += `-${( new Date() ).getTime()}`;
      }
    } );
    filename = path.join( exportDir, filename );
    filename = exportExcel( JSON.parse( JSON.stringify( transaction ) ), req.query.type, { "filename": filename } );

    res.download( filename );
  }
};
