"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const CCU = sequelize.define( "report_hourly_ccu", {
    "dt_id": DataTypes.INTEGER, // mã đối tác
    "device_type_id": DataTypes.INTEGER, // mã loại thiết bị mobile,web,tv,...
    "platform_id": DataTypes.INTEGER, // nền tảng sử dụng: android, ios, window, macos,linux
    "peak_ccu": DataTypes.INTEGER, // ccu cao
    "avg_ccu": DataTypes.INTEGER, // ccu trung binh
    "time": DataTypes.TIME, // thời gian
    "date": {
      "type": DataTypes.DATE, // ngày thống kê
      get() {
        return moment( this.getDataValue( "date" ) ).format( "YYYY-MM-DD" );
      }
    },
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false, "freezeTableName": true } );

  // eslint-disable-next-line no-unused-vars
  CCU.associate = function( models ) {
    // associations can be defined here
  };


  return CCU;
};

