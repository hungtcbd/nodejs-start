"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const user = sequelize.define( "user", {
    "user_uuid": DataTypes.STRING,
    "username": DataTypes.STRING,
    "password": DataTypes.STRING,
    "fullname": DataTypes.STRING,
    "birthday": DataTypes.DATE,
    "hour_of_birth": DataTypes.TIME,
    "gender": DataTypes.INTEGER,
    "email": DataTypes.STRING,
    "phone_number": DataTypes.STRING,
    "login_type": DataTypes.STRING,
    "avatar": DataTypes.TEXT,
    "last_login": DataTypes.DATE,
    "social_id": DataTypes.STRING,
    "social_token": DataTypes.TEXT,
    "telco_id": DataTypes.INTEGER,
    "active_at": DataTypes.DATE,
    "last_subscribe_at": DataTypes.DATE,
    "last_unsubscribe_at": DataTypes.DATE,
    "last_renew_at": DataTypes.DATE,
    "last_transaction_at": DataTypes.DATE,
    "status": DataTypes.INTEGER,
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false } );

  // eslint-disable-next-line no-unused-vars
  user.associate = function( models ) {
    // associations can be defined here
    user.hasMany( models.product_feedbacks, {
      "foreignKey": "user_uuid"
    } );
    user.hasMany( models.user_otp, {
      "foreignKey": "user_uuid"
    } );
  };
  return user;
};

