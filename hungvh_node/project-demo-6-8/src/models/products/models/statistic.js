"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const Statistic = sequelize.define( "report_daily_overview", {
    "app_id": DataTypes.INTEGER, // ứng dụng: tứ trụ, lịch cát nhật
    "dt_id": DataTypes.INTEGER, // mã đối tác
    "device_type_id": DataTypes.INTEGER, // mã loại thiết bị mobile,web,tv,...
    "platform_id": DataTypes.INTEGER, // nền tảng sử dụng: android, ios, window, macos,linux
    "total_user": DataTypes.INTEGER, // tổng số lượng user (cộng dồn qua tất cả các ngày)
    "total_active_user": DataTypes.INTEGER, // tổng số active user(vip user còn hạn) cộng dồn qua các ngày
    "total_open_app": DataTypes.INTEGER, // tổng số mở app
    "total_new_user": DataTypes.INTEGER, // tổng số user tạo mới trong ngày
    "total_new_user_subs": DataTypes.INTEGER, // tổng số user mới đăng ký sử dụng gói vip
    "total_new_subs": DataTypes.INTEGER,
    "total_resubs": DataTypes.INTEGER, // tổng số user đăng ký gói vip lại
    "total_unsubs": DataTypes.INTEGER, // tổng số lượng hủy gói vip
    "total_unsubs_today": DataTypes.INTEGER, // tổng số lượng hủy trong ngày( những user đăng ký và hủy ngay trong ngày)
    "total_unsubs_nomal": DataTypes.INTEGER, // số lượng hủy thông thường
    "total_unsub_limit_retry": DataTypes.INTEGER, // số lượng hủy do không đủ tiền để retry
    "total_unsub_cskh": DataTypes.INTEGER, // số lượng hủy qua cskh (gọi điện tổng đài)
    "unsub_rate_today": DataTypes.INTEGER, //  tỷ lệ hủy trong ngày
    "unsub_rate": DataTypes.INTEGER, // tỷ lệ hủy ( số hủy/ tổng số)
    "total_renew": DataTypes.INTEGER, // tổng số trừ tiền gói cước
    "total_renew_success": DataTypes.INTEGER, // tổng số thành công
    "total_renew_failed": DataTypes.INTEGER, // tổng số trừ tiền lỗi
    "renew_success_rate": DataTypes.INTEGER, // tỷ lệ làm mới thành công
    "renew_success_ytd_reg": DataTypes.INTEGER, // tổng số làm mới tài khoản đăng ký ngày hôm qua
    "new_subs_sms_income": DataTypes.INTEGER, // tổng tiền thu được qua sub sms đăng ký mới
    "renew_sms_income": DataTypes.INTEGER, // tổng tiền thu được gia hạn qua sms
    "total_subs_sms_income": DataTypes.INTEGER, // tổng tiền thu được qua sub sms
    "monthly_subs_sms_revenue": DataTypes.INTEGER, // lũy kế tháng
    "total_subs_sms_revenue": DataTypes.INTEGER, // TỔNG thu được từ lúc chạy dịch vụ
    "income_after_telco_sms_sub": DataTypes.INTEGER, // tiền thu được trong ngày sau khi trừ phần trăm nhà mạng
    "monthly_revenue_telcos_sms_subs": DataTypes.INTEGER, // lũy kế tháng sau khi trừ phần trăm nhà mạng
    "total_revenue_telcos_sms_subs": DataTypes.INTEGER, // TỔNG tiền
    "total_retail_income": DataTypes.INTEGER, // tổng doanh số tải lẻ
    "monthly_retail_revenue": DataTypes.INTEGER, // lũy kế tải lẻ
    "total_retail_sms": DataTypes.INTEGER, // tổng doanh số tải lẻ qua sms
    "total_retail_sms_income_after_telco": DataTypes.INTEGER, // DOANH THU TẢI LẺ SAU TRỪ NHÀ MẠNG
    "monthly_retail_sms_revenue_after_telco": DataTypes.INTEGER, // LŨY KẾ THÁNG SAU NHÀ MẠNG
    "total_retail_payment_gateway": DataTypes.INTEGER, // tổng doanh số tải lẻ qua các kênh thanh toán khác
    "total_retail_payment_gateway_after_gateway": DataTypes.INTEGER, // TỔNG TIỀN MUA LẺ QUA KÊNH THANH TOÁN
    "monthly_retail_payment_gateway_revenue_after_gateway": DataTypes.INTEGER,
    "total_charge": DataTypes.INTEGER, // tổng doanh thu từ nạp tiền
    "monthly_charge_revenue": DataTypes.INTEGER,
    "total_charge_sms": DataTypes.INTEGER, // tổng doanh thu nạp tiền từ sms
    "total_charge_sms_after_telco": DataTypes.INTEGER,
    "monthly_charge_sms_revenue_after_telco": DataTypes.INTEGER,
    "total_charge_payment_gateway": DataTypes.INTEGER, // tổng doanh thu nạp tiền từ cổng thanh toán
    "total_charge_payment_gateway_after_gateway": DataTypes.INTEGER,
    "monthly_charge_payment_gateway_revenue_after_gateway": DataTypes.INTEGER,
    "date": {
      "type": DataTypes.DATE, // ngày thống kê
      get() {
        return moment( this.getDataValue( "date" ) ).format( "YYYY-MM-DD" );
      }
    },
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false, "freezeTableName": true } );

  // eslint-disable-next-line no-unused-vars
  Statistic.associate = function( models ) {
    // associations can be defined here
    // Statistic.belongsTo( models.user, {
    //   "foreignKey": "user_uuid",
    //   "targetKey": "user_uuid"
    // } );
  };


  return Statistic;
};

