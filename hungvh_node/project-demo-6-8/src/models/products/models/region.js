"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const Region = sequelize.define( "region", {
    "name": DataTypes.STRING,
    "slug": DataTypes.STRING,
    "description": DataTypes.STRING,
    "status": DataTypes.BOOLEAN, // 1: Active, 0: Deactivate
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false, "freezeTableName": true } );

  // eslint-disable-next-line no-unused-vars
  Region.associate = function( models ) {
    // associations can be defined here
  };

  return Region;
};

