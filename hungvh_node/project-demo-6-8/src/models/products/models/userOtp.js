"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const UserOtp = sequelize.define( "user_otp", {
    "user_uuid": DataTypes.STRING,
    "phone_number": DataTypes.STRING,
    "otp_code": DataTypes.STRING,
    "expired_time": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "expired_time" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false, "freezeTableName": true } );

  // eslint-disable-next-line no-unused-vars
  UserOtp.associate = function( models ) {
    // associations can be defined here
    UserOtp.belongsTo( models.user, {
      "foreignKey": "user_uuid",
      "targetKey": "user_uuid"
    } );
  };


  return UserOtp;
};

