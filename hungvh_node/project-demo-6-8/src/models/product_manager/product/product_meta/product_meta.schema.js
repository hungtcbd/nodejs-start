const Sequelize = require( "sequelize" );

class ProductMetaSchema {
  constructor() {
    this.productId = Sequelize.STRING;
    this.key = Sequelize.STRING;
    this.value = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}
module.exports = ProductMetaSchema;
