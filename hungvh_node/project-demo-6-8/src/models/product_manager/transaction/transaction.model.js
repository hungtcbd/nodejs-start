/* eslint-disable no-param-reassign */
const config = require( "../model" );
const TransactionSchema = require( "./transaction.schema" );
const Sequelize = require( "sequelize" ),
  today = new Date(),
  thisMonth = today.getMonth() + 1 < 10 ? `0${ today.getMonth() + 1}` : today.getMonth() + 1,
  thisYear = today.getFullYear();

class TransactionModel extends Sequelize.Model {}
TransactionModel.init( new TransactionSchema(), config( { "modelName": `transaction_${thisMonth}_${thisYear}` } ) );

class TransactionRepository {
  constructor() {
    this.model = TransactionModel;
  }

  async getTransactions( filter = {}, order = [] ) {
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "ASC" ]
      ];
    }
    let coinTopups = await this.model.findAll( {
      "where": filter,
      "order": order
    } );
    
    return coinTopups;
  }

  async createTransaction( type, event, userId, paymentChannel, amount, moneyType, status, dtId, info = [] ) {
    let data = {}, transaction;

    data.transactionId = info.transactionId ? info.transactionId : "";
    data.requestId = info.requestId ? info.requestId : "" ;
    data.userId = userId;
    data.deviceId = info.deviceId ? info.deviceId : "";
    data.type = type;
    data.paymentChannelCode = paymentChannel;
    data.event = event;
    data.toUser = info.toUser ;
    if ( moneyType === "VND" ) {
      data.amount = amount;
    } else if ( moneyType === "COIN" || moneyType === "COIN_LOCK" ) {
      data.coin = amount;
      data.coin_type = moneyType === "COIN" ? 1 : 2;
    }
    if ( info.packageCode ) {
      data.packageCode = info.packageCode;
    } else if ( info.contentCode ) {
      data.packageCode = info.contentCode;
    } else if ( info.order_id ) {
      // $order = Order::getOrder(info['order_id']);
      // data['packageCode'] = $order->packageCode;
    }
    if ( dtId ) {
      data.dtId = dtId;
    } else {
      data.dtId = 1 ;
    }
    data.status = status;
    data.log = info.log ;
    transaction = await this.model.create( data );
    return transaction;
  }
}

module.exports = { TransactionModel, TransactionRepository };
