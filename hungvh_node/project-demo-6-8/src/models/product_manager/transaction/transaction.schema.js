const Sequelize = require( "sequelize" );

class TransactionSchema {
  constructor() {
    this.transactionId = Sequelize.STRING;
    this.requestId = Sequelize.STRING;
    this.userId = Sequelize.STRING;
    this.deviceId = Sequelize.STRING;
    this.type = Sequelize.STRING;
    this.paymentChannelCode = Sequelize.STRING;
    this.event = Sequelize.STRING;
    this.toUser = Sequelize.STRING;
    this.amount = Sequelize.INTEGER;
    this.coin = Sequelize.INTEGER;
    this.coinType = Sequelize.INTEGER;
    this.packageCode = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
    this.log = Sequelize.STRING;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
    this.dtId = Sequelize.INTEGER;
  }
}
module.exports = TransactionSchema;
