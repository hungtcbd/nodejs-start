const config = require( "../../model" );
const ResourceSchema = require( "./resource.schema" );
const Sequelize = require( "sequelize" );

class ResourceModel extends Sequelize.Model {}
ResourceModel.init( new ResourceSchema(), config( { "modelName": "app_resources" } ) );

class ResourceRepository {
  constructor() {
    this.model = ResourceModel;
  }

  async getResources( filter = {} ) {
    let resources = await this.model.findAll( {
      "where": filter
    } );

    return resources;
  }

  async createResource( name, resourceCode, param = "", appId = 1, source = "tu_tru" ) {
    let data = {
        "resourceCode": resourceCode,
        "name": name,
        "param": param,
        "appId": appId,
        "source": source,
        "status": 1
      },
      resource = await this.model.create( data );

    return resource;
  }

  async updateResource ( resourceId, data ) {
    let resource = await this.model.update( data, {
      "where": {
        "id": resourceId
      }
    } );

    return resource;
  }

  async getResourceById( resourceId ) {
    let resources = await this.model.findOne( {
      "where": {
        "id": resourceId
      }
    } );

    return resources;
  }
}

module.exports = { ResourceModel, ResourceRepository };
