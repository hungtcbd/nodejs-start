/* eslint-disable no-param-reassign */
const config = require( "../model" );
const Sequelize = require( "sequelize" );
const UserSchema = require( "./user.schema" );
const { CoinModel } = require( "../coins/coin.model" );
const bcrypt = require( "bcrypt" );
const PhoneNumberHelper = require( "../../../helpers/utils/functions/phone_number.helper" );

class UserModel extends Sequelize.Model {
  toJSON() {
    let values = Object.assign( {}, this.get() ),
      hiddens = this.hidden();

    for ( let hidden of hiddens ) {
      delete values[ hidden ];
    }
    
    return values;
  }
  hidden() {
    return [
      "id",
      "password",
      "userType",
      "loginType",
      "socialToken",
      "createdAt",
      "updateAt"
    ];
  }
}
UserModel.init( new UserSchema(), config( { "modelName": "users" } ) );
UserModel.hasOne( CoinModel, { "foreignKey": "user_id", "sourceKey": "userUuid" } );

class UserRepository {
  constructor() {
    this.model = UserModel;
  }

  async getUserByUsername( username ) {
    let user = await this.model.findOne( { "where": { "username": username } } );

    if ( user ) {
      user.password = "";
    }
    return user;
  }

  async getUser( userId ) {
    let user = await this.model.findOne( { "where": { "userUuid": userId } } );

    if ( user ) {
      user.password = "";
    }
   
    return user;
  }

  async getUsers( filter = {}, order = [] ) {
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "ASC" ]
      ];
    }
    let users = await this.model.findAll( {
      "where": filter,
      "order": order
    } );

    return users;
  }

  async getUsersWithCoin( filter = {}, order = [] ) {
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "ASC" ]
      ];
    }
    let users = await this.model.findAll( {
      "where": filter,
      "order": order,
      "include": [
        {
          "model": CoinModel,
          "attributes": [ "currentCoin", "totalCoin", "lastTransaction" ]
        }
      ]
    } );

    return users;
  }

  async updateUser( userId, data = {} ) {
    let notUpdate = [
      "userUuid", "loginType", "userType", "password", "username"
    ];

    for ( let key in data ) {
      if ( notUpdate.indexOf( key ) >= 0 ) {
        delete data[ key ];
      }
    }
    if ( data.note ) {
      data.logs = data.note;
    }

    const update = await this.model.update( data, {
      "where": { "user_uuid": userId }
    } );

    return update;
  }

  async createUser( data = {} ) {
    data.loginType = "basic";

    if ( !data.userType ) {
      data.userType = "basic";
    }
    if ( !data.birthday ) {
      data.birthday = new Date();
    }
    if ( !data.hourOfBirth ) {
      data.hourOfBirth = `${( new Date() ).getHours() }:${ ( new Date() ).getMinutes()}`;
    }
    if ( data.email && !data.username ) {
      data.username = data.email;
    }
  
    if ( data.phoneNumber ) {
      data.phoneNumber = PhoneNumberHelper.format( data.phoneNumber );
    }
    if ( data.phoneNumber && !data.username ) {
      data.username = data.phoneNumber ;
    }
    if ( !data.gender ) {
      data.gender = 1;
    }
    if ( !data.fullname ) {
      data.fullname = "User";
    }
    if ( data.note ) {
      data.logs = data.note;
    }
    data.status = 2;
    if ( data.password ) {
      data.password = bcrypt.hashSync( data.password, 10 );
    } else {
      data.password = bcrypt.hashSync( "12345678", 10 );
    }
    let newUser = await this.model.create( data );

    await newUser.reload();
    if ( data.coin && data.userType === "special" ) {
      await CoinModel.create( {
        "userId": newUser.userUuid,
        "totalCoin": data.coin,
        "currentCoin": data.coin
      } );
    }
    newUser.password = "";
    return newUser;
  }

  async deleteUser( userId ) {
    let data = {
      "status": 3
    };
    const update = await this.model.update( data, {
      "where": { "user_uuid": userId }
    } );

    return update;
  }
}
module.exports = { UserRepository, CoinModel };
