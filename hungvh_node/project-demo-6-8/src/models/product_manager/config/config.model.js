/* eslint-disable no-param-reassign */
const Sequelize = require( "sequelize" );
const config = require( "../model" );
const ConfigSchema = require( "./config.schema" );

class ConfigModel extends Sequelize.Model {}
ConfigModel.init( new ConfigSchema(), config( { "modelName": "system_config" } ) );

class ConfigRepository {
  constructor() {
    this.model = ConfigModel;
  }

  async list( filter, order = [] ) {
    let configs;

    if ( order.length === 0 ) {
      order = [
        [ "name", "ASC" ]
      ];
    }

    configs = await this.model.findAll( {
      "where": filter,
      "order": order
    } );
    return configs;
  }

  async create( data ) {
    let cf;

    if ( !data.name || !data.value ) {
      return false;
    }
    if ( !data.status ) {
      data.status = 1;
    }
    cf = await this.model.create( data );

    return cf;
  }

  async update( model, data ) {
    await model.update( data );
    await model.reload();
    return model;
  }

  async delete( model ) {
    let success = await model.destroy();

    return success;
  }

  async getConfigById( id ) {
    let configs = await this.model.findOne( {
      "where": {
        "id": id
      }
    } );

    return configs;
  }

  async getConfigByKey( key ) {
    let configs = await this.model.findOne( {
      "where": {
        "name": key
      }
    } );
  
    return configs;
  }
}

module.exports = { ConfigModel, ConfigRepository };

