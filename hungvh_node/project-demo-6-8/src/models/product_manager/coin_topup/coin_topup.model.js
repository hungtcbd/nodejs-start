/* eslint-disable no-param-reassign */
const config = require( "../model" );
const CoinTopupSchema = require( "./coin_topup.schema" );
const Sequelize = require( "sequelize" );
const { UserRepository } = require( "../users/user.model" );
const { TransactionRepository } = require( "../transaction/transaction.model" );
const { CoinTransactionRepository } = require( "../coin_transaction/coin_transaction.model" );
const { CoinRepository } = require( "../coins/coin.model" );

class CoinTopup extends Sequelize.Model {}
CoinTopup.init( new CoinTopupSchema(), config( { "modelName": "coin_topup" } ) );

class CoinTopupRepository {
  constructor() {
    this.model = CoinTopup;
  }

  async getCoinTopup( filter = {}, order = [] ) {
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "ASC" ]
      ];
    }
    let coinTopups = await this.model.findAll( {
      "where": filter,
      "order": order
    } );
  
    return coinTopups;
  }

  async coinTopup( fromUser, toUser, reason, amount, requestId ) {
    let transaction2 = await this.model.sequelize.transaction( async ( t ) => {
      let userRepository = new UserRepository(),
        transactionRepository = new TransactionRepository(),
        coinRepository = new CoinRepository(),
        coinTransactionRepository = new CoinTransactionRepository(),
        user = await userRepository.getUserByUsername( toUser ),
        coin = await coinRepository.getCoinBalance( user.userUuid );

      return this.model.create( {
        "requestId": requestId,
        "userId": user.userUuid,
        "fromUser": fromUser,
        "reason": reason,
        "amount": amount
      }, { "transaction": t } ).then( async ( coinTopup ) => {
        let coinTransaction = await coinTransactionRepository.createCoinTransaction( user, null, null, "topup", coin, coinTopup.amount, "Cộng tiền thành công" ),
          transaction1 = await transactionRepository.createTransaction( "ADD",
            coinTransaction.event, user.userUuid, "topup",
            coinTransaction.amountCoinChange, "COIN", coinTransaction.status, user.dtId, coinTransaction );
          
        await coinRepository.addCoin( user.userUuid, amount, transaction1.transactionId );

        return transaction1;
      } );
    } );

    return transaction2;
  }
}
module.exports = { CoinTopupRepository, CoinTopup };
