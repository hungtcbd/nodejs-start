const Sequelize = require( "sequelize" );

class BlackListSchema {
  constructor() {
    this.id = {
      "type": Sequelize.INTEGER,
      "primaryKey": true,
      "autoIncrement": true
    };
    this.userId = Sequelize.BIGINT;
    this.username = Sequelize.STRING;
    this.ip = Sequelize.STRING;
    this.expiredTime = Sequelize.DATE;
    this.reason = Sequelize.TEXT;
    this.log = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}

module.exports = BlackListSchema;
