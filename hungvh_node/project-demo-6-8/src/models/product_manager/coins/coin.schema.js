const Sequelize = require( "sequelize" );

class CoinSchema {
  constructor() {
    this.userId = Sequelize.STRING;
    this.totalCoin = Sequelize.FLOAT;
    this.currentCoin = Sequelize.FLOAT;
    this.totalCoinLock = Sequelize.FLOAT;
    this.currentCoinLock = Sequelize.FLOAT;
    this.lastTransaction = Sequelize.STRING;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}
module.exports = CoinSchema;
