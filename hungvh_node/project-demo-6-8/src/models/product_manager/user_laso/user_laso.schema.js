const Sequelize = require( "sequelize" );

class UserLaSo {
  constructor() {
    this.userUuid = Sequelize.STRING;
    this.laSoUuid = Sequelize.STRING;
    this.fullname = Sequelize.STRING;
    this.birthday = Sequelize.DATEONLY;
    this.hourOfBirth = Sequelize.TIME;
    this.gender = Sequelize.INTEGER;
    this.khoiVan = Sequelize.DATE;
    this.type = Sequelize.INTEGER;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}
module.exports = UserLaSo;
