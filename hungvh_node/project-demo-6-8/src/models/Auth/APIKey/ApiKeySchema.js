const mongoose = require( "mongoose" );

class APIKey {
  constructor() {
    this._id = mongoose.Schema.Types.ObjectId;
    this.userRequest = {
      "type": String,
      "unique": true
    };
    this.apiKey = {
      "type": String,
      "unique": true
    };
    this.role = {
      "type": Number
    };
    this.status = {
      "type": Number
    };
    this.log = {
      "type": String
    };
    this.createdAt = {
      "type": Date,
      "default": Date.now
    };
    this.updatedAt = {
      "type": Date,
      "default": Date.now
    };
  }
}

apiKeySchema = mongoose.Schema( new APIKey() );

module.exports = apiKeySchema;
