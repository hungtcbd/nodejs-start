/**
 * Module route search otp
 * Creator: hocpv
 * Editor:
 * CreateAt: 13/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const UserOtpController = require( "../../controllers/search/userOtp.controller" );

router.route( "/" ).post( UserOtpController.index );

module.exports = router;
