/**
 * Module route search activity log
 * Creator: hocpv
 * Editor:
 * CreateAt: 17/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const ActivityLogController = require( "../../controllers/search/activityLog.controller" );

router.route( "/" ).post( ActivityLogController.index );

module.exports = router;
