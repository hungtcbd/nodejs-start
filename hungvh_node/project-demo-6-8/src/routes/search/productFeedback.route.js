/**
 * Module route search product feedback
 * Creator: hocpv
 * Editor:
 * CreateAt: 12/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const ProductFeedbackController = require( "../../controllers/search/productFeedback.controller" );

router.route( "/" ).post( ProductFeedbackController.index );

module.exports = router;
