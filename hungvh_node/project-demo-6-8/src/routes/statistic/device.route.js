/**
 * Module route statistic device detail
 * Creator: hocpv
 * Editor:
 * CreateAt: 12/10/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const DeviceController = require( "../../controllers/statistic/device.controller" );

router.route( "/active-device" ).get( DeviceController.activeDevice );
router.route( "/active-device-device" ).get( DeviceController.activeDeviceByDevice );
router.route( "/new-device" ).get( DeviceController.newDevice );
router.route( "/new-device-device" ).get( DeviceController.newDeviceByDevice );
router.route( "/new-device-partner" ).get( DeviceController.newDeviceByPartner );
router.route( "/open-app" ).get( DeviceController.openApp );
router.route( "/open-app-device" ).get( DeviceController.openAppByDevice );
router.route( "/open-app-partner" ).get( DeviceController.openAppByPartner );
router.route( "/active-device-region" ).get( DeviceController.activeByRegion );

module.exports = router;
