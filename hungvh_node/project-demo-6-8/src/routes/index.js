const router = require( "express" ).Router();
// eslint-disable-next-line no-unused-vars
const authMiddleware = require( "../middlewares/auth.middleware" );

router.use( authMiddleware );
router.use( "/users/", require( "./product_manager/users/user.route" ) );
router.use( "/coinTopup/", require( "./product_manager/coin_topup/coin_topup.route" ) );
router.use( "/product/", require( "./product_manager/product/product.route" ) );
router.use( "/apps/", require( "./product_manager/app/app.route" ) );
router.use( "/black-list/", require( "./product_manager/black_list/black_list.route" ) );
router.use( "/system-config/", require( "./product_manager/system_config/system_config.route" ) );
router.use( "/payment-channel", require( "./product_manager/payment_channel/payment_channel.route" ) );

// General route
router.use( "/search/user", require( "./search/user.route" ) );
router.use( "/search/transaction", require( "./search/transaction.route" ) );
router.use( "/search/coin", require( "./search/coin.route" ) );
router.use( "/search/otp", require( "./search/userOtp.route" ) );
router.use( "/search/product-feedback", require( "./search/productFeedback.route" ) );
router.use( "/search/log", require( "./search/activityLog.route" ) );
router.use( "/device-type", require( "./filter/deviceType.route" ) );
router.use( "/platform", require( "./filter/platform.route" ) );
router.use( "/partner", require( "./filter/partner.route" ) );


// Statistic route
router.use( "/statistic/general", require( "./statistic/general.route" ) );
router.use( "/statistic/device", require( "./statistic/device.route" ) );
router.use( "/statistic/user", require( "./statistic/user.route" ) );
router.use( "/statistic/revenue", require( "./statistic/revenue.route" ) );

module.exports = router;
