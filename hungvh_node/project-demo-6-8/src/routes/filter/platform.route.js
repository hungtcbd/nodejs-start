/**
 * Module route platform
 * Creator: hocpv
 * Editor:
 * CreateAt: 12/10/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const PlatformController = require( "../../controllers/filter/platform.controller" );

router.route( "/" ).get( PlatformController.index );

module.exports = router;
