/**
 * Module route device type
 * Creator: hocpv
 * Editor:
 * CreateAt: 12/10/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const DeviceTypeController = require( "../../controllers/filter/deviceType.controller" );

router.route( "/" ).get( DeviceTypeController.index );

module.exports = router;
