const router = require( "express-promise-router" )();
const ProductController = require( "../../../controllers/product_manager/product/product.controller" );

router.route( "/" ).get( ProductController.list );
router.route( "/" ).post( ProductController.create );
router.route( "/:productId" ).get( ProductController.show.bind( ProductController ) );
router.route( "/:productId" ).put( ProductController.update.bind( ProductController ) );
router.route( "/:productId" ).delete( ProductController.delete );
router.route( "/:productId/status" ).put( ProductController.status );

module.exports = router;
