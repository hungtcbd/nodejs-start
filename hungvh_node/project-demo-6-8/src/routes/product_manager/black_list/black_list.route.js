const router = require( "express-promise-router" )();
const BlackListController = require( "../../../controllers/product_manager/black_list/black_list.controller" );

router.route( "/" ).get( BlackListController.list );
router.route( "/" ).post( BlackListController.create );
router.route( "/:blackListId" ).get( BlackListController.show );
router.route( "/:blackListId" ).put( BlackListController.update );
router.route( "/:blackListId" ).delete( BlackListController.delete );

module.exports = router;
