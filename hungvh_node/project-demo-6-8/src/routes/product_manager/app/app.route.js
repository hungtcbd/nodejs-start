const router = require( "express-promise-router" )();
const AppController = require( "../../../controllers/product_manager/app/app.controller" );
const AppVersionController = require( "../../../controllers/product_manager/app/app_version.controller" );
const AppResourceControler = require( "../../../controllers/product_manager/app/app_resource.controller" );

router.route( "/" ).get( AppController.list );
router.route( "/" ).post( AppController.create );
router.route( "/:appId" ).get( AppController.show );
router.route( "/:appId" ).put( AppController.update );
router.route( "/:appId" ).delete( AppController.delete );

router.route( "/:appId/version" ).get( AppVersionController.list );
router.route( "/:appId/version" ).post( AppVersionController.create );
router.route( "/:appId/version/:appVersionId" ).put( AppVersionController.update );
router.route( "/:appId/version/:appVersionId/status" ).put( AppVersionController.status );
router.route( "/:appId/version/:appVersionId" ).delete( AppVersionController.delete );

router.route( "/:appId/resource" ).get( AppResourceControler.list );

module.exports = router;
