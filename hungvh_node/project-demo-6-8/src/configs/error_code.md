## Quy định dữ liệu trả về cho api
* Content-type: application/json
* Tất cả đều trả về http code là 200 kể cả lỗi 
* Dạng json trả về
  
  ``` 
  {
      "CODE":0,
      "MESSAGE": "THÀNH CÔNG",
      "DATA": {
        "users": [

        ]
      }
  } 
  ```

  ## Bảng code và message 

| STT | CODE   | MESSAGE                   | CONST              | NOTE |
| --- |:------:| -------------------------:|--------------------|------|
| 1   |   0    | THÀNH CÔNG                | SUCCESS            |      |
| 2   |   1    | THẤT BẠI                  | FAIL               |      |
| 3   |   2    | SAI HOẶC THIẾU THAM SỐ    | INVALID_PARAM      |      |
| 4   |   3    | SAI CHỮ KÝ XÁC THỰC       | INVALID_SIGNATURE  |      |   