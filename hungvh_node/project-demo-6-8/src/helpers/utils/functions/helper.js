const PhoneNumberHelper = require( "./phone_number.helper" );

class Helper {
  static isEmail( data ) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( re.test( data ) ) {
      return true;
    }
    return false;
  }
  static isPhoneNumber( data ) {
    if ( PhoneNumberHelper.isValid( data ) ) {
      return true;
    }
    return false;
  }

  static formatPhoneNumber( phoneNumber ) {
    return PhoneNumberHelper.format( phoneNumber );
  }
}

module.exports = Helper;
