class RequestHelper {
  constructor() {
        
  }

  static input( request, key, defaultValue = null ) {
    let data = this.all( request );

    if ( !data[ key ] ) {
      return defaultValue;
    }
    return data[ key ];
  }

  static param( request, key, defaultValue = null ) {
    if ( request.params[ key ] ) {
      return request.params[ key ];
    }
    return defaultValue;
  }

  static all( request ) {
    let query = {}, body = {};

    if ( Object.keys( request.query ).length > 0 ) {
      query = request.query;
    }
    if ( Object.keys( request.body ).length > 0 ) {
      body = request.body;
    }
    return Object.assign( query, body );
  }

  static only( request, params = [], defaultValue = true ) {
    let input = {},
      all = this.all( request );

    for ( let param of params ) {
      if ( all[ param ] ) {
        input[ param ] = all[ param ];
      } else if ( defaultValue ) {
        input[ param ] = null;
      }
    }
    return input;
  }
}

module.exports = RequestHelper;
