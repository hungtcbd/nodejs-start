const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../services/log.service" );

const { doi_tac, device_type, platform } = require( "../../models/products/models" );

const moment = require( "moment" );

// Handle format date to query condition yyyy-mm-dd
formatDate = ( date, numDay ) => {
  const resDate = moment( date.getTime() - numDay * 24 * 3600 * 1000 ).format( "YYYY-MM-DD" );

  return resDate;
};

module.exports = {
  "conditionHelper": async ( params, res ) => {
    let con = {};

    // Filter with device type
    if ( params.device ) {
      const findDevice = await device_type.findOne( {
        "where": {
          "name": params.device
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": params, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } );

      // Check exist device type
      if ( !findDevice || findDevice === 0 ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL }: KHÔNG TÌM THẤY THIẾT BỊ HOẶC THIẾT BỊ NGỪNG HOẠT ĐỘNG!` } );
      }
      con.device_type_id = findDevice.id;
    }

    // Filter with platform
    if ( params.platform ) {
      const findPlatform = await platform.findOne( {
        "where": {
          "name": params.platform
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": params, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } );

      // Check exist platform
      if ( !findPlatform || findPlatform.status === 0 ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL }: KHÔNG TÌM THẤY NỀN TẢNG HOẶC NỀN TẢNG NGỪNG HOẠT ĐỘNG!` } );
      }
      con.platform_id = findPlatform.id;
    }

    // Filter with partner
    if ( params.partner ) {
      const findPartner = await doi_tac.findOne( {
        "where": {
          "name": params.partner
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": params, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } );

      // Check exist platform
      if ( !findPartner || findPartner.status === 0 ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL }: KHÔNG TÌM THẤY ĐỐI TÁC HOẶC ĐỐI TÁC NGỪNG HOẠT ĐỘNG!` } );
      }
      con.dt_id = findPartner.id;
    }

    // Filter with for time
    if ( !params.fromDate && !params.toDate ) {
      con.date = {
        "$between": [ formatDate( new Date(), 6 ), formatDate( new Date(), 0 ) ]
      };
    }
    if ( params.fromDate && params.toDate ) {
      con.date = {
        "$between": [ params.fromDate, params.toDate ]
      };
    }
    return con;
  }
};
