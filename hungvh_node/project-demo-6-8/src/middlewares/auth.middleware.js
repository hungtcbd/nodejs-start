const Signature = require( "../helpers/utils/functions/signature.helper" );
const RequestHelper = require( "../helpers/utils/functions/request.helper" );
const ErrorCode = require( "../configs/ErrorCode" );
const ErrorMessage = require( "../configs/ErrorMessage" );
const APIKeyModel = require( "../models/Auth/APIKey/ApiKeyModel" );


module.exports = async ( request, response, next ) => {
  let requestData = RequestHelper.all( request ),
    check = false,
    apiKeyModel = new APIKeyModel();

  if ( !requestData.userRequest || !requestData.signature ) {
    response.status( 200 ).json( {
      "CODE": ErrorCode.INVALID_SIGNATURE,
      "MESSAGE": ErrorMessage.MESSAGE_INVALID_SIGNATURE
    } );
    return;
  }
  userRequest = requestData.userRequest;
  signature = requestData.signature;

  apiKey = await apiKeyModel.getAPIKey( userRequest );
  if ( apiKey && apiKey.role === 1 ) {
    check = await Signature.checkSignature( apiKey, signature );
    if ( !check ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_SIGNATURE,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_SIGNATURE
      } );
      return;
    }
    next();
    return;
  }
  if ( !check ) {
    response.status( 200 ).json( {
      "CODE": ErrorCode.INVALID_SIGNATURE,
      "MESSAGE": ErrorMessage.MESSAGE_INVALID_SIGNATURE
    } );
    return;
  }
  next( );
};
