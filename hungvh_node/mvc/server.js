var express = require('express');
var bodyparser = require('body-parser');
var connection = require('./connection');
var routes = require('./Routes/tasks');
// var routes = require('./routers');

var app = express();
app.use(bodyparser.urlencoded({extended: true}));
app.use(bodyparser.json());

connection.init();
// routes.configure(app);
app.user('/api',routes);

var server = app.listen(8888, function() {
    console.log('Server listening on port ' + server.address().port);
});