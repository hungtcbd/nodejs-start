// const express = require('express');
// const tasks = require('../Controllers/tasks');
//
// const router = express.Router();
//
// exports.getAll = router.get('/tasks', tasks.tasks.getAll);
//
//
// modules.exports = router;
var tasks = require('../Models/tasks');
var taskscontroller = require('../Controllers/tasks');

module.exports = {
    configure: function(app) {
        app.get('/tasks/', function(req, res) {
            tasks.get(res);
        });

        app.get('/tasks/read/:id/', function(req, res) {
            // tasks.read(res);
            taskscontroller.tasks.getOne(req.param('id'),res);
        });

        app.post('/tasks/create', function(req, res) {
            tasks.create(req.body, res);
        });

        app.put('/tasks/update', function(req, res) {
            tasks.update(req.body, res);
        });

        app.delete('/tasks/delete/:id/', function(req, res) {
            tasks.delete(req.params.id, res);
        });
    }
};