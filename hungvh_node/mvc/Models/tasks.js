
var connection = require('../connection');

function tasks() {
    this.get = function (res) {
        connection.acquire(function (err, con) {
            con.query("SELECT * FROM task", function (err, result) {
                con.release();
                res.send(result);
            });
        });
    };
    
    this.create = function (field_data, res) {
        connection.acquire(function (err, con) {
            con.query("insert into task set ?", field_data, function (err, result) {
                con.release();
                if(err) {
                    res.send({status:1, message: 'created fail'});
                } else {
                    res.send({status:0, message: 'created successfully'});
                }
            });
        });
    };

    this.update = function (field_data, res) {
        connection.acquire(function (err, con) {
            con.query("update task set ? where id = ?", [field_data, field_data.id], function (err, result) {
                con.release();
                if(err) {
                    res.send({status: 1, message: 'updated fail'});
                } else {
                    res.send({status: 0, message: 'updated successfully'});
                }
            });
        });
    };

    this.read = function(field_data, res) {
        connection.acquire(function(err, con) {
            con.query('select * from task where id = ?', [field_data.id], function(err, result) {
                con.release();
                res.send(result);
            });
        });
    };

    this.delete = function(id, res) {
        connection.acquire(function(err, con) {
            con.query('delete from task where id = ?', [id], function(err, result) {
                con.release();
                if (err) {
                    res.send({status: 1, message: 'Failed to delete'});
                } else {
                    res.send({status: 0, message: 'Deleted successfully'});
                }
            });
        });
    };
}

module.exports = new tasks();