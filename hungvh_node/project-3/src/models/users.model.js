var mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/first_one');

var userSchema = mongoose.Schema({
    "userId": {
    "type": String,
        "unique": true
},
    "userName": String,
    "password": String,
    "email": String,
    "skills": Array,
    "group": Object,
    "age": Number,
    "create_at": Date,
    "update_at": Date
});
// userSchema.pre( "save", function( next ) {
//     this.update_at = Date.now();
//     next();
// } );
const users = mongoose.model( "users", userSchema );
module.exports = users;