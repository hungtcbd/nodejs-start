const mongoose = require( "mongoose" ),
    Schema = mongoose.Schema,

    UsersSchema = new Schema( {
        "userId": {
            "type": String,
            "unique": true
        },
        "userName": String,
        "password": String,
        "email": String,
        "skills": Array,
        "group": Object,
        "age": Number,
        "created_at": Date,
        "updated_at": Date
    } );

// UsersSchema.pre( "save", function( next ) {
//     this.updated_at = Date.now();
//     next();
// } );

// eslint-disable-next-line one-var
const users = mongoose.model( "users", UsersSchema );

module.exports = users;