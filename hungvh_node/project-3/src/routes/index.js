const router = require("express").Router();

router.use( "/users", require( "./users/users.route" ) );

module.exports = router;
