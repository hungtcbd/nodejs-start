const router = require( "express-promise-router" )();
const UserController = require( "../../controllers/users.controller" );

router.route( "/index" ).get( UserController.index );

module.exports = router;
