const user = require("../models/users.schema");
const code = require("./../configs/ErrorCode");
const msg = require("./../configs/ErrorMessage");
const helper = require( "./../helpers/product/function/hungvh.function" );

module.exports = {

    "index": async (req, res) => {
        let dataUser = null,
            users = null,
            condition = {},
            attributes = {
                userId: 1,
                userName: 1,
                password: 1,
                email: 1,
                group: 1,
                age: 1,
                note: 1,
                create_at: 1,
                update_at: 1
            };
        // let data = req.query.name;
        let page = 1;
        let perPage = 10;
        // res.status(200).json({"CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": typeof req.query.age});
        if ( req.query.perPage && req.query.page ) {
            if ( helper.checkNumber( req.query.page ) || helper.checkNumber( req.query.perPage ) ) {
                return res.status(200).json({"CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM});
             }
            // else {
            //     return res.status( 200 ).json( { "page": req.query.page, "perPage": req.query.perPage, "a": helper.checkNumber( req.query.page ), "b": helper.checkNumber( req.query.perPage ) } )
            // }
        }

        if ( typeof parseInt( req.query.perPage ) !== "number" || typeof parseInt(req.query.page) !== "number") {
            return res.status(200).json({"CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM});
        }
        if (req.query.perPage && req.query.page) {
            if (typeof parseInt(req.query.perPage) !== "number" || typeof parseInt(req.query.page) !== "number") {
                return res.status(200).json({"CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM});
            } else {
                perPage = parseInt(req.query.perPage);
                page = parseInt(req.query.page);
            }
        }
        // res.status(200).json({"CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA":  Number( req.query.page )});
        if (req.query.name && req.query.name.trim() !== "") {
            condition.userName = req.query.name;
        }
        if (req.query.age ) {
            condition.age = {$gte: req.query.age};
        }
        if (req.query.skills && req.query.skills.trim() !== "") {
            condition.skills = {$in: req.query.skills};
        }
        if (req.query.fromDate && req.query.toDate) {
            let fromDate = req.query.fromDate;
            let toDate = req.query.toDate;
            condition.create_at = {$gt:new Date(fromDate),$lte:new Date(toDate)};
        }
        if(req.query.groupId ) { //note
            condition[ "group.groupId" ]  =  parseInt(req.query.groupId);
        }

        // res.status(200).json({"CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": condition});
        // handle process with try catch
        try {
            users = await user.find(condition);
            // res.status(200).json({"CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": condition});
            dataUser = await user.find(condition, attributes).limit(perPage).skip((page - 1) * perPage);
        } catch (e) {
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        if (req.query.export && req.query.export === "true") {
           return res.status(200).json({
                "CODE": code.SUCCESS,
                "MESSAGE": msg.MESSAGE_SUCCESS,
                "DATA": {
                    "users": users,
                    "pagination": {
                        "total": users.length,
                        "perPage": users.length,
                        "page": 1
                    }
                }
            })
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "user": dataUser,
                "pagination": {
                    "total": users.length,
                    "perPage": perPage,
                    "page": page
                }
            }
        });
    }

};
