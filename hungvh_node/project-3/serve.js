// configure server express
const bodyParser = require( "body-parser" );
// const http = require( "http" );
// const https = require( "https" );
const express = require( "express" ),
    app = express();
const api = require( "./src/routes/index" );
const mongoose = require( "mongoose" );


mongoose.connect( 'mongodb://localhost/first_one', {
    "useCreateIndex": true,
    "useNewUrlParser": true,
    "useUnifiedTopology": true
} , function (err) {
    if (err) throw err;

    console.log('Successfully connected');
});
mongoose.set( "useFindAndModify", false );

// handle form data using bodyParser
app.use( bodyParser.json( { "extended": true } ) );
app.use( bodyParser.urlencoded( { "extended": true } ) );

// create route api
app.use( "/api/v1", api );

// route default
app.use( "/", ( req, res ) => res.send( "API running!" ) );

// listen a port
var server = app.listen(8000, function() {
    console.log('Server listening on port ' + server.address().port);
});

module.exports = app;

