// configure server express
const bodyParser = require( "body-parser" );
const http = require( "http" );
const https = require( "https" );
const express = require( "express" ),
    app = express();
const api = require( "./Routes/index" );

// handle form data using bodyParser
app.use( bodyParser.json( { "extended": true } ) );
app.use( bodyParser.urlencoded( { "extended": false } ) );

// create route api
app.use( "/api/v1", api );

// route default
app.use( "/", ( req, res ) => res.send( "API running!" ) );

// listen a port
var server = app.listen(8888, function() {
    console.log('Server listening on port ' + server.address().port);
});

module.exports = app;

