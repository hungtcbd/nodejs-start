// const { TaskRepository } = require("../../../models/product_manager/coin_topup/coin_topup.model");
const  { TaskRepository } = require('../../Models/task/task.model');
// const Validator = require("../../helpers/utils/functions/validation.helper");
const ErrorCode = require("../../configs/ErrorCode");
const ErrorMessage = require("../../configs/ErrorMessage");
const RequestHelper = require("../../helpers/utils/functions/request.helper"),
    Op = require("sequelize").Op;
class TaskController {
    constructor() {
    }

    async list( request, response ) {
        let Task, isExport = false, pagination, page = 1, limit = 10, total = 0,
            data = RequestHelper.only( request, [
                "id",
                "name",
                "user_id"
            ], false ),
            TaskRepo = new TaskRepository();

        try {
            Task = await TaskRepo.index( data );
            limit = RequestHelper.input( request, "perPage", 10 );
            page = RequestHelper.input( request, "page", 1 );
            isExport = RequestHelper.input( request, "export", "false" );
            total = Task.length;
            if ( isExport === "false" ) {
                const offset = ( page - 1 ) * limit,
                    endOffset = parseInt( offset ) + parseInt( limit );

                Task = Task.slice( offset, endOffset );
            }
            pagination = {
                "total": total,
                "perPage": limit,
                "page": page
            };
            response.status( 200 )
                .json( {
                    "CODE": ErrorCode.SUCCESS,
                    "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
                    "DATA": {
                        "Task": Task,
                        "pagination": pagination
                    }
                } );
        } catch ( e ) {
            console.log( e );
            response.status( 200 )
                .json( {
                    "CODE": ErrorCode.FAIL,
                    "MESSAGE": ErrorMessage.MESSAGE_FAIL
                } );
        }
        return;
    }
}

module.exports = new TaskController();