module.exports = {
  "generateRandomString": () => {
    let characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
      charactersLength = characters.length,
      randomString = "";

    for ( let i = 0; i < 8; i++ ) {
      randomString += characters[ Math.round( Math.random() * ( charactersLength - 1 ) ) ];
    }
    return randomString;
  },
  "isEmail": ( data ) => {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( re.test( data ) ) {
      return true;
    }
    return false;
  },
  "isPhoneNumber": ( data ) => {
    if ( PhoneNumberHelper.isValid( data ) ) {
      return true;
    }
    return false;
  },

  "formatPhoneNumber": ( phoneNumber ) => {
    return PhoneNumberHelper.format( phoneNumber );
  }
};
