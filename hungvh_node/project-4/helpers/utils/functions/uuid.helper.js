/* eslint-disable no-bitwise */

class UUID {
  
  static userUuid( userId ) {
    return this.uuidEncode( userId );
  }

  static uuidEncode( id ) {

    let timestamp = ( new Date() ).getTime() / 1000,
      shard = Math.random() * 99,
      uuid = timestamp << 23 | shard << 10 | id << 0;

    console.log( ( timestamp << 23 ) & 0xffffffff );
    return uuid;
  }

  static uuidDecode( uuid ) {
    let time = ( uuid >> 23 ) & 0x1FFFFFFFFFF,
      shard = ( uuid >> 10 ) & 0x1FFF,
      id = ( uuid >> 0 ) & 0x3FF;

    return [ id, time, shard ];
  }
  static uuid() {
    let today = new Date(),
      timestamp = `${today.getFullYear()}${this.appendLeadingZeroes( today.getMonth() + 1 )}${this.appendLeadingZeroes( today.getDate() )}${this.appendLeadingZeroes( today.getHours() )}${this.appendLeadingZeroes( today.getMinutes() )}${this.appendLeadingZeroes( today.getSeconds( 0 ) )}`,
      random = [], uuid;

    for ( let i = 0; i < 10; i++ ) {
      random.push( Math.round( Math.random() * 9 ) );
    }
    random = random.join( "" );
    uuid = `${timestamp }${random}`;
    return uuid;
  }
  static appendLeadingZeroes( n ) {
    if ( n <= 9 ) {
      return `0${ n}`;
    }
    return n;
  }
}
module.exports = UUID;
