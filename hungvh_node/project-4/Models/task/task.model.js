const config = require( "../model" );
const TaskSchema = require( "./task.schema" );
const Sequelize = require( "sequelize" );


class Task extends Sequelize.Model {}
Task.init( new TaskSchema(), config( { "modelName": "task" } ) );

class TaskRepository {
    constructor() {
        this.model = Task;
    }

    async index ( filter = {}, order = [] ) {
        if ( order.length === 0 ) {
            order = [
                [ "created_at", "ASC" ]
            ];
        }
        let task = await this.model.findAll( {
            "where": filter,
            "order": order
        } );

        return task;
    }


}
module.exports = { TaskRepository, Task };
