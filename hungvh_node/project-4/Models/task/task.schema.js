const Sequelize = require( "sequelize" );

class TaskSchema {
    constructor() {
        this.id = {
            "type": Sequelize.INTEGER,
            "primaryKey": true
        };
        this.name = Sequelize.STRING;
        this.description = Sequelize.TEXT;
        this.user_id = Sequelize.FLOAT;
        this.createdAt = Sequelize.DATE;
        this.updatedAt = Sequelize.DATE;
    }
}
module.exports = TaskSchema;