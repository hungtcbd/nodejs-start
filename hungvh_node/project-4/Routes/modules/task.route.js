const router = require( "express-promise-router" )();
const TaskController = require( "../../Controllers/task/task.controller" );

router.route( "/" ).get( TaskController.list );

module.exports = router;


