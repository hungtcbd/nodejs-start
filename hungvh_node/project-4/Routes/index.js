const router = require( "express" ).Router();

// General Route
// router.use( "/coinTopup/", require( "./product_manager/coin_topup/coin_topup.route" ) );
router.use( "/tasks/", require( "./modules/task.route" ) );

module.exports = router;