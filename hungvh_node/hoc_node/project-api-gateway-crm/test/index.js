// During the test the env variable is set to test
process.env.NODE_ENV = "test";

const chai = require( "chai" );
const chaiHttp = require( "chai-http" );

// eslint-disable-next-line no-unused-vars
const server = require( "../index" ),
  // eslint-disable-next-ligetne no-unused-vars
  should = chai.should();

chai.use( chaiHttp );

/** ***************************** DEFINE ************************************ **/

// Your test

// Product Manager
const productManager = require( "./e2e/product_manager/product_manager.test" );

// Search
const searchUser = require( "./e2e/search/user" );
const searchTransaction = require( "./e2e/search/transaction" );
const searchHistoryCoin = require( "./e2e/search/coin" );
const searchProductFeedback = require( "./e2e/search/productFeedback" );
const searchUserOtp = require( "./e2e/search/userOtp" );
const searchActivityLog = require( "./e2e/search/activityLog" );

// Statistic
const statisticGeneral = require( "./e2e/statistic/general" );
const statisticDetailDevice = require( "./e2e/statistic/device" );
const statisticDetailUser = require( "./e2e/statistic/user" );
const statisticDetailRevenue = require( "./e2e/statistic/revenue" );

// Filter
const filter = require( "./e2e/filter/filter" );

/** ***************************** END DEFINE ************************************ **/


/** ***************************** PROCESS ************************************ **/
// Product Manager
productManager( chai, server, should );

// Search
searchUser( chai, server, should );
searchTransaction( chai, server, should );
searchHistoryCoin( chai, server, should );
searchProductFeedback( chai, server, should );
searchUserOtp( chai, server, should );
searchActivityLog( chai, server, should );

// Statistic
statisticGeneral( chai, server, should );
statisticDetailDevice( chai, server, should );
statisticDetailUser( chai, server, should );
statisticDetailRevenue( chai, server, should );

// Filter
filter( chai, server, should );

/** ***************************** END PROCESS ************************************ **/

