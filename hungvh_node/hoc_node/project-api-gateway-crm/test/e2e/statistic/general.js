// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Statistic General ", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /GET statistic ccu
    */
    describe( "/GET ", () => {
      it( "it should GET statistic ccu in 24 hours recent", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/ccu" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic new user by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new user by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/new-user-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic active user by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active user by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/active-user-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic new user by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new user by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/new-user-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic active user by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active user by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/active-user-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic active user by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active user by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/active-user-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  revenue by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic revenue by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/revenue-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  revenue by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic revenue by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/revenue-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  open app
    */
    describe( "/GET ", () => {
      it( "it should GET statistic open app", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/open-app" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  new device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/new-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  new device by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new device by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/new-device-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  new device by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new device by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/new-device-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  active device by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active device by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/active-device-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  active device by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active device by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/active-device-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic  active device by region
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active device by region", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/general/active-device-region" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

  } );
};
