// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Statistic Detail device ", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /GET active device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active device by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/active-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET active device by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active device by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/active-device-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET new device by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new device by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/new-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET new device by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new device by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/new-device-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET new device by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new device by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/new-device-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET device open app
    */
    describe( "/GET ", () => {
      it( "it should GET statistic device open app", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/open-app" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET open app by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic open app by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/open-app-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET open app by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic open app by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/open-app-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET active device by region table
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active device by region table", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/device/active-device-region" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

  } );
};
