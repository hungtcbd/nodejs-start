// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Statistic Detail User ", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /GET statistic user ccu
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user ccu with filter", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/ccu" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic new user by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new user by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/new-user-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic new user by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic new user by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/new-user-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic active user by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active user by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/active-user-day" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic active user by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active user by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/active-user-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic active user by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic active user by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/active-user-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic user unsub
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user unsub", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/total-unsub" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic user unsub by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user unsub by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/total-unsub-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic user unsub by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user unsub by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/total-unsub-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic user renew success by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user renew success by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/renew-success" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic user  renew success by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user renew success by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/renew-success-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic user renew success by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic user renew success by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/user/renew-success-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );


  } );
};
