// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Statistic Detail Revenue ", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /GET statistic revenue by source money
    */
    describe( "/GET ", () => {
      it( "it should GET statistic revenue by source money", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/revenue/revenue-money" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "sms" );
            res.body.DATA.should.have.property( "payment" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic revenue by partner
    */
    describe( "/GET ", () => {
      it( "it should GET statistic revenue by partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/revenue/revenue-partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic revenue by device
    */
    describe( "/GET ", () => {
      it( "it should GET statistic revenue by device", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/revenue/revenue-device" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic revenue by platform
    */
    describe( "/GET ", () => {
      it( "it should GET statistic revenue by platform", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/revenue/revenue-platform" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET statistic revenue by day
    */
    describe( "/GET ", () => {
      it( "it should GET statistic  revenue by day", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/statistic/revenue/" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            done();
          } );
      } );
    } );


  } );
};
