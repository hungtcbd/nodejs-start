// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Get Info Filter ", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /GET all device type
    */
    describe( "/GET ", () => {
      it( "it should GET all device type", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/device-type" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET all platform
    */
    describe( "/GET ", () => {
      it( "it should GET all platform", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/platform" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET all partner
    */
    describe( "/GET ", () => {
      it( "it should GET all partner", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( "/api/v1/partner" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

  } );
};
