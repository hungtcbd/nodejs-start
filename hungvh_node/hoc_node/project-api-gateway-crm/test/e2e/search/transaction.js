// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Search Transaction Controller", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /POST search Transaction
    */
    describe( "/POST ", () => {
      it( "it should GET transaction in a month", ( done ) => {
        let data = {
          "month": "08",
          "year": "2019"
        };

        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/transaction" )
          .send( data )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "transactions" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET search Transaction with filter
    */
    describe( "/POST ", () => {
      it( "it should GET transaction in a month with filter", ( done ) => {
        let data = {
          "month": "08",
          "year": "2019",
          "username": "thuytrannd@gmail.com",
          "package_code": "LUC_HAO_5",
          "payment_channel_code": "momo",
          "fromDate": "2019/08/16",
          "toDate": "2019/08/19",
          "amount": "10000",
          "status": 0
        };

        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/transaction" )
          .send( data )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "transactions" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );
  } );
};
