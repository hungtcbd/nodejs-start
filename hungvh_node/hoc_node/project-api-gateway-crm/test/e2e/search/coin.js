// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Search History Coin Controller", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /GET search Transaction
    */
    describe( "/POST ", () => {
      it( "it should GET History Coin in a month", ( done ) => {
        let data = {
          "month": "08",
          "year": "2019"
        };

        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/coin" )
          .send( data )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "coins" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET search Transaction with filter
    */
    describe( "/POST ", () => {
      it( "it should GET History Coin in a month with filter", ( done ) => {
        let data = {
          "month": "08",
          "year": "2019",
          "status": 0,
          "date": "2019/08/01",
          "type": "trừ"
        };

        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/coin" )
          .send( data )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "coins" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );
  } );
};
