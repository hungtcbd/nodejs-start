// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Search User OTP Controller", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /POST search Transaction
    */
    describe( "/POST ", () => {
      it( "it should GET all User OTP", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/otp" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "userOtp" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET search Transaction with filter
    */
    describe( "/POST ", () => {
      it( "it should GET all User OTP with filter", ( done ) => {
        let data = {
          "name": "84397828780",
          "createdAt": "",
          "updatedAt": "",
          "status": "",
          "productId": "",
          "fromDate": "",
          "toDate": ""
        };

        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/otp" )
          .send( data )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "userOtp" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );
  } );
};
