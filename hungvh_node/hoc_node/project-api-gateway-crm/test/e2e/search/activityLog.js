// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {

  // App block test
  describe( " Search Activity Log Controller", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );

    /*
    * Test the /POST search Transaction
    */
    describe( "/POST ", () => {
      it( "it should GET Activity Log Feedback", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/log" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "log" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET search Transaction with filter
    */
    describe( "/POST ", () => {
      it( "it should GET all Activity Log with filter", ( done ) => {
        let data = {
          "fromDate": "2019/08/22",
          "toDate": "2019/09/22",
          "date": "",
          "name": "",
          "type": "users.upload-avatar",
          "status": "success"
        };

        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .post( "/api/v1/search/log" )
          .send( data )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "log" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            done();
          } );
      } );
    } );
  } );
};
