module.exports = ( chai, server, should ) => {
  describe( "Test API coin topup ", () => {
    /**
    * Test lấy danh sách user với filter
    */
    describe( "Lấy lịch sử cộng coin với filter ", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "userId": "13109075961240596"
        };
      
        chai.request( server )
          .get( "/api/v1/coinTopup" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "coinTopup" ).be.a( "array" );
            done();
          } );
      } );
      it( "Filter rỗng ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "userId": "100"
        };
      
        chai.request( server )
          .get( "/api/v1/coinTopup" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "coinTopup" ).to.be.an( "array" ).that.is.empty;
            done();
          } );
      } );
    } );
      
    describe( " API cộng coin", () => {
    
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "amount": 1000,
          "reason": "Test cộng tiền",
          "toUser": "apptest@5-mail.info"
        };

        chai.request( server )
          .post( "/api/v1/coinTopup/topup" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // res.body.DATA.should.have.property( "coinTopup" ).be.a( "object" );
            done();
          } );
      } );

      it( "Thiếu tham số ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "amount": 1000,
          "reason": "Test cộng tiền"
        };
          
        chai.request( server )
          .post( "/api/v1/coinTopup/topup" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "object" );
            done();
          } );
      } );

    } );

    
  } );
  
};
