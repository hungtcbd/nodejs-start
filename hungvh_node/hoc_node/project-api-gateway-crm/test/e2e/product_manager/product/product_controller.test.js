const { ProductRepository } = require( "../../../../src/models/product_manager/product/product.model" );

module.exports = ( chai, server, should ) => {
  describe( "Test API Product Manager ", () => {
    /**
    * Test lấy danh sách user với filter
    */
    describe( "Lấy danh sách product với filter ", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "type": "vip"
        };
      
        chai.request( server )
          .get( "/api/v1/product" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "products" ).be.a( "array" );
            done();
          } );
      } );
      it( "Filter rỗng ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "type": "abc"
        };
      
        chai.request( server )
          .get( "/api/v1/product" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "products" ).to.be.an( "array" ).that.is.empty;
            done();
          } );
      } );
    } );

    describe( "Lấy product qua product id", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
        
        chai.request( server )
          .get( "/api/v1/product/5d243639ea2c1" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "product" ).be.a( "object" );
            done();
          } );
      } );
      it( "Không tìm thấy ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
        
        chai.request( server )
          .get( "/api/v1/product/5d243639ea2c11" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "product" ).is.null;
            done();
          } );
      } );
    } );
      
    describe( " API tạo mới sản phẩm ", () => {
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new ProductRepository();
        
        repo.getProductByPackageCode( "TESTPK" ).then( ( product ) => {
          if ( product ) {
            product.destroy().then( ( ) => {
              done();
            } );
          } else {
            done();
          }
        } );
    
          
      } );
    
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "productId": "4bc69aad-16b1-47bf-8545-8b18ca479d75",
          "packageCode": "TESTPK",
          "name": "Test Product",
          "resources": "[ 1, 2, 3, 68, 5, 6, 7, 8 ]",
          "price": 100,
          "appId": 1,
          "priceMoney": 10000,
          "type": "vip",
          "time": "month",
          "duration": 100
        };
        
        chai.request( server )
          .post( "/api/v1/product" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "product" ).be.a( "object" );
            done();
          } );
      } );

      it( "Thiếu tham số packageCode, resource, price, name, appId", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "productId": "4bc69aad-16b1-47bf-8545-8b18ca479d75",
          "name": "Test Product",
          "resources": "[ 1, 2, 3, 4, 5, 6, 7, 8 ]",
          "price": 100,
          "appId": 1,
          "priceMoney": 10000,
          "type": "vip",
          "time": "month",
          "duration": 100
        };
          
        chai.request( server )
          .post( "/api/v1/product" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "object" );
            done();
          } );
      } );

      it( "Package code đã tồn tại", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "packageCode": "NV960",
          "name": "Test Product",
          "resources": "[ 1, 2, 3, 4, 5, 6, 7, 8 ]",
          "price": 100,
          "appId": 1,
          "priceMoney": 10000,
          "type": "retail",
          "time": "month",
          "duration": 100
        };
          
        chai.request( server )
          .post( "/api/v1/product" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            done();
          } );
      } );
    
    
    } );

    describe( " API cập nhật sản phẩm ", () => {
      let currentPackage = null;

      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new ProductRepository();
        
        repo.getProductByPackageCode( "TESTPK" ).then( ( product ) => {
          if ( product ) {
            currentPackage = product;
            done();
          } else {
            repo.createProduct( {
              "productId": "4bc69aad-16b1-47bf-8545-8b18ca479d75",
              "packageCode": "TESTPK",
              "name": "Test Product",
              "resources": "[ 1, 2, 3, 4, 5, 6, 7, 8 ]",
              "price": 100,
              "appId": 1,
              "priceMoney": 10000,
              "type": "vip",
              "time": "month",
              "duration": 100
            } ).then( ( newProduct ) => {
              currentPackage = newProduct;
              done();
            } );
            
          }
        } );
      
            
      } );
      
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "packageCode": "TESTPK",
          "name": "Test Product",
          "resources": "[ 1, 2, 3, 4, 5, 6, 7, 8 ]",
          "price": 1000,
          "appId": 1,
          "priceMoney": 10000,
          "type": "vip",
          "time": "month",
          "duration": 100
        };
          
        chai.request( server )
          .put( `/api/v1/product/${currentPackage.productId}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "product" ).be.a( "object" );
            done();
          } );
      } );

    } );
  } );
  
};
