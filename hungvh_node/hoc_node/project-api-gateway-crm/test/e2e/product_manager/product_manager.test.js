// eslint-disable-next-line no-unused-vars
const userControllerTest = require( "./users/user_controller.test" );
const productControllerTest = require( "./product/product_controller.test" );
const appControllerTest = require( "./app/app_controller.test" );
const appVersionControllerTest = require( "./app/app_version_controller.test" );
const appResourceControllerTest = require( "./app/app_resource_controller.test" );
const blackListControllerTest = require( "./blacklist/blacklist_controller.test" );
const coinTopupControllerTest = require( "./coin_topup/coin_topup_controller.test" );

module.exports = ( chai, server, should ) => {
  userControllerTest( chai, server, should );
  productControllerTest( chai, server, should );
  appControllerTest( chai, server, should );
  appVersionControllerTest( chai, server, should );
  appResourceControllerTest( chai, server, should );
  blackListControllerTest( chai, server, should );
  coinTopupControllerTest( chai, server, should );
}
;
  
