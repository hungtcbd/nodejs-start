const { UserRepository } = require( "../../../../src/models/product_manager/users/user.model" );

module.exports = ( chai, server, should ) => {
  describe( " API get list user ", () => {
    /**
    * Test lấy danh sách user với filter
    */
    describe( "Lấy danh sách user với filter ", () => {
      it( "luôn trả về code là 200 ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "email": "bachnq214@gmail.com"
        };
      
        chai.request( server )
          .get( "/api/v1/users" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "users" ).be.a( "array" );
            done();
          } );
      } );
    } );
  } );
  
  describe( " API tạo mới người dùng ", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      let repo = new UserRepository();

      repo.getUserByUsername( "test@test.com" ).then( ( user ) => {
        if ( user ) {
          user.destroy().then( () => {
            done();
          } );
        } else {
          done();
        }
      } );

      
    } );

    /**
    * Test tạo người dùng thông thường user
    */
    describe( "Tạo người dùng thông thường", ( ) => {
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "email": "test@test.com",
          "userType": "basic"
        };
    
        chai.request( server )
          .post( "/api/v1/users" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "user" ).be.a( "object" );
            done();
          } );
      } );

      it( "Không có email hoặc số điện thoại", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "userType": "basic"
        };
    
        chai.request( server )
          .post( "/api/v1/users" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            done();
          } );
      } );

    } );

    /**
    * Test tạo người dùng ngoại giao user
    */
    describe( "Tạo người dùng ngoại giao", ( ) => {
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "email": "test@test.com",
          "expiredTime": "2020-01-01",
          "coin": "1000",
          "note": "test",
          "userType": "special"
        };
  
        chai.request( server )
          .post( "/api/v1/users" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "user" ).be.a( "object" );
            done();
          } );
      } );

      it( "Không có expiredTime,note,coin", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "userType": "special"
        };
  
        chai.request( server )
          .post( "/api/v1/users" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            done();
          } );
      } );

    } );

  } );

  describe( " API sửa đổi thông tin người dùng ", () => {
    let currentUser = "";

    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      let repo = new UserRepository();

      repo.getUserByUsername( "test@test.com" ).then( ( user ) => {
        if ( !user ) {
          repo.createUser( {
            "email": "test@test.com",
            "userType": "basic",
            "status": 1
          } ).then( ( newUser ) => {
            currentUser = newUser;
            done();
          } );
        } else {
          currentUser = user;
          done();
        }
      } );

      
    } );

    /**
    * Test tạo người dùng thông thường user
    */
    describe( "Reset mật khẩu", ( ) => {
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "email": "test@test.com",
          "userType": "basic"
        };
    
        chai.request( server )
          .get( `/api/v1/users/reset-password/${currentUser.userUuid}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "newPassword" ).be.a( "string" );
            done();
          } );
      } );
    } );


    /**
    * Test tạo người dùng thông thường user
    */
    describe( "Thay đổi trạng thái người dùng", ( ) => {
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": "0",
          "note": "Thích thì khóa"
        };
  
        chai.request( server )
          .put( `/api/v1/users/${currentUser.userUuid}/status` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "user" ).be.a( "object" );
            done();
          } );
      } );
      it( "Không tìm thấy người dùng", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": "0",
          "note": "Thích thì khóa"
        };
  
        chai.request( server )
          .put( "/api/v1/users/123456789/status" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "string" ).eql( "user not found" );
            done();
          } );
      } );
    } );
  
    describe( "Thay đổi thông tin người dùng", ( ) => {
      it( "Thay đổi email hoặc số điện thoại", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "email": "abcd@abcd.aaa",
          "note": "Đổi email của người dùng"
        };
  
        chai.request( server )
          .put( `/api/v1/users/${currentUser.userUuid}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "user" ).be.a( "object" );
            res.body.DATA.user.should.have.property( "username" ).eql( currentUser.username );
            res.body.DATA.user.should.have.property( "email" ).eql( data.email );
            done();
          } );
      } );

      it( "Thay đổi email hoặc số điện thoại nhưng số điện thoại hoặc email đã tồn tại", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "email": "bachnq22215@gmail.com",
          "note": "Đổi email của người dùng"
        };
  
        chai.request( server )
          .put( `/api/v1/users/${currentUser.userUuid}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            done();
          } );
      } );

      it( "Không tìm thấy người dùng", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": "0",
          "note": "Thích thì khóa"
        };
  
        chai.request( server )
          .put( "/api/v1/users/123456789" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "string" ).eql( "user not found" );
            done();
          } );
      } );
    } );
  } );
};
