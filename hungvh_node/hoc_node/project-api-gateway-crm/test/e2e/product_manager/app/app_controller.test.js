const { AppRepository } = require( "../../../../src/models/product_manager/app/app/app.model" );

module.exports = ( chai, server, should ) => {
  describe( "Test API Application manager ", () => {
    describe( "Lấy danh sách app với filter ", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": 1
        };
      
        chai.request( server )
          .get( "/api/v1/apps" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "apps" ).be.a( "array" );
            done();
          } );
      } );
      it( "Filter rỗng ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "appName": "aaa"
        };
      
        chai.request( server )
          .get( "/api/v1/apps" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "apps" ).to.be.an( "array" ).that.is.empty;
            done();
          } );
      } );
    } );

    describe( "Lấy app qua app id", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
          
        chai.request( server )
          .get( "/api/v1/apps/1" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "app" ).be.a( "object" );
            done();
          } );
      } );
      it( "Không tìm thấy ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
          
        chai.request( server )
          .get( "/api/v1/apps/1000" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "app" ).is.null;
            done();
          } );
      } );
    } );
        
    describe( " API tạo mới app ", () => {
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new AppRepository();
          
        repo.list( { "appName": "Test app 2" } ).then( ( app ) => {
          if ( app.length > 0 ) {
            app[ 0 ].destroy().then( ( ) => {
              done();
            } );
          } else {
            done();
          }
        } );
      
            
      } );
      
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "appName": "Test app 2",
          "description": "Đây là bản test api"
        };
          
        chai.request( server )
          .post( "/api/v1/apps" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "app" ).be.a( "object" );
            done();
          } );
      } );
  
      it( "Thiếu tham số", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
            
        chai.request( server )
          .post( "/api/v1/apps" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "object" );
            done();
          } );
      } );
  
      it( "app name đã tồn tại", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "appName": "Test app 1",
          "description": "test app da ton tai"
        };
            
        chai.request( server )
          .post( "/api/v1/apps" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            done();
          } );
      } );
      
      
    } );
  
    describe( " API cập nhật app ", () => {
      let currentApp = null;
  
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new AppRepository();

        repo.list( { "appName": "Test app 2" } ).then( ( app ) => {
          if ( app.length > 0 ) {
            currentApp = app[ 0 ];
            done();
          } else {
            repo.create( {
              "appName": "Test app 2"
            } ).then( ( newApp ) => {
              newApp.reload();
              currentApp = newApp;
              done();
            } );
           
          }
        } );
      } );
        
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "description": "Test update"
        };
            
        chai.request( server )
          .put( `/api/v1/apps/${currentApp.id}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "app" ).be.a( "object" );
            done();
          } );
      } );
  
    } );
  } );
  
};
