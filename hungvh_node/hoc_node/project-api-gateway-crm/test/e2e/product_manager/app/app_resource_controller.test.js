module.exports = ( chai, server, should ) => {
  describe( "Test API Application resource manager ", () => {
    describe( "Lấy danh sách app với filter ", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": 1
        };
      
        chai.request( server )
          .get( "/api/v1/apps/1/resource" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "resources" ).be.a( "array" );
            done();
          } );
      } );
      it( "Filter rỗng ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": "5"
        };
      
        chai.request( server )
          .get( "/api/v1/apps/1/resource" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // res.body.DATA.should.have.property( "pagination" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "resources" ).to.be.an( "array" ).that.is.empty;
            done();
          } );
      } );
    } );
  } );
  
};
