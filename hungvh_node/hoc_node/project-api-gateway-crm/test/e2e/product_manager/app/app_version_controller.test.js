const { AppVersionRepository } = require( "../../../../src/models/product_manager/app/app_version/app_version.model" );

module.exports = ( chai, server, should ) => {
  describe( "Test API Application Version manager", () => {
    describe( "Lấy danh sách app version với filter ", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": 1
        };
      
        chai.request( server )
          .get( "/api/v1/apps/1/version" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "appVersions" ).be.a( "array" );
            done();
          } );
      } );
      it( "Filter rỗng ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "name": "version 100"
        };
      
        chai.request( server )
          .get( "/api/v1/apps/1/version" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "appVersions" ).to.be.an( "array" ).that.is.empty;
            done();
          } );
      } );

      it( "App không tồn tại ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
      
        chai.request( server )
          .get( "/api/v1/apps/11/version" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "error" ).eql( "app not found" );
            done();
          } );
      } );
    } );
        
    describe( " API tạo mới app version", () => {
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new AppVersionRepository();
          
        repo.list( { "name": "App version test" } ).then( ( app ) => {
          if ( app.length > 0 ) {
            app[ 0 ].destroy().then( ( ) => {
              done();
            } );
          } else {
            done();
          }
        } );
      
            
      } );
      
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "name": "App version test",
          "version": "2.0.0",
          "file": "{\"apk\":\"link'\"}",
          "changeLog": "test version "
        };
          
        chai.request( server )
          .post( "/api/v1/apps/1/version" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "app" ).be.a( "object" );
            done();
          } );
      } );
  
      it( "Thiếu tham số", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "name": "App version test",
          "version": "2.0.0",
          "changeLog": "test version "
        };
            
        chai.request( server )
          .post( "/api/v1/apps/1/version" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "object" );
            done();
          } );
      } );
  
      it( "app không tồn tại", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "name": "App version test",
          "version": "2.0.0",
          "file": "{apk:'link'}",
          "changeLog": "test version "
        };
            
        chai.request( server )
          .post( "/api/v1/apps/11/version" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            done();
          } );
      } );
      
      
    } );
  
    describe( " API cập nhật app ", () => {
      let currentAppVer = null;
  
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new AppVersionRepository();
   
        repo.list( { "name": "App version test" } ).then( ( app ) => {
          if ( app.length > 0 ) {
            currentAppVer = app[ 0 ];
            done();
          } else {
            repo.create( {
              "appId": "1",
              "name": "App version test",
              "version": "2.0.0",
              "file": "{\"apk\":\"link\"}",
              "changeLog": "test version "
            } ).then( ( newAppVer ) => {
              currentAppVer = newAppVer;
              done();
            } );
            
          }
        } );
    
      } );
        
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "description": "Test update"
        };
            
        chai.request( server )
          .put( `/api/v1/apps/1/version/${currentAppVer.id}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "appVersion" ).be.a( "object" );
            done();
          } );
      } );
  
    } );

    describe( " API cập nhật trạng thái app ", () => {
      let currentAppVer = null;
  
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new AppVersionRepository();
   
        repo.list( { "name": "App version test" } ).then( ( app ) => {
          if ( app.length > 0 ) {
            currentAppVer = app[ 0 ];
            done();
          } else {
            repo.create( {
              "appId": "1",
              "name": "App version test",
              "version": "2.0.0",
              "file": "{\"apk\":\"link\"}",
              "changeLog": "test version "
            } ).then( ( newAppVer ) => {
              currentAppVer = newAppVer;
              done();
            } );
            
          }
        } );
    
      } );
        
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "status": 2
        };
            
        chai.request( server )
          .put( `/api/v1/apps/1/version/${currentAppVer.id}/status` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "appVersion" ).be.a( "object" );
            done();
          } );
      } );
  
    } );
  } );
  
};
