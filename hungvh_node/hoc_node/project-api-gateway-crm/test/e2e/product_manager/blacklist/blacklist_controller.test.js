const { BlackListRepository } = require( "../../../../src/models/product_manager/black_list/black_list.model" );

module.exports = ( chai, server, should ) => {
  describe( "Test API Black list ", () => {
    describe( "Lấy danh sách blacklist với filter ", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "username": "84886869591"
        };
      
        chai.request( server )
          .get( "/api/v1/black-list" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            res.body.DATA.should.have.property( "blackList" ).be.a( "array" );
            done();
          } );
      } );
      it( "Filter rỗng ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "username": "aaa"
        };
      
        chai.request( server )
          .get( "/api/v1/black-list" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "pagination" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "blackList" ).to.be.an( "array" ).that.is.empty;
            done();
          } );
      } );
    } );

    describe( "Lấy blacklist qua app id", () => {
      it( "Thành công ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
          
        chai.request( server )
          .get( "/api/v1/black-list/2" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "blackList" ).be.a( "object" );
            done();
          } );
      } );
      it( "Không tìm thấy ", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
          
        chai.request( server )
          .get( "/api/v1/black-list/1000" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            // eslint-disable-next-line no-unused-expressions
            res.body.DATA.should.have.property( "blackList" ).is.null;
            done();
          } );
      } );
    } );
        
    describe( " API tạo mới blacklist ", () => {
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new BlackListRepository();
          
        repo.list( { "username": "84886869591" } ).then( ( list ) => {
          if ( list.length > 0 ) {
            list[ 0 ].destroy().then( ( ) => {
              done();
            } );
          } else {
            done();
          }
        } );
      
            
      } );
      
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "username": "84886869591",
          "reason": "test api"
        };
          
        chai.request( server )
          .post( "/api/v1/black-list" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "blackList" ).be.a( "object" );
            done();
          } );
      } );
  
      it( "Thiếu tham số", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234"
        };
            
        chai.request( server )
          .post( "/api/v1/black-list" )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 2 );
            res.body.should.have.property( "MESSAGE" ).eql( "SAI HOẶC THIẾU THAM SỐ" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "error" ).be.a( "object" );
            done();
          } );
      } );
      
      
    } );
  
    describe( " API cập nhật blacklist ", () => {
      let currentBlack = null;
  
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new BlackListRepository();

        repo.list( { "username": "84886869591" } ).then( ( list ) => {
          if ( list.length > 0 ) {
            currentBlack = list[ 0 ];
            done();
          } else {
            repo.create( {
              "username": "84886869591",
              "reason": "test api"
            } ).then( ( newBlack ) => {
              currentBlack = newBlack;
              done();
            } );
          }
        } );
      } );
        
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "reason": "test update blacklist"
        };
            
        chai.request( server )
          .put( `/api/v1/black-list/${currentBlack.id}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            res.body.should.have.property( "DATA" );
            res.body.DATA.should.be.a( "object" );
            res.body.DATA.should.have.property( "blackList" ).be.a( "object" );
            done();
          } );
      } );
  
    } );
    describe( " API xóa blacklist ", () => {
      let currentBlack = null;
    
      beforeEach( ( done ) => {
        // Before each test we empty the database in your case
        let repo = new BlackListRepository();
  
        repo.list( { "username": "84886869591" } ).then( ( list ) => {
          if ( list.length > 0 ) {
            currentBlack = list[ 0 ];
            done();
          } else {
            repo.create( {
              "username": "84886869591",
              "reason": "test api"
            } ).then( ( newBlack ) => {
              currentBlack = newBlack;
              done();
            } );
          }
        } );
      } );
          
      it( "Thành công", ( done ) => {
        let data = {
          "userRequesst": "dangian1",
          "signature": "1234",
          "reason": "test update blacklist"
        };
              
        chai.request( server )
          .delete( `/api/v1/black-list/${currentBlack.id}` )
          .send( data )
          .end( ( err, res ) => {
            should.not.exist( err );
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.should.have.property( "CODE" ).eql( 0 );
            res.body.should.have.property( "MESSAGE" ).eql( "THÀNH CÔNG" );
            done();
          } );
      } );
    
    } );
  } );
  
};
