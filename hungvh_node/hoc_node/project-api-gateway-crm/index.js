// configure server express
const bodyParser = require( "body-parser" );
const fs = require( "fs" );
const cors = require( "cors" );
const http = require( "http" );
const https = require( "https" );
const express = require( "express" ),
  app = express();
const logger = require( "morgan" );
const api = require( "./src/routes" );
const mongoose = require( "mongoose" );
const dotenv = require( "dotenv" );
const swaggerUi = require( "swagger-ui-express" );
const swaggerDoc = require( "./swagger" );
const Sentry = require( "@sentry/node" ); // using Sentry to check throw error

let server = null;

dotenv.config( { "path": ".env" } );

// config server with http of https
if ( process.env.APP_ENV === "production" ) {
  const options = {
    "pfx": fs.readFileSync( process.env.HTTPS_URL ),
    "passphrase": process.env.HTTPS_PASSWORD
  };

  server = https.createServer( options, app );
} else {
  server = http.createServer( app );
}

// connect to mongoose NoSQL DB
mongoose.connect( `${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`, {
  "useCreateIndex": true,
  "useNewUrlParser": true,
  "useUnifiedTopology": true
} );
mongoose.set( "useFindAndModify", false );

/* Handle try catch error with sentry */
Sentry.init( { "dsn": "https://2bfd51a80aa1465fb04b69d599d218e2@sentry.io/1736232" } );
// The request handler must be the first middleware on the app
app.use( Sentry.Handlers.requestHandler() );
// The error handler must be before any other error middleware and after all controllers
app.use( Sentry.Handlers.errorHandler() );

// Optional fallthrough error handler
// eslint-disable-next-line no-unused-vars
// eslint-disable-next-line handle-callback-err,no-unused-vars
app.use( function onError( err, req, res, next ) {
  // The error id is attached to `res.sentry` to be returned
  // and optionally displayed to the user for support.
  res.statusCode = 500;
  res.end( `${res.sentry }\n` );
} );

// set server port
app.set( "port", process.env.PORT_BASE );

// handle cors
app.use( cors( {
  "origin": "*",
  "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
  "allowedHeaders": [ "Content-Type", "Authorization" ],
  "exposedHeaders": [ "Cookie" ] } ) );

// handle form data using bodyParser
app.use( bodyParser.json( { "extended": true } ) );
app.use( bodyParser.urlencoded( { "extended": false } ) );

// create log on server
// app.use( logger( "dev" ) );

// create route api
app.use( "/api/v1", api );

// config swagger
app.use( "/api-docs", swaggerUi.serve, swaggerUi.setup( swaggerDoc ) );

// route default
app.use( "/", ( req, res ) => res.send( "API running!" ) );

// DATABASE
// eslint-disable-next-line no-unused-vars
const models = require( "./src/models/products/models" );

// listen a port
server.listen( process.env.PORT_BASE, () => {
  console.log( `Api server: process ${ process.pid } running on ${process.env.APP_URL}:${process.env.PORT_BASE}` );
} );

module.exports = app;

