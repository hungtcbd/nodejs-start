##Tài liệu hệ thống CRM Lý Số ##

### 1. Thông tin chung ###
- URL: http://...../api/v1
- URL Doc API:  http://...../api-docs



### 2. Quy định mã phản hồi đầu ra ###
 - Đầu ra là 1 json string với các tham số như sau	
		
| STT    | Field | Mô tả | Required 
| :---------: | :------| --------- | :-----:|
|   1  | CODE | Mã phản hồi  | x |
|   2     |   MESSAGE | Mô tả phản hồi  | x
|   3     |   TOTAL | Mô tả tổng số data phản hồi  | 
|   4     |   DATA | Dữ liệu trả thêm nếu có (tùy theo API) |

*Bảng quy định mã phản hồi*

| CODE  | MESSAGE |
| :----------: | :---------|
| 0     | THÀNH CÔNG
| 1     | THẤT BẠI
| 2     | SAI HOẶC THIẾU THAM SỐ
| 3     | SAI CHỮ KÝ XÁC THỰC
| 4     | TRUY CẬP TỚI DB THẤT BẠI

### 3. Cấu trúc thư mục ###


```
|-- src
|   |-- configs
|   |-- controllers
|   |   |-- Auth
|   |   |-- filter
|   |   |-- product_manager
|   |   |   |-- app
|   |   |   |-- black_list
|   |   |   |-- coin_topup
|   |   |   |-- product
|   |   |   |-- system_config
|   |   |   `-- users
|   |   |-- search
|   |   `-- statistic
|   |-- helper
|   |   |-- services
|   |   |-- statistic
|   |   `-- utils
|   |   |   |-- functions
|   |-- microservice
|   |-- middlewares
|   |-- models
|   |   |-- Auth
|   |   |   `-- APIKey
|   |   |-- product_manager
|   |   |   |-- app
|   |   |   |-- black_list
|   |   |   |-- coin_topup
|   |   |   |-- coin_transaction
|   |   |   |-- coins
|   |   |   |-- config
|   |   |   |-- product
|   |   |   |-- resource
|   |   |   |-- transaction
|   |   |   |-- user_laso
|   |   |   |-- users
|   |   |   `-- model.js
|   |   |-- productLogs
|   |   `-- products
|   |       |-- migrations
|   |       `-- models
|   `-- routes
|       |-- filter
|       |-- product_manager
|       |   |-- app
|       |   |-- black_list
|       |   |-- coin_topup
|       |   |-- product
|       |   |-- system_config
|       |   `-- users
|       |-- search
|       |-- statistic
|       `-- index.js
`-- test
|    |-- e2e
|    |    |-- filter
|    |    |-- product_manager
|    |    |-- search
|    |    `-- statistic
|    `-- index.js
`-- .env
`-- .gitignore
`-- cluster.js
`-- index.js
`-- package.json
`-- swagger.json
```
Mô tả:
- File `.env` : file môi trường chứa các thông tin về database, mật khẩu, kết nối hệ thống...
- File `cluster.js` : Khởi tạo server đa nhân ( multi-core ) để tận dụng tối đa tài nguyên hệ thống
- File `index.js` : file chứa các cấu hình khởi tạo serve, kết nối serve với db,...
- File `package.json` : file chứa các định nghĩa module thành phần cài đặt để khởi chạy hệ thống
- File `swagger.json` : file chứa các định nghĩa module thành phần cài đặt để khởi chạy hệ thống
- Thư mục `/src` : thư mục cha bao chứa toàn bộ source code về luồng xử lý api
- Thư mục `/src/configs` : chứa các file cấu hình quy định về mã lỗi, mã phản hồi và hướng dẫn
- Thư mục `/src/controllers` chứa các hàm xử lý api
- Thư mục `/src/controllers/Auth` : api cung cấp api key cho các hệ thống khác
- Thư mục `/src/controllers/filter` : chứa các file js với các hàm xử lý để tạo api lấy ra loại thiết bị, các đối tác và nền tảng phục vụ 
trong quá trình đổ giao diện phần thống kê chi tiết
- Thư mục `/src/controllers/product_manager` : Chứa các thư mục liên quan đến việc quản lý sản phẩm 
- Thư mục `/src/controllers/product_manager/app` : Api quản lý các ứng dụng như ứng dụng Tứ trụ cải mệnh, Lịch cát nhật,...
- Thư mục `/src/controllers/product_manager/black_list` : API quản lý người dùng hoặc ip bị block
- Thư mục `/src/controllers/product_manager/coin_topup` : API cộng tiền và lịch sử cộng tiền qua CRM 
- Thư mục `/src/controllers/product_manager/product` : API quản lý sản phẩm (gói cước) được kinh doanh trong app
- Thư mục `/src/controllers/product_manager/system_config` : API quản lý các config hệ thống
- Thư mục `/src/controllers/product_manager/users` : API quản lý user, thêm, sửa, xóa, khóa, mở tài khoản
- Thư mục `/src/controllers/search` : chứa các file js với các hàm xử lý để tạo ra các api tra cứu
- Thư mục `/src/controllers/statistic` : chứa các file js với các hàm xử lý để tạo ra các api thống kê
- Thư mục `/src/helpers` : chứa các hàm hỗ trợ sử dụng trong các mục đích khác nhau
- Thư mục `/src/services` : chứa hàm hỗ trợ lưu log
- Thư mục `/src/statistic` : chứa hàm hỗ trợ thống kê
- Thư mục `/src/utils` : chứa hàm hỗ trợ xử lý vào ra, validate,...
- Thư mục `/src/microservices` : mục đích cho scale hệ thống
- Thư mục `/src/middlewares` : request middlewares
- Thư mục `/src/models` : chứa các file khởi tạo và định nghĩa các bảng trong cơ sở dữ liệu
dùng các kết nối sử dụng được trên nhiều cơ sở dữ liệu khác nhau: mongodb, sql.
- Thư mục `/src/models/Auth` : bachnq
- Thư mục `/src/models/product_manager` : bachnq
- Thư mục `/src/models/productLog` : chứa khởi tạo bảng lưu log sử dụng mongodb trên server
- Thư mục `/src/models/products`: chưa khởi tạo database sử dụng ORM Sequelize cho sql
- Thư mục `/src/routes` : khai báo các đường dẫn endpoint để truy cập các api
- Thư mục `/src/routes/filter`: chứa các endpoint để lấy dữ liệu về loại thiết bị, đối tác, nền tảng phục vụ cho quá trình thống kê
- Thư mục `/src/routes/product_manage` : bachnq
- Thư mục `/src/routes/search`: chứa các endpoint để lấy dữ liệu tra cứu
- Thư mục `/src/routes/statistic`: chứa các endpoint để lấy dữ liệu thống kê
- File `/src/routes/index.js`: khai báo tổng hợp chung cho các module endpoint
- Thư mục `/test`: sử dụng để viết các file unit test cho toàn bộ dự án
- Thư mục `/test/filter`: các unit test cho các api lấy dữ liệu loại thiết bị, đối tác, nền tảng
- Thư mục `/test/product_manager`: bachnq
- Thư mục `/test/search`: unit test cho các api tra cứu
- Thư mục `/test/statistic`: unit test cho các api thống kê
- File `/test/index.js`: khai báo tổng hợp chung cho các module unit test

