# API Gateway CRM

### Installation

Recomment [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ npm install or npm i
```

For build production environments...

```sh
$ npm run serve
```

For test Api

```sh
$ npm run test
```

### Todos

 - Product  
 - Search
 - Statistic
 - Doc API
 - Unit test
 

License
----
- MIT


