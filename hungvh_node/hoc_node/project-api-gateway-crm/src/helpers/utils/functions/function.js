module.exports = {
  /**
   * sum property of object in array
   * @param items
   * @param prop
   * @returns {*}
   */
  "sumPropertyValue": ( items, prop ) => {
    return items.reduce( ( a, b ) => a + b[ prop ], 0 );
  },

  /**
   * sum array property of object in array
   * @param items
   * @param prop
   * @returns {*}
   */
  "sumArrPropertyValue": ( items, prop ) => {
    let sum = 0;

    for ( let i = 0; i < prop.length; i++ ) {
      sum += items.reduce( ( a, b ) => a + b[ prop[ i ] ], 0 );
    }
    return sum;
  }

}
