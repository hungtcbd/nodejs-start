const PhoneNumberHelper = require( "./phone_number.helper" );
const db = require( "../../../models/products/models" );

class Validate {
  constructor() {
    this.error = {};
    this.result = null;
  }
  fail() {
    if ( this.result === null ) {
      throw new Error( "run validate before" );
    }
    if ( Object.keys( this.error ).length === 0 ) {
      this.result = true;
      return false;
    }
    this.result = false;
    return true;
    
  }
  // params = {"username":["required","length:20"]}
  async validate( data, rules = {} ) {
    for ( let key in rules ) {
      for ( let validator of rules[ key ] ) {
        validator = validator.split( ":" );
        let fn = this[ validator[ 0 ] ],
          param = validator[ 1 ] ? validator[ 1 ].split( "," ) : [ "", "" ];

        switch ( fn ) {
          case this.email:
            this.email( data, key );break;
          case this.required:
            this.required( data, key ); break;
          case this.phoneNumber:
            this.phoneNumber( data, key );break;
          case this.requiredIfNotHave:
            this.requiredIfNotHave( data, key, validator[ 1 ] ); break;
          case this.notExist:
            await this.notExist( data, key, param[ 0 ], param[ 1 ] );break;
          case this.exist:
            await this.exist( data, key, param[ 0 ], param[ 1 ] );break;
          case this.json:
            this.json( data, key ); break;
          case this.requiredIf:
            this.requiredIf( data, key, param[ 0 ], param[ 1 ] ); break;
          default:
            throw new Error( `${fn } is not a validate function` );
        }
      }
    }
    if ( Object.keys( this.error ).length === 0 ) {
      this.result = true;
    } else {
      this.result = false;
    }
    return {
      "result": this.result,
      "error": this.error
    };
  }

  required( data, key ) {
    let value = data[ key ];

    if ( !value ) {
      this.error[ key ] = `${key } is required`;
      return false;
    }
    return true;
    
  }
  email( data, key ) {
    let value = data[ key ],

      re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if ( !value ) {
      return true;
    }
    if ( re.test( value ) ) {
      return true;
    }
    this.error[ key ] = `${key } is not valid email`;
    return false;
  }

  phoneNumber( data, key ) {
    let value = data[ key ];

    if ( !value ) {
      return true;
    }

    if ( PhoneNumberHelper.isValid( value ) ) {
      return true;
    }
    this.error[ key ] = `${key } is not valid phone number`;
    return false;
  }

  requiredIfNotHave( data, key, otherKey ) {
    if ( data[ otherKey ] ) {
      return true;
    }
    if ( data[ key ] ) {
      return true;
    }
    this.error[ key ] = `${key } is required`;
    return false;
  }

  async notExist( data, key, table, column ) {
    if ( !data[ key ] ) {
      return true;
    }
    let value = data[ key ],
      sql = `SELECT ${column } FROM ${ table } WHERE ${ column }='${ value}'`,
      result = await db.sequelize.query( sql, { "type": db.sequelize.QueryTypes.SELECT } ) ;

    if ( result.length > 0 ) {
      this.error[ key ] = `${key } existed in ${table}`;
      return false;
    }
    return true;
  }

  async exist( data, key, table, column ) {
    if ( !data[ key ] ) {
      return true;
    }
    let value = data[ key ],
      sql = `SELECT ${column } FROM ${ table } WHERE ${ column }='${ value}'`,
      result = await db.sequelize.query( sql, { "type": db.sequelize.QueryTypes.SELECT } ) ;

    if ( result.length <= 0 ) {
      this.error[ key ] = `${key } not exist in ${table}`;
      return false;
    }
    return true;
  }
  json( data, key ) {
    let str = data[ key ];

    try {
      JSON.parse( str );
    } catch ( e ) {
      this.error[ key ] = `${key } must be a json`;
      return false;
    }
    return true;
  }
  requiredIf( data, key, otherKey, otherValue ) {
    if ( data[ otherKey ] === otherValue ) {
      if ( !data[ key ] ) {
        this.error[ key ] = `${key } is required`;
        return false;
      }
      return true;
    }
    return true;
  }

}

module.exports = Validate;
