const PhoneNumberUtil = require( "google-libphonenumber" ).PhoneNumberUtil,
  PhoneNumberFormat = require( "google-libphonenumber" ).PhoneNumberFormat,
  DEFAULT_REGION = "VN",
  FORMAT_FOR_HUMAN_VIETNAM = "VN_HUMAN";

class PhoneNumberHelper {
    
  static isValid( phoneNumber = "", region = "" ) {
    let isValid = false;

    try {
      let phoneNumberUtil = PhoneNumberUtil.getInstance(),
        use_region = region !== "" ? region.toLocaleUpperCase() : DEFAULT_REGION,
        phoneNumberObject = phoneNumberUtil.parse( phoneNumber, use_region );

      isValid = phoneNumberUtil.isValidNumber( phoneNumberObject );
    } catch ( e ) {
      return false;
    }

    return isValid;
  }

  static isPossibleNumber( phoneNumber = "", region = DEFAULT_REGION ) {
    try {
      let phoneNumberUtil = PhoneNumberUtil.getInstance(),
        phoneNumberObject = phoneNumberUtil.parse( phoneNumber, region.toLocaleUpperCase() ),
        isPossible = phoneNumberUtil.isPossibleNumber( phoneNumberObject );

      return isPossible;
    } catch ( e ) {
      return false;
    }
  }

  static format( phoneNumber = "", format = "" ) {
    let result;

    try {
      let phoneNumberUtil = PhoneNumberUtil.getInstance(),
        phoneNumberObject = phoneNumberUtil.parse( phoneNumber.trim(), DEFAULT_REGION );

      if ( format === DEFAULT_REGION ) {
        result = `0${ phoneNumberObject.getNationalNumber()}`;
      } else if ( format === FORMAT_FOR_HUMAN_VIETNAM ) {
        result = phoneNumberUtil.formatOutOfCountryCallingNumber( phoneNumberObject, self.DEFAULT_COUNTRY );
      } else if ( format === PhoneNumberFormat.FORMAT_E164 ) {
        result = phoneNumberUtil.format( phoneNumberObject, PhoneNumberFormat.E164 );
      } else if ( format === PhoneNumberFormat.FORMAT_INTERNATIONAL ) {
        result = phoneNumberUtil.format( phoneNumberObject, PhoneNumberFormat.INTERNATIONAL );
      } else if ( format === PhoneNumberFormat.FORMAT_NATIONAL ) {
        result = phoneNumberUtil.format( phoneNumberObject, PhoneNumberFormat.NATIONAL );
      } else if ( format === PhoneNumberFormat.FORMAT_RFC3966 ) {
        result = phoneNumberUtil.format( phoneNumberObject, PhoneNumberFormat.RFC3966 );
      } else {
        result = `${phoneNumberObject.getCountryCode() }${ phoneNumberObject.getNationalNumber()}`;
      }
      
      return result;
    } catch ( e ) {
      return phoneNumber;
    }
  }

}
module.exports = PhoneNumberHelper;
