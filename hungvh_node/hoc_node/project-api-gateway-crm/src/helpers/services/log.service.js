const graylog2 = require( "graylog2" );

class Log {
  constructor() {
    this.logger = new graylog2.graylog( {
      "servers": [
        { "host": process.env.HOST_GRAY_LOG, "port": process.env.PORT_GRAY_LOG }
      ],
      // the name of this host
      // (optional, default: os.hostname())
      // "hostname": "server.name",

      // the facility for these log messages
      // (optional, default: "Node.js")
      "facility": "Node.js",

      // max UDP packet size, should never exceed the
      // MTU of your system (optional, default: 1400)
      "bufferSize": 1350
    } );

    this.logger.on( "error", function ( error ) {
      console.error( "Error while trying to write to graylog2:", error );
      if ( error ) {
        throw ( error );
      }
    } );
  }

  log( message, data ) {
    this.logger.log( message, data );
  }
}

module.exports = new Log();
