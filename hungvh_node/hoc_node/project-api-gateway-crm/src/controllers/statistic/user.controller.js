const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

const { report_daily_overview, doi_tac, device_type, report_hourly_ccu } = require( "../../models/products/models" );
const { conditionHelper } = require( "../../helpers/statistic/statistic.helper" );
const { sumPropertyValue } = require( "../../helpers/utils/functions/function" );

const moment = require( "moment" ),

  // Handle format date to query condition yyyy-mm-dd
  formatDate = ( date, numDay ) => {
    const resDate = moment( date.getTime() - numDay * 24 * 3600 * 1000 ).format( "YYYY-MM-DD" );

    return resDate;
  },

  // find result by condition
  findByCondition = async ( res, info, attr, con ) => {
    const data = await report_daily_overview.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  },

  // find result by condition with ccu
  findByConditionCCU = async ( res, info, attr, con ) => {
    const data = await report_hourly_ccu.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  };

module.exports = {
  /**
   * Statistic detail user ccu
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "ccu": async ( req, res ) => {
    let attr = [ "id", "date", "time", "peak_ccu", "avg_ccu", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      length = 1,
      date = new Date(),
      dataResponse = {};

    delete ( con.date );

    const dataResult = await findByConditionCCU( res, req.query, attr, con );

    // Check request format date
    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }

    if ( length === 1 ) {
      let resArr = [];

      for ( let j = date.getHours(); j >= 0; j-- ) {
        const result = {
          "time": `${j < 10 ? "0" : ""}${j}:00`,
          // eslint-disable-next-line no-loop-func
          "peak_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 0 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "peak_ccu" ),
          // eslint-disable-next-line no-loop-func
          "avg_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 0 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "avg_ccu" )
        };

        resArr.push( result );
      }
      for ( let j = 23; j > 24 - ( 24 - date.getHours() ); j-- ) {
        const result = {
          "time": `${j < 10 ? "0" : ""}${j}:00`,
          // eslint-disable-next-line no-loop-func
          "peak_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 1 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "peak_ccu" ),
          // eslint-disable-next-line no-loop-func
          "avg_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, 1 ) ).filter( ( prod ) => prod.time === `${j < 10 ? "0" : ""}${j}:00:00` ), "avg_ccu" )
        };

        resArr.push( result );
      }

      return res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": resArr } );
    }

    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${ length - i }` ] = {
        "date": formatDate( date, i ),
        "ccu": []
      };
      for ( let j = 0; j < 24; j++ ) {
        const ccu = {
          "time": `${j < 10 ? "0" : ""}${j}:00`,
          // eslint-disable-next-line no-loop-func
          "peak_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ).filter( ( prod ) => prod.time === `${ j < 10 ? "0" : "" }${j}:00:00` ), "peak_ccu" ),
          // eslint-disable-next-line no-loop-func
          "avg_ccu": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ).filter( ( prod ) => prod.time === `${ j < 10 ? "0" : "" }${j}:00:00` ), "avg_ccu" )
        };

        ( dataResponse[ `DAY_${ length - i }` ] ).ccu.push( ccu );
      }
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },


  /**
   * Statistic detail new user by day
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "newUserByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_new_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      length = 7,
      date = new Date(),
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    // Check request format date
    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }
    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${ length - i }` ] = {
        "date": formatDate( date, i ),
        // eslint-disable-next-line no-loop-func
        "total_new_user": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ), "total_new_user" )
      };
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail new user by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newUserByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_new_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_new_user": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_new_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail active user by day
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "activeUserByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_active_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      length = 7,
      date = new Date(),
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    // Check request format date
    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }
    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${ length - i }` ] = {
        "date": formatDate( date, i ),
        // eslint-disable-next-line no-loop-func
        "total_active_user": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ), "total_active_user" )
      };
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail active user by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeUserByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "total_active_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.device_type_id === item.id ), "total_active_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail active user by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeUserByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_active_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_active_user" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail unsub by day
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "unSubByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_unsubs", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      length = 7,
      date = new Date(),
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    // Check request format date
    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }
    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${ length - i }` ] = {
        "date": formatDate( date, i ),
        // eslint-disable-next-line no-loop-func
        "total_unsubs": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ), "total_unsubs" )
      };
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail unsub by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "unSubByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "total_unsubs", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_unsubs": sumPropertyValue( dataResult.filter( ( value ) => value.device_type_id === item.id ), "total_unsubs" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail unsub by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "unSubByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_unsubs", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_unsubs": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_unsubs" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail renew success by day
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "renewSuccessByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_renew_success", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      length = 7,
      date = new Date(),
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    // Check request format date
    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }
    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${ length - i }` ] = {
        "date": formatDate( date, i ),
        // eslint-disable-next-line no-loop-func
        "total_renew_success": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ), "total_renew_success" )
      };
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail renew success by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "renewSuccesByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "total_renew_success", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_renew_success": sumPropertyValue( dataResult.filter( ( value ) => value.device_type_id === item.id ), "total_renew_success" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail renew success by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "renewSuccessByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_renew_success", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_renew_success": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_renew_success" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  }
};
