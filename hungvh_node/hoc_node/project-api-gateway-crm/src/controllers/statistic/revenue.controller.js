const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

const { report_daily_overview, doi_tac, device_type, platform } = require( "../../models/products/models" );
const { conditionHelper } = require( "../../helpers/statistic/statistic.helper" );
const { sumArrPropertyValue } = require( "../../helpers/utils/functions/function" );

const moment = require( "moment" ),

  // Handle format date to query condition yyyy-mm-dd
  formatDate = ( date, numDay ) => {
    const resDate = moment( date.getTime() - numDay * 24 * 3600 * 1000 ).format( "YYYY-MM-DD" );

    return resDate;
  },

  // find result by condition
  findByCondition = async ( res, info, attr, con ) => {
    const data = await report_daily_overview.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  };

module.exports = {
  /**
   * Statistic detail revenue by amount
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "revenueByAmount": async ( req, res ) => {
    const find = [];

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": find } );
  },

  /**
   * Statistic detail revenue by source money
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "revenueBySourceMoney": async ( req, res ) => {
    let attr = [ "id", "date", "total_charge_payment_gateway", "total_retail_payment_gateway", "total_retail_sms", "total_charge_sms", "total_subs_sms_income", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    if ( req.query.money ) {
      if ( req.query.money === "sms" ) {
        dataResponse.sms = sumArrPropertyValue( dataResult, [ "total_retail_sms", "total_charge_sms", "total_subs_sms_income" ] );
        dataResponse.payment = 0;
        return res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
      }
      if ( req.query.money === "payment" ) {
        dataResponse.sms = 0;
        dataResponse.payment = sumArrPropertyValue( dataResult, [ "total_charge_payment_gateway", "total_retail_payment_gateway" ] );
        return res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
      }
    }

    dataResponse = {
      "sms": sumArrPropertyValue( dataResult, [ "total_retail_sms", "total_charge_sms", "total_subs_sms_income" ] ),
      "payment": sumArrPropertyValue( dataResult, [ "total_charge_payment_gateway", "total_retail_payment_gateway" ] )
    };

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );

  },

  /**
   * Statistic detail revenue by partner
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "revenueByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_charge_payment_gateway", "total_retail_payment_gateway", "total_retail_sms", "total_charge_sms", "total_subs_sms_income", "total_retail_income", "total_charge", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      prodTransfer = [ "total_subs_sms_income", "total_retail_income", "total_charge" ];

    if ( req.query.money ) {
      if ( req.query.money === "sms" ) {
        prodTransfer = [ "total_retail_sms", "total_charge_sms", "total_subs_sms_income" ];
      }
      if ( req.query.money === "payment" ) {
        prodTransfer = [ "total_charge_payment_gateway", "total_retail_payment_gateway" ];
      }
    }

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_revenue": sumArrPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), prodTransfer )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail revenue by device
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "revenueByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "total_charge_payment_gateway", "total_retail_payment_gateway", "total_retail_sms", "total_charge_sms", "total_subs_sms_income", "total_retail_income", "total_charge", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      prodTransfer = [ "total_subs_sms_income", "total_retail_income", "total_charge" ];

    if ( req.query.money ) {
      if ( req.query.money === "sms" ) {
        prodTransfer = [ "total_retail_sms", "total_charge_sms", "total_subs_sms_income" ];
      }
      if ( req.query.money === "payment" ) {
        prodTransfer = [ "total_charge_payment_gateway", "total_retail_payment_gateway" ];
      }
    }

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_revenue": sumArrPropertyValue( dataResult.filter( ( value ) => value.device_type_id === item.id ), prodTransfer )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail revenue by platform
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "revenueByPlatform": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "total_charge_payment_gateway", "total_retail_payment_gateway", "total_retail_sms", "total_charge_sms", "total_subs_sms_income", "total_retail_income", "total_charge", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      prodTransfer = [ "total_subs_sms_income", "total_retail_income", "total_charge" ];

    if ( req.query.money ) {
      if ( req.query.money === "sms" ) {
        prodTransfer = [ "total_retail_sms", "total_charge_sms", "total_subs_sms_income" ];
      }
      if ( req.query.money === "payment" ) {
        prodTransfer = [ "total_charge_payment_gateway", "total_retail_payment_gateway" ];
      }
    }

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPlatform.map( async ( item ) => {
        return {
          "name_platform": item.name,
          "id_platform": item.id,
          "total_revenue": sumArrPropertyValue( dataResult.filter( ( value ) => value.platform_id === item.id ), prodTransfer )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail revenue by day
   * @param req
   * @param res
   * @returns {Promise<*>}
   */
  "revenueByDay": async ( req, res ) => {
    let attr = [ "id", "date", "total_subs_sms_income", "total_retail_income", "total_charge", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      length = 7,
      date = new Date(),
      dataResponse = {};

    const dataResult = await findByCondition( res, req.query, attr, con );

    // Check request format date
    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }
    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${ length - i }` ] = {
        "date": formatDate( date, i ),
        // eslint-disable-next-line no-loop-func
        "total_revenue": sumArrPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ), [ "total_subs_sms_income", "total_retail_income", "total_charge" ] )
      };
    }
    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  }
};
