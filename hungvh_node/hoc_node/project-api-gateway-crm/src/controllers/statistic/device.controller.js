const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

const { report_daily_overview, doi_tac, platform, device_type, report_user_by_region, region } = require( "../../models/products/models" );
const { conditionHelper } = require( "../../helpers/statistic/statistic.helper" );
const { sumPropertyValue } = require( "../../helpers/utils/functions/function" );

const moment = require( "moment" ),

  // Handle format date to query condition yyyy-mm-dd
  formatDate = ( date, numDay ) => {
    const resDate = moment( date.getTime() - numDay * 24 * 3600 * 1000 ).format( "YYYY-MM-DD" );

    return resDate;
  },

  // find result by condition
  findByCondition = async ( res, info, attr, con ) => {
    const data = await report_daily_overview.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  },

  // find result by condition region
  findByConditionRegion = async ( res, info, attr, con ) => {
    const data = await report_user_by_region.findAll( {
      "attributes": attr,
      "where": con
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": info, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    return data;
  };

module.exports = {
  /**
   * Statistic detail active device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeDevice": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "total_active_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      dataResponse = {},
      date = new Date(),
      length = 7,
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < length; i++ ) {
      // eslint-disable-next-line no-loop-func
      dataResponse[ `DAY_${ length - i }` ] = await Promise.all( findPlatform.map( ( item ) => {
        return {
          "date": formatDate( date, i ),
          "name_platform": item.name,
          "id_platform": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === item.id ).filter( ( prop ) => prop.date === formatDate( date, i ) ), "total_active_user" )
        };
      } ) );
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail active device by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeDeviceByDevice": async ( req, res ) => {
    let con = await conditionHelper( req.query, res );

    const findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      findData = await report_daily_overview.findAll( {
        "where": con
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        const activeDevice = await Promise.all( findPlatform.map( ( child ) => {
          return {
            "name_platform": child.name,
            "id_platform": child.id,
            "total_active_user": sumPropertyValue( findData.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.device_type_id === item.id ), "total_active_user" )
          };
        } ) );

        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_active_user": sumPropertyValue( findData.filter( ( prod ) => prod.device_type_id === item.id ), "total_active_user" ),
          "active_device": activeDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail new device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newDevice": async ( req, res ) => {
    let attr = [ "id", "date", "platform_id", "total_new_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      dataResponse = {},
      date = new Date(),
      length = 7,
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( { "CODE": code.FAIL, "MESSAGE": `${msg.MESSAGE_FAIL } KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!` } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }

    const dataResult = await findByCondition( res, req.query, attr, con );

    for ( let i = 0; i < length; i++ ) {
      // eslint-disable-next-line no-loop-func
      dataResponse[ `DAY_${ length - i }` ] = await Promise.all( findPlatform.map( ( item ) => {
        return {
          "date": formatDate( date, i ),
          "name_platform": item.name,
          "id_platform": item.id,
          "total_new_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === item.id ).filter( ( prop ) => prop.date === formatDate( date, i ) ), "total_new_user" )
        };
      } ) );
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail new device by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newDeviceByDevice": async ( req, res ) => {
    let con = await conditionHelper( req.query, res );

    const findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      findData = await report_daily_overview.findAll( {
        "where": con
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        const newDevice = await Promise.all( findPlatform.map( ( child ) => {
          return {
            "name_platform": child.name,
            "id_platform": child.id,
            "total_new_user": sumPropertyValue( findData.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.device_type_id === item.id ), "total_new_user" )
          };
        } ) );

        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_new_user": sumPropertyValue( findData.filter( ( prod ) => prod.device_type_id === item.id ), "total_new_user" ),
          "new_device": newDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic detail new device by partner
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "newDeviceByPartner": async ( req, res ) => {
    let con = await conditionHelper( req.query, res );

    const findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      findData = await report_daily_overview.findAll( {
        "where": con
      } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.query, "ERROR": e.original } );
        return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
      } ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        const newDevice = await Promise.all( findPlatform.map( ( child ) => {
          return {
            "name_platform": child.name,
            "id_platform": child.id,
            "total_new_user": sumPropertyValue( findData.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.dt_id === item.id ), "total_new_user" )
          };
        } ) );

        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_new_user": sumPropertyValue( findData.filter( ( prod ) => prod.dt_id === item.id ), "total_new_user" ),
          "new_device": newDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general open app by day
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "openApp": async ( req, res ) => {
    let attr = [ "id", "date", "total_open_app", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      dataResponse = {},
      date = new Date(),
      length = 7;

    const dataResult = await findByCondition( res, req.query, attr, con );

    if ( req.query.fromDate && req.query.toDate ) {
      const fromDate = new Date( req.query.fromDate ),
        toDate = new Date( req.query.toDate );

      if ( isNaN( fromDate ) === true || isNaN( toDate ) === true ) {
        return res.status( 200 ).json( {
          "CODE": code.FAIL,
          "MESSAGE": `${msg.MESSAGE_FAIL} KHÔNG ĐÚNG ĐỊNH DẠNG NGÀY THÁNG!`
        } );
      }
      length = Math.ceil( Math.abs( toDate - fromDate ) / ( 1000 * 60 * 60 * 24 ) ) + 1;
      date = toDate;
    }

    for ( let i = 0; i < length; i++ ) {
      dataResponse[ `DAY_${length - i}` ] = {
        "date": formatDate( date, i ),
        // eslint-disable-next-line no-loop-func
        "total_open_app": sumPropertyValue( dataResult.filter( ( item ) => item.date === formatDate( date, i ) ), "total_open_app" )
      };
    }

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general open app by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "openAppByDevice": async ( req, res ) => {
    let attr = [ "id", "date", "device_type_id", "total_open_app", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findDevice.map( async ( item ) => {
        return {
          "name_device": item.name,
          "id_device": item.id,
          "total_open_app": sumPropertyValue( dataResult.filter( ( value ) => value.device_type_id === item.id ), "total_open_app" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general open app by device
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "openAppByPartner": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "total_open_app", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPartner = await doi_tac.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByCondition( res, req.query, attr, con ),
      dataResponse = await Promise.all( findPartner.map( async ( item ) => {
        return {
          "name_partner": item.name,
          "id_partner": item.id,
          "total_open_app": sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === item.id ), "total_open_app" )
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  },

  /**
   * Statistic general active by region
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "activeByRegion": async ( req, res ) => {
    let attr = [ "id", "date", "dt_id", "platform_id", "region_id", "total_active_user", "created_at", "updated_at" ],
      con = await conditionHelper( req.query, res ),
      findPlatform = await platform.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      findDevice = await device_type.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } ),
      findRegion = await region.findAll( {
        "where": {
          "status": {
            "$ne": 0
          }
        }
      } );

    const dataResult = await findByConditionRegion( res, req.query, attr, con ),
      dataResponse = await Promise.all( findRegion.map( async ( item ) => {
        const activeByWeb = sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === findDevice.filter( ( device ) => device.name === "web" )[ 0 ].id ).filter( ( prod ) => prod.region_id === item.id ), "total_active_user" ),
          activeMobile = sumPropertyValue( dataResult.filter( ( value ) => value.dt_id === findDevice.filter( ( device ) => device.name === "mobile" )[ 0 ].id ).filter( ( prod ) => prod.region_id === item.id ), "total_active_user" ),
          activeDevice = await Promise.all( findPlatform.map( async ( child ) => {
            return {
              "name_platform": child.name,
              "id_platform": child.id,
              "total_active_user": sumPropertyValue( dataResult.filter( ( value ) => value.platform_id === child.id ).filter( ( prod ) => prod.region_id === item.id ), "total_active_user" )
            };
          } ) );

        return {
          "name_region": item.name,
          "id_region": item.id,
          "total_active_user": sumPropertyValue( dataResult.filter( ( prod ) => prod.region_id === item.id ), "total_active_user" ),
          "total_active_user_web": activeByWeb,
          "total_active_user_mobile": activeMobile,
          "active_device_region": activeDevice
        };
      } ) );

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataResponse } );
  }
};
