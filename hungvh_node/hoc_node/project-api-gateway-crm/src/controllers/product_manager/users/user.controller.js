const { UserRepository } = require( "../../../models/product_manager/users/user.model" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );
const exportExcel = require( "../../../helpers/utils/functions/export_excel.helper" );
const fs = require( "fs" );
const path = require( "path" );
const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const Validate = require( "../../../helpers/utils/functions/validation.helper" );
const bcrypt = require( "bcrypt" );
const util = require( "../../../helpers/utils/functions/util" );

class UserController {
  constructor() {
    this.userRepository = new UserRepository();
  }

  async list( request, response ) {
    let filter = {},
      userRepository = new UserRepository(),
      users = [],
      limit = 10,
      total = 0,
      page = 1,
      isExport = false,
      pagination = {};


    filter = RequestHelper.only( request, [
      "username",
      "phoneNumber",
      "email",
      "loginType",
      "status"
    ], false );
    
    users = await userRepository.getUsersWithCoin( filter );
    limit = RequestHelper.input( request, "perPage", 10 );
    page = RequestHelper.input( request, "page", 1 );
    isExport = RequestHelper.input( request, "export", "false" );
    total = users.length;
    if ( isExport === "false" ) {
      const offset = ( page - 1 ) * limit,
        endOffset = parseInt( offset ) + parseInt( limit );

      users = users.slice( offset, endOffset );
    }
    pagination = {
      "total": total,
      "perPage": limit,
      "page": page
    };
    response.status( 200 )
      .json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "users": users,
          "pagination": pagination
        }
      } );
  }

  async export1( request, response ) {
    let filter = {},
      users = [],
      userRepository = new UserRepository(),
      exportDir = "Export",
      filename = "Export",
      today = `${( new Date() ).getDate() }-${ ( new Date() ).getMonth() + 1 }-${ ( new Date() ).getFullYear()}`,
      fileType = "xlsx";

    filter = RequestHelper.only( request, [
      "username",
      "phoneNumber",
      "email",
      "loginType",
      "status"
    ], false );
   
    fileType = RequestHelper.input( request, "type" );
    if ( fileType !== "csv" || fileType !== "xlsx" ) {
      fileType = "xlsx";
    }

    users = await userRepository.getUsers( filter );
    for ( let key in filter ) {
      filename += `-${key}:${filter[ key ]}`;
    }
    
    filename += `-${today}`;
    if ( !fs.existsSync( exportDir ) ) {
      fs.mkdir( "Export", ( e ) => {
        throw e;
      } );
    }
    
    fs.readdirSync( exportDir ).forEach( ( file ) => {
      if ( path.basename( file ) === filename ) {
        filename += `-${( new Date() ).getTime()}`;
        return;
      }
    } );
    filename = path.join( exportDir, filename );
    filename = exportExcel( JSON.parse( JSON.stringify( users ) ), fileType, { "filename": filename } );
    
    response.download( filename );
  }

  async export( request, response ) {
    let filter = {},
      users = [],
      userRepository = new UserRepository();
     

    filter = RequestHelper.only( request, [
      "username",
      "phoneNumber",
      "email",
      "loginType",
      "status"
    ], false );

    users = await userRepository.getUsers( filter );
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "users": users
      }
    } );
    return;
  }

  async update( request, response ) {
    let rules = {
        "email": [ "email", "notExist:users,username", "notExist:users,email" ],
        "phoneNumber": [ "phoneNumber", "notExist:users,username", "notExist:users,phonenumer" ]
      },
      userRepository = new UserRepository(),
      updated,
      userId,
      user,
      data = {},
      validate = {};

    data = RequestHelper.only( request, [
      "username",
      "email",
      "phoneNumber",
      "status",
      "fullname",
      "birthday",
      "hourOfBirth",
      "gender",
      "note"
    ], false );
    validate = await ( new Validate() ).validate( data, rules );
    if ( validate.result === false ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validate.error
        }
      } );
      return;
    }
    userId = request.params.userId;

    user = await userRepository.getUser( userId );
    if ( !user ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "user not found"
        }
      } );
      return;
    }
    try {
      updated = await userRepository.updateUser( userId, data );
      user = await userRepository.getUser( userId );
    } catch ( e ) {
      updated = false;
    }
    if ( updated ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "user": user
        }
      } );
    } else {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
    }
   
  }

  async resetPassword( request, response ) {
    let userRepository = new UserRepository(), user, userId, randomPassword, password, updated;

    userId = request.params.userId;
    user = await userRepository.getUser( userId );
    if ( !user ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "user not found"
        }
      } );
      return;
    }

    randomPassword = util.generateRandomString();
    password = bcrypt.hashSync( randomPassword, 10 );
    try {
      updated = await userRepository.updateUser( userId, { "password": password } );
    } catch ( e ) {
      updated = false;
    }
    if ( updated ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "newPassword": randomPassword
        }
      } );
    } else {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
    }
  }

  async create( request, response ) {
    let data = RequestHelper.only( request, [
        "fullname",
        "email",
        "phoneNumber",
        "birthday",
        "hourOfBirth",
        "gender",
        "userType",
        "expiredTime",
        "coin",
        "note",
        "password"
      ] ),
      rules = {
        "email": [ "email", "requiredIfNotHave:phoneNumber", "notExist:users,email", "notExist:users,username" ],
        "phoneNumber": [ "phoneNumber", "requiredIfNotHave:email", "notExist:users,phone_number", "notExist:users,username" ],
        "expiredTime": [ "requiredIf:userType,special" ],
        "coin": [ "requiredIf:userType,special" ],
        "note": [ "requiredIf:userType,special" ]
      },
      validate = await ( new Validate() ).validate( data, rules ),
      userRepository = new UserRepository(),
      newUser = null;

    if ( validate.result === false ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validate.error
        }
      } );
      return;
    }

    newUser = await userRepository.createUser( data );
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "user": newUser
      }
    } );
    return;
  }

  async delete( request, response ) {
    let userId = request.params.userId,
      userRepository = new UserRepository();

    user = await userRepository.getUser( userId );
    if ( !user ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "user not found"
        }
      } );
      return;
    }
    try {
      await userRepository.deleteUser( userId );

      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "delete": userId
        }
      } );
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
    }
     
  
  }

  async status( request, response ) {
    let userId, user, statusChange, note, userRepository = new UserRepository();

    userId = RequestHelper.param( request, "userId" );
    user = await userRepository.getUser( userId );
    if ( !user ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "user not found"
        }
      } );
      return;
    }
    statusChange = RequestHelper.input( request, "status" );
    if ( statusChange === 3 ) {
      statusChange = 0;
    }
    note = RequestHelper.input( request, "note" );
    try {
      await userRepository.updateUser( userId, {
        "status": statusChange,
        "logs": note
      } );
      await user.reload();
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "user": user
        }
      } );
    } catch ( e ) {
      console.log( e );
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
    }
  }

  
}

module.exports = new UserController();
