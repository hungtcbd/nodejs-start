const { CoinTopupRepository } = require( "../../../models/product_manager/coin_topup/coin_topup.model" );
// const { CoinRepository } = require( "../../../models/bachnq/coins/coin.model" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );
const RequestHelper = require( "../../../helpers/utils/functions/request.helper" ),
  Op = require( "sequelize" ).Op;

class CoinTopupController {
  constructor() {
  }

  async list( request, response ) {
    let coinTopup, isExport = false, pagination, page = 1, limit = 10, total = 0,
      data = RequestHelper.only( request, [
        "userId",
        "toUser",
        "fromUser",
        "fromDate",
        "toDate"
      ], false ),
      coinTopupRepo = new CoinTopupRepository();

    data.createdAt = {};
    if ( data.fromDate ) {
      data.createdAt = Object.assign( data.createdAt, {
        [ Op.gte ]: data.fromDate
      } );
      delete data.fromDate;
    }
    if ( data.toDate ) {
      data.createdAt = Object.assign( data.createdAt, {
        [ Op.lte ]: data.toDate
      } );
      delete data.toDate;
    }
    try {
      coinTopup = await coinTopupRepo.getCoinTopup( data );
      limit = RequestHelper.input( request, "perPage", 10 );
      page = RequestHelper.input( request, "page", 1 );
      isExport = RequestHelper.input( request, "export", "false" );
      total = coinTopup.length;
      if ( isExport === "false" ) {
        const offset = ( page - 1 ) * limit,
          endOffset = parseInt( offset ) + parseInt( limit );

        coinTopup = coinTopup.slice( offset, endOffset );
      }
      pagination = {
        "total": total,
        "perPage": limit,
        "page": page
      };
      response.status( 200 )
        .json( {
          "CODE": ErrorCode.SUCCESS,
          "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
          "DATA": {
            "coinTopup": coinTopup,
            "pagination": pagination
          }
        } );
    } catch ( e ) {
      console.log( e );
      response.status( 200 )
        .json( {
          "CODE": ErrorCode.FAIL,
          "MESSAGE": ErrorMessage.MESSAGE_FAIL
        } );
    }
    return;
  }

  async coinTopup( request, response ) {
    let data = RequestHelper.only( request, [
        "requestId",
        "fromUser",
        "toUser",
        "reason",
        "amount" ] ),
      rules = {
        "toUser": [ "required", "exist:users,username" ],
        "reason": [ "required" ],
        "amount": [ "required" ]
      },
      coinTopupRepo = new CoinTopupRepository(),
      validation = await ( new Validator() ).validate( data, rules );

    if ( validation.result === false ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validation.error
        }
      } );
      return;
    }
    try {
      let transaction = await coinTopupRepo.coinTopup( data.fromUser, data.toUser, data.reason, data.amount, data.requestId );

      response.status( 200 )
        .json( {
          "CODE": ErrorCode.SUCCESS,
          "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
          "DATA": {
            "transaction": transaction
          }
        } );
    } catch ( e ) {
      response.status( 200 )
        .json( {
          "CODE": ErrorCode.FAIL,
          "MESSAGE": ErrorMessage.MESSAGE_FAIL
        } );
    }
    return;
  }
}

module.exports = new CoinTopupController();
