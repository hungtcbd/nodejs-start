const { ConfigRepository } = require( "../../../models/product_manager/config/config.model" );
const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );

class SystemConfigController {
  constructor() {
        
  }

  async list( request, response ) {
    let config, data, pagination, page = 1, limit = 10, total = 0, isExport = false, configRepository = new ConfigRepository();

    data = RequestHelper.only( request, [ "name" ], false );
    try {
      config = await configRepository.list( data );
      total = config.length;
      limit = RequestHelper.input( request, "perPage", 10 );
      page = RequestHelper.input( request, "page", 1 );
      isExport = RequestHelper.input( request, "export", "false" );
      if ( isExport === "false" ) {
        const offset = ( page - 1 ) * limit,
          endOffset = parseInt( offset ) + parseInt( limit );

        config = config.slice( offset, endOffset );
      }
      pagination = {
        "total": total,
        "perPage": limit,
        "page": page
      };
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "configs": config,
          "pagination": pagination
        }
      } );
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
    }
  }

  async create( request, response ) {
    let data, config, rules,
      configRepository = new ConfigRepository(),
      validator = new Validator();
    
    data = RequestHelper.only( request, [
      "name",
      "value",
      "description"
    ], true );
    rules = {
      "name": [ "required", "notExist:system_config,name" ],
      "value": [ "required" ]
    };
    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      config = await configRepository.create( data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "config": config
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
      return;
    }
  }

  async update( request, response ) {
    let data, config, rules = {}, configId,
      configRepository = new ConfigRepository(),
      validator = new Validator();
  
    configId = RequestHelper.param( request, "configId" );
    data = RequestHelper.only( request, [
      "name",
      "value",
      "description",
      "status"
    ], true );

    config = await configRepository.getConfigById( configId );

    if ( !config ) {
      response.status( 404 );
      return;
    }

    if ( data.name !== config.name ) {
      let testConfig = await configRepository.getConfigByKey( data.name );

      if ( testConfig ) {
        response.status( 200 ).json( {
          "CODE": ErrorCode.INVALID_PARAM,
          "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
          "DATA": {
            "error": {
              "name": `${data.name } existed`
            }
          }
        } );
        return;
      }
    }

    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      config = await configRepository.update( config, data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "config": config
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

  async show( request, response ) {
    let configId, config, configRepository = new ConfigRepository();

    configId = RequestHelper.param( request, "configId" );
    config = await configRepository.getConfigById( configId );
    if ( !config ) {
      response.status( 404 );
      return;
    }
    
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "config": config
      }
    } );
    return;
  }

  async delete( request, response ) {
    let config, configId, configRepository = new ConfigRepository();

    configId = RequestHelper.param( request, "configId" );
    config = await configRepository.getConfigById( configId );
    if ( !config ) {
      response.status( 404 );
      return;
    }
    await configRepository.delete( config );

    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS
    } );
    return;
  }
}

module.exports = new SystemConfigController();
