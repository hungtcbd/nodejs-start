const { ProductRepository } = require( "../../../models/product_manager/product/product.model" );
const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );

class ProductController {
  constructor() {
  }
  
  async list( request, response ) {
    let data = RequestHelper.only( request, [
        "packageCode",
        "appId",
        "type",
        "name",
        "status" ], false ),
      productRepository = new ProductRepository(),
      products, pagination, page = 1, limit = 10, total = 0, isExport = false;

    try {
      products = await productRepository.getProducts( data );
      total = products.length;
      limit = RequestHelper.input( request, "perPage", 10 );
      page = RequestHelper.input( request, "page", 1 );
      isExport = RequestHelper.input( request, "export", "false" );
      if ( isExport === "false" ) {
        const offset = ( page - 1 ) * limit,
          endOffset = parseInt( offset ) + parseInt( limit );

        products = products.slice( offset, endOffset );
      }
      pagination = {
        "total": total,
        "perPage": limit,
        "page": page
      };
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "products": products,
          "pagination": pagination
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
    
  }

  async create( request, response ) {
    let data = RequestHelper.only( request, [
        "appId",
        "packageCode",
        "planId",
        "name",
        "description",
        "type",
        "price",
        "priceMoney",
        "time",
        "duration",
        "status",
        "resources",
        "default",
        "promotion"
      ], false ),
      rules = {
        "appId": [ "required" ],
        "packageCode": [ "required", "notExist:products,package_code" ],
        "name": [ "required", "notExist:products,name" ],
        "type": [ "required" ],
        "price": [ "required" ],
        "priceMoney": [ "required" ],
        "resources": [ "required", "json" ],
        "time": [ "requiredIf:type,vip" ],
        "duration": [ "requiredIf:type,vip" ]
      },
      product,
      validator = await ( new Validator() ).validate( data, rules ),
      productRepository = new ProductRepository();

    if ( validator.result === false ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }

    try {
      product = await productRepository.createProduct( data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "product": product
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
   
  }

  async update( request, response ) {
    let data = RequestHelper.only( request, [
        "appId",
        "packageCode",
        "planId",
        "name",
        "description",
        "type",
        "price",
        "priceMoney",
        "time",
        "duration",
        "status",
        "resources",
        "default",
        "promotion"
      ], false ),
      rules = {
        "appId": [ "required" ],
        "packageCode": [ "required" ],
        "name": [ "required" ],
        "type": [ "required" ],
        "price": [ "required" ],
        "priceMoney": [ "required" ],
        "resources": [ "required" ],
        "time": [ "requiredIf:type,vip" ],
        "duration": [ "requiredIf:type,vip" ]
      },
      productId = RequestHelper.param( request, "productId" ),
      product,
      validator = await ( new Validator() ).validate( data, rules ),
      productRepository = new ProductRepository();

    product = await productRepository.getProductById( productId );
    if ( !product ) {
      response.status( 404 ).end();
      return;
    }
    if ( !await this.checkUpdate( product, data ) ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": { "productCheck": "fail" }
        }
      } );
      return;
    }


    if ( validator.result === false ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }

    try {
      product = await productRepository.updateProduct( product, data );
    } catch ( e ) {
      console.log( e );
      product = null;
    }
    if ( !product ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
    product = await productRepository.getProductById( product.productId );
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "product": product
      }
    } );
  }

  async status( request, response ) {
    let productId = RequestHelper.param( request, "productId" ),
      productRepository = new ProductRepository(),
      status = RequestHelper.input( request, "status", 1 ),

      product = await productRepository.getProductById( productId );

    if ( !product ) {
      response.status( 404 );
      return;
    }

    try {
      product = await productRepository.updateProductStatus( product, status );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "product": product
        }
      } );
    } catch ( e ) {
      console.log( e );
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

  async show( request, response ) {
    let product,
      productId = RequestHelper.param( request, "productId" ),
      productRepository = new ProductRepository();

    try {
      product = await productRepository.getProductById( productId );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "product": product
        }
      } );
    } catch ( e ) {
      console.log( e );
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }
  async delete() {
    
  }

  async checkUpdate( product, data ) {
    let productRepository = new ProductRepository();

    if ( data.packageCode !== product.packageCode || data.name !== product.name ) {
      let productCheck = await productRepository.getProducts( {
        "packageCode": data.packageCode,
        "productId": {
          "$ne": product.productId
        },
        "$or": {
          "packageCode": data.packageCode,
          "name": data.name
        }
      } );
      
      if ( productCheck.length > 0 ) {
        return false;
      }
      return true;
    }
    return true;
  }
}
module.exports = new ProductController();
