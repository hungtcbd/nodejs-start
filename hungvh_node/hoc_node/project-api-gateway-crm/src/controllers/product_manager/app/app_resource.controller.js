const { AppRepository } = require( "../../../models/product_manager/app/app/app.model" );
const { ResourceRepository } = require( "../../../models/product_manager/app/app_resource/resource.model" );

const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );

class AppResourceController {
  constructor() {
        
  }

  async list( request, response ) {
    let app,
      appId,
      resources,
      appRepository = new AppRepository(),
      resourceRepository = new ResourceRepository();

    appId = RequestHelper.param( request, "appId" );
    app = await appRepository.getAppById( appId );
    if ( !app ) {
      response.status( 404 ).end();
      return;
    }
    try {
      resources = await resourceRepository.getResources( { "appId": appId } );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "resources": resources
        }
      } );
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
    }
  }

  async create( request, response ) {
    let data, appId, app, rules,
      appRepository = new AppRepository(),
      resourceRepository = new ResourceRepository(),
      resource,
      validator = new Validator();

    appId = RequestHelper.param( request, "appId" );
    app = await appRepository.getAppById( appId );
    if ( !app ) {
      response.status( 404 ).end();
      return;
    }
    data = RequestHelper.only( request, [
      "name",
      "source",
      "resourceCode"
    ], true );
    rules = {
      "name": [ "required", "notExist:app_resources,name" ],
      "source": [ "required" ],
      "resourceCode": [ "required" ]
    };
    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      data.appId = appId;
      resource = await resourceRepository.create( data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "resource": resource
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
      return;
    }
  }

  async update( request, response ) {
    let data, app, resource, rules = {}, appId, resourceId,
      resourceRepository = new ResourceRepository(),
      appRepository = new AppRepository(),
      validator = new Validator();
  
    appId = RequestHelper.param( request, "appId" );
    resourceId = RequestHelper.param( request, "resourceId" );
    app = await appRepository.getAppById( appId );
    resource = await resourceRepository.getResourceById( resourceId );
    if ( !app || !resource ) {
      response.status( 404 ).end();
      return;
    }

    data = RequestHelper.only( request, [
      "name",
      "source",
      "resourceCode"
    ], true );
    rules = {
      "name": [ "required" ],
      "source": [ "required" ],
      "resourceCode": [ "required" ]
    };
    
    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }

    if ( data.name !== resource.name || data.resourceCode !== resource.resourceCode ) {
      let filter = {
        "$or": {
          "name": data.name,
          "resourceCode": data.resourceCode
        },
        "id": {
          "$ne": parseInt( resourceId )
        },
        "status": 1
      };

      const existVersions = await resourceRepository.getResources( filter );

      if ( existVersions.length > 0 ) {
        response.status( 200 ).json( {
          "CODE": ErrorCode.INVALID_PARAM,
          "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
          "DATA": {
            "error": "existed"
          }
        } );
        return;
      }
    }
    
    try {
      resource = await resourceRepository.updateResource( resourceId, data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "resource": resource
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

}

module.exports = new AppResourceController();
