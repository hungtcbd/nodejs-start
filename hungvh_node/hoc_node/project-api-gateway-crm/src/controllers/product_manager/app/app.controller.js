const { AppRepository } = require( "../../../models/product_manager/app/app/app.model" );
const RequestHelper = require( "../../../helpers/utils/functions/request.helper" );
const Validator = require( "../../../helpers/utils/functions/validation.helper" );
const ErrorCode = require( "../../../configs/ErrorCode" );
const ErrorMessage = require( "../../../configs/ErrorMessage" );

class AppController {
  constructor() {
        
  }

  async list( request, response ) {
    let apps, data, pagination, page = 1, limit = 10, appRepository = new AppRepository();

    data = RequestHelper.only( request, [ "appName", "status" ], false );
    try {
      apps = await appRepository.list( data, [], true );
      limit = RequestHelper.input( request, "perPage", 10 );
      page = RequestHelper.input( request, "page", 1 );
      pagination = {
        "total": apps.length,
        "perPage": limit,
        "page": page
      };
      const offset = ( page - 1 ) * limit,
        endOffset = parseInt( offset ) + parseInt( limit );

      apps = apps.slice( offset, endOffset );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "apps": apps,
          "pagination": pagination
        }
      } );
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
    }
  }

  async create( request, response ) {
    let data, app, rules,
      appRepository = new AppRepository(),
      validator = new Validator();
    
    data = RequestHelper.only( request, [
      "appName",
      "description"
    ], true );
    rules = {
      "appName": [ "required", "notExist:apps,app_name" ]
    };
    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      app = await appRepository.create( data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "app": app
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL,
        "ERROR": e
      } );
      return;
    }
  }

  async update( request, response ) {
    let data, app, rules = {}, appId,
      appRepository = new AppRepository(),
      validator = new Validator();
  
    appId = RequestHelper.param( request, "appId" );
    data = RequestHelper.only( request, [
      "appName",
      "description"
    ], false );

    app = await appRepository.getAppById( appId );

    if ( !app ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": "app not found"
        }
      } );
      return;
    }

    await validator.validate( data, rules );
    if ( validator.fail() ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_PARAM,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_PARAM,
        "DATA": {
          "error": validator.error
        }
      } );
      return;
    }
    try {
      app = await appRepository.update( app, data );
      response.status( 200 ).json( {
        "CODE": ErrorCode.SUCCESS,
        "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
        "DATA": {
          "app": app
        }
      } );
      return;
    } catch ( e ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.FAIL,
        "MESSAGE": ErrorMessage.MESSAGE_FAIL
      } );
      return;
    }
  }

  async show( request, response ) {
    let appId, app, appRepository = new AppRepository();

    appId = RequestHelper.param( request, "appId" );
    app = await appRepository.getAppById( appId );
    
    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS,
      "DATA": {
        "app": app
      }
    } );
    return;
  }

  async delete( request, response ) {
    let app, appId, appRepository = new AppRepository();

    appId = RequestHelper.param( request, "appId" );
    app = appRepository.getAppById( appId );
    if ( !app ) {
      response.status( 404 );
      return;
    }
    
    await appRepository.delete( app );

    response.status( 200 ).json( {
      "CODE": ErrorCode.SUCCESS,
      "MESSAGE": ErrorMessage.MESSAGE_SUCCESS
    } );
    return;
  }
}

module.exports = new AppController();
