const Log = require( "../../models/productLogs/activityLog.model" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

module.exports = {
  "index": async ( req, res ) => {
    let date = new Date(),
      dateNow = new Date(),
      oldDate = new Date( date.setDate( date.getDate() - 7 ) ),
      condition = {},
      offSet = 0;

    // handle params
    // eslint-disable-next-line no-return-assign
    Object.keys( req.body ).map( ( k ) => req.body[ k ] = typeof req.body[ k ] == "string" ? req.body[ k ].trim() : req.body[ k ] );

    // Multi filter params
    if ( Object.keys( req.body ).length === 0 ) {
      condition.$expr = {
        "$and": [
          { "$gte": [ { "$dateFromString": { "dateString": "$responseTime" } }, oldDate ] },
          { "$lte": [ { "$dateFromString": { "dateString": "$responseTime" } }, dateNow ] }
        ]
      };
    }
    if ( req.body.name && req.body.name.trim() !== "" ) {
      condition.username = req.body.name;
    }
    if ( req.body.fromDate && req.body.toDate && req.body.fromDate.trim() !== "" && req.body.toDate.trim() !== "" ) {
      condition.$expr = {
        "$and": [
          { "$gte": [ { "$dateFromString": { "dateString": "$responseTime" } }, new Date( req.body.fromDate ) ] },
          { "$lte": [ { "$dateFromString": { "dateString": "$responseTime" } }, new Date( req.body.toDate ) ] }
        ]
      };
    }
    if ( req.body.date && req.body.date.trim() !== "" ) {
      let dateCon = new Date( req.body.date );

      condition.$expr = {
        "$and": [
          { "$gte": [ { "$dateFromString": { "dateString": "$responseTime" } }, new Date( req.body.date ) ] },
          { "$lt": [ { "$dateFromString": { "dateString": "$responseTime" } }, new Date( dateCon.setDate( dateCon.getDate() + 1 ) ) ] }
        ]
      };
    }
    if ( req.body.type && req.body.type.trim() !== "" ) {
      condition.event = req.body.type;
    }
    if ( req.body.status && req.body.status.trim() !== "" ) { // fail or success
      condition.result = req.body.status;
    }
    // Pagination
    if ( req.query.perPage && req.query.page ) {
      if ( typeof parseInt( req.query.perPage ) !== "number" || typeof parseInt( req.query.page ) !== "number" ) {
        return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
      }
      offSet = ( parseInt( req.query.page ) - 1 ) * parseInt( req.query.perPage );
    }

    // Query DB
    const log = await Log.find( condition, "-_id -request -response -__v" ).lean().catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    if ( req.query.export === "true" ) {
      return res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "TOTAL": log.length, "DATA": log } );
    }
    // eslint-disable-next-line one-var
    const pagination = {
        "total": log.length,
        "perPage": req.query.perPage ? parseInt( req.query.perPage ) : 10,
        "page": req.query.page ? parseInt( req.query.page ) : 1
      },
      dataRes = {
        "log": log.slice( offSet ).slice( 0, req.query.perPage ? parseInt( req.query.perPage ) : 10 ),
        "pagination": pagination
      };

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataRes } );
  }
};
