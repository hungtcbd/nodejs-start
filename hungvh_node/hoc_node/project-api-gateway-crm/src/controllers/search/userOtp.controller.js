/**
 * Module controller API Search OTP
 * Creator: hocpv
 * Editor:
 * CreateAt: 04/09/19
 * UpdateAt:
 *
 */
const { user, user_otp } = require( "../../models/products/models" );
const db = require( "../../models/products/models" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const logger = require( "../../helpers/services/log.service" );

module.exports = {
  "index": async ( req, res ) => {
    let conditionRe = {},
      condition = {},
      offSet = 0;

    // handle params
    // eslint-disable-next-line no-return-assign
    Object.keys( req.body ).map( ( k ) => req.body[ k ] = typeof req.body[ k ] == "string" ? req.body[ k ].trim() : req.body[ k ] );

    // Check params
    if ( req.body.status && Number.isNaN( parseFloat( req.body.status ) ) === true ) {
      return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
    }

    if ( req.body.name && req.body.name.trim() !== "" ) {
      conditionRe.username = req.body.name;
    }
    if ( req.body.status ) { // 0: response, 1: not response
      condition.status = parseFloat( req.body.status );
    }
    if ( req.body.productId && req.body.productId.trim() !== "" ) {
      condition.product_id = req.body.productId;
    }
    if ( req.body.createdAt && req.body.createdAt.trim() !== "" ) {
      condition.created_at = db.sequelize.where( db.sequelize.fn( "DATE", db.sequelize.col( "user_otp.created_at" ) ), req.body.createdAt );
    }
    if ( req.body.updatedAt && req.body.updatedAt.trim() !== "" ) {
      condition.updated_at = db.sequelize.where( db.sequelize.fn( "DATE", db.sequelize.col( "user_otp.updated_at" ) ), req.body.updatedAt );
    }
    if ( req.body.fromDate && req.body.fromDate.trim() !== "" ) {
      condition.created_at = db.sequelize.where( db.sequelize.fn( "DATE", db.sequelize.col( "user_otp.created_at" ) ), ">=", req.body.fromDate );
    }
    if ( req.body.toDate && req.body.toDate.trim() !== "" ) {
      condition.updated_at = db.sequelize.where( db.sequelize.fn( "DATE", db.sequelize.col( "user_otp.updated_at" ) ), "<=", req.body.toDate );
    }

    // Pagination
    if ( req.query.perPage && req.query.page ) {
      if ( typeof parseInt( req.query.perPage ) !== "number" || typeof parseInt( req.query.page ) !== "number" ) {
        return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
      }
      offSet = ( parseInt( req.query.page ) - 1 ) * parseInt( req.query.perPage );
    }


    const searchUserOtp = await user_otp.findAll( {
      "include": [
        {
          "model": user,
          "attributes": [ "username" ],
          "where": conditionRe
        }
      ],
      "where": {
        "$and": condition
      }
    } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!!", { "INFO": req.body, "ERROR": e.original } );
      return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    } );

    if ( req.query.export === "true" ) {
      return res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "TOTAL": searchUserOtp.length, "DATA": searchUserOtp } );
    }
    // eslint-disable-next-line one-var
    const pagination = {
        "total": searchUserOtp.length,
        "perPage": req.query.perPage ? parseInt( req.query.perPage ) : 10,
        "page": req.query.page ? parseInt( req.query.page ) : 1
      },
      dataRes = {
        "userOtp": searchUserOtp.slice( offSet ).slice( 0, req.query.perPage ? parseInt( req.query.perPage ) : 10 ),
        "pagination": pagination
      };

    res.status( 200 ).json( { "CODE": code.SUCCESS, "MESSAGE": msg.MESSAGE_SUCCESS, "DATA": dataRes } );
  }
};
