/**
 * author: hocpv
 * create_at: 18/0892019
 * editor:
 * update_at:
 */

const mongoose = require( "mongoose" ),
  con = mongoose.createConnection( `${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE_PRODUCT}`, {
    "useCreateIndex": true,
    "useNewUrlParser": true,
    "useUnifiedTopology": true
  } ),
  Schema = mongoose.Schema,

  LogSchema = new Schema( {
    "event": String,
    "result": String,
    "component": String,
    "message": String,
    "user": String,
    "requestTime": String,
    "responseTime": String,
    "request": Object,
    "extra": Object,
    "createdAt": {
      "type": Date,
      "default": Date.now()
    },
    "updatedAt": Date
  } );

LogSchema.pre( "save", function( next ) {
  this.updatedAt = Date.now();
  next();
} );

// eslint-disable-next-line one-var
const Log = con.model( "Log", LogSchema, "logs" );

module.exports = Log;
