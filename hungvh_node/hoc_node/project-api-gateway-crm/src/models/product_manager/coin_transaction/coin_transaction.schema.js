const Sequelize = require( "sequelize" );

class CoinTransactionSchema {
  constructor() {
    this.userId = Sequelize.STRING;
    this.deviceId = Sequelize.STRING;
    this.transactionId = Sequelize.STRING;
    this.event = Sequelize.STRING;
    this.productId = Sequelize.STRING;
    this.contentCode = Sequelize.STRING;
    this.coinType = Sequelize.INTEGER;
    this.toUser = Sequelize.STRING;
    this.totalCoinBefore = Sequelize.INTEGER;
    this.amountCoinChange = Sequelize.INTEGER;
    this.totalCoinAfter = Sequelize.INTEGER;
    this.status = Sequelize.INTEGER;
    this.logs = Sequelize.TEXT;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
    this.dtId = Sequelize.INTEGER;
  }
}
module.exports = CoinTransactionSchema;
