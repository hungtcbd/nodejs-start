/* eslint-disable no-param-reassign */
const config = require( "../model" );
const CoinTransactionSchema = require( "./coin_transaction.schema" );
const UUID = require( "../../../helpers/utils/functions/uuid.helper" );
const Sequelize = require( "sequelize" ),
  today = new Date(),
  thisMonth = today.getMonth() + 1 < 10 ? `0${ today.getMonth() + 1}` : today.getMonth() + 1,
  thisYear = today.getFullYear();

class CoinTransactionModel extends Sequelize.Model {}
CoinTransactionModel.init( new CoinTransactionSchema(), config( { "modelName": `coin_transaction_${thisMonth}_${thisYear}` } ) );

class CoinTransactionRepository {
  constructor() {
    this.model = CoinTransactionModel;
  }

  async getCoinTransactions( filter = {}, order = [] ) {
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "ASC" ]
      ];
    }
    let coinTransaction = await this.model.findAll( {
      "where": filter,
      "order": order
    } );
    
    return coinTransaction;
  }

  async createCoinTransaction( user, transaction, order, event, coin, amount, logs ) {
    if ( !transaction ) {
      transaction = UUID.uuid();
    }
    let data = {
        "userId": user.userUuid,
        "deviceId": "",
        "transactionId": transaction,
        "event": event,
        "productId": "",
        "contentCode": "",
        "coinType": "coin",
        "totalCoinBefore": parseInt( coin.currentCoin ),
        "amountCoinChange": amount,
        "totalCoinAfter": parseInt( coin.currentCoin ) + parseInt( amount ),
        "status": 1,
        "logs": logs,
        "dtId": user.dtId
      }, coinTransaction = await this.model.create( data );

    return coinTransaction;
  }
}

module.exports = { CoinTransactionModel, CoinTransactionRepository };
