/* eslint-disable no-param-reassign */
const config = require( "../model" );
const BlackListSchema = require( "./black_list.schema" );
const Sequelize = require( "sequelize" ),
  Op = Sequelize.Op;

class BlackListModel extends Sequelize.Model {}
BlackListModel.init( new BlackListSchema(), config( { "modelName": "black_list" } ) );

class BlackListRepository {
  constructor() {
    this.model = BlackListModel;
  }

  async list( filter = {}, order = [] ) {
    let blackList;

    if ( filter.type === "user" ) {
      filter.username = {
        [ Op.ne ]: null
      };
    } else if ( filter.type === "ip" ) {
      filter.ip = {
        [ Op.ne ]: null
      };
    }
    delete filter.type;
    filter.status = {
      [ Op.ne ]: 3
    };
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "DESC" ]
      ];
    }
    
    blackList = await this.model.findAll( {
      "where": filter,
      "order": order
    } );

    return blackList;
  }

  async create( data ) {
    if ( !data.status ) {
      data.status = 1;
    }
    if ( !data.expiredTime ) {
      let expired = ( new Date() ).getTime() + 10 * 24 * 365 * 3600000;

      data.expiredTime = new Date( expired );
    }
    let blackList = await this.model.create( data );
  
    return blackList;
  }

  async update( model, data ) {
    if ( !data.status ) {
      data.status = 1;
    }
    if ( !data.expiredTime ) {
      let expired = ( new Date() ).getTime() + 10 * 24 * 365 * 3600000;

      data.expiredTime = new Date( expired );
    }
    await model.update( data );
    await model.reload();

    return model;
  }

  async delete( model ) {
    let data = {
      "status": 3
    };

    await model.update( data );
    await model.reload();
    
    return model;
  }

  async getBlackListByIp( ip ) {
    let blackList = await this.model.findOne( {
      "where": {
        "ip": ip,
        "status": {
          [ Op.ne ]: 3
        }
      }
    } );

    return blackList;
  }
  async getBlackListById( id ) {
    let blackList = await this.model.findOne( {
      "where": {
        "id": id,
        "status": {
          [ Op.ne ]: 3
        }
      }
    } );

    return blackList;
  }

  async getBlackListByUsername( username ) {
    let blackList = await this.model.findOne( {
      "where": {
        "username": username,
        "status": {
          [ Op.ne ]: 3
        }
      }
    } );
  
    return blackList;
  }

  async getBlackListByUserId( userId ) {
    let blackList = await this.model.findOne( {
      "where": {
        "userId": userId,
        "status": {
          [ Op.ne ]: 3
        }
      }
    } );
  
    return blackList;
  }
}

module.exports = { BlackListModel, BlackListRepository };
