const Sequelize = require( "sequelize" );

class UserSchema {
  constructor() {
    this.userUuid = {
      "type": Sequelize.BIGINT
    };

    this.username = Sequelize.STRING;
    this.password = {
      "type": Sequelize.STRING
    };
    this.userType = Sequelize.STRING;
    this.fullname = Sequelize.STRING;
    this.birthday = Sequelize.DATEONLY;
    this.hourOfBirth = Sequelize.STRING;
    this.gender = Sequelize.INTEGER;
    this.email = Sequelize.STRING;
    this.phoneNumber = Sequelize.STRING;
    this.lastLogin = Sequelize.DATE;
    this.loginType = Sequelize.STRING;
    this.avatar = Sequelize.TEXT;
    this.socialId = Sequelize.STRING;
    this.socialToken = Sequelize.STRING;
    this.telcoId = Sequelize.INTEGER;
    this.activeAt = Sequelize.FLOAT;
    this.lastSubscribeAt = Sequelize.DATE;
    this.lastUnsubscribeAt = Sequelize.DATE;
    this.lastRenewAt = Sequelize.DATE;
    this.lastTransactionAt = Sequelize.DATE;
    this.lastTransactionType = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
    this.expiredTime = Sequelize.DATE;
    this.logs = Sequelize.TEXT;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
    this.dtId = Sequelize.INTEGER;
  }
}
module.exports = UserSchema;
