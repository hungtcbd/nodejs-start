const Sequelize = require( "sequelize" );

class ConfigSchema {
  constructor() {
    this.id = {
      "type": Sequelize.INTEGER,
      "primaryKey": true
    };
    this.name = Sequelize.STRING;
    this.value = Sequelize.STRING;
    this.type = Sequelize.STRING;
    this.description = Sequelize.TEXT;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}
module.exports = ConfigSchema;
