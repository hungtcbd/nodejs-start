const Sequelize = require( "sequelize" );

class CoinTopupSchema {
  constructor() {
    this.requestId = Sequelize.STRING;
    this.userId = Sequelize.STRING;
    this.amount = Sequelize.STRING;
    this.reason = Sequelize.TEXT;
    this.fromUser = Sequelize.TEXT;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}
module.exports = CoinTopupSchema;
