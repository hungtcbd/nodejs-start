const Sequelize = require( "sequelize" );

class ResourceSchema {
  constructor() {
    this.appId = Sequelize.INTEGER;
    this.source = Sequelize.STRING;
    this.name = Sequelize.STRING;
    this.resourceCode = Sequelize.STRING;
    this.param = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}

module.exports = ResourceSchema;
