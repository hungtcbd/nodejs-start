const Sequelize = require( "sequelize" );

class AppVersionSchema {
  constructor() {
    this.id = {
      "type": Sequelize.INTEGER,
      "primaryKey": true,
      "autoIncrement": true
    };
    this.appId = Sequelize.INTEGER;
    this.name = Sequelize.STRING;
    this.version = Sequelize.STRING;
    this.description = Sequelize.STRING;
    this.changeLog = Sequelize.TEXT;
    this.file = Sequelize.TEXT;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}
module.exports = AppVersionSchema;
