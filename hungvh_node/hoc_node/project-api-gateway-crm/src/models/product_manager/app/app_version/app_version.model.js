/* eslint-disable global-require */
/* eslint-disable no-param-reassign */
const config = require( "../../model" );
const Sequelize = require( "sequelize" );
const AppVersionSchema = require( "./app_version.schema" ),
  Op = Sequelize.Op;

class AppVersionModel extends Sequelize.Model {}
AppVersionModel.init( new AppVersionSchema(), config( { "modelName": "app_version" } ) );

class AppVersionRepository {
  constructor() {
    this.model = AppVersionModel;
  }

  async list( filter, order = [], withApp = false ) {
    let include = [], appVersion;

    if ( filter.status ) {
      filter.status = Object.assign( filter.status, { [ Op.ne ]: 3 } );
    } else {
      filter.status = { [ Op.ne ]: 3 };
    }
    if ( withApp ) {
      const { AppModel } = require( "../app/app.model" );

      this.model.belongsTo( AppModel );
      include = [ {
        "model": AppModel
      } ];
    }
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "DESC" ]
      ];
    }
    appVersion = await this.model.findAll( {
      "where": filter,
      "order": order,
      "include": include
    } );
    return appVersion;
  }

  async create( data ) {
    if ( !data.status ) {
      data.status = 1;
    }

    let version = await this.model.create( data );

    return version;
  }

  async update( version, data ) {
    
    await version.update( data );
    await version.reload();

    return version;
  }

  async delete( version ) {
    
    let data = {
      "status": 3
    };

    await version.update( data );
    await version.reload();
  
    return version;
  }

  async getAppVersionById( appId, appVersionId ) {
    let appVersion;

    appVersion = await this.model.findOne( {
      "where": {
        "id": appVersionId,
        "appId": appId,
        "status": {
          [ Op.ne ]: 3
        }
      }
    } );
    return appVersion;
  }
}

module.exports = { AppVersionModel, AppVersionRepository };
