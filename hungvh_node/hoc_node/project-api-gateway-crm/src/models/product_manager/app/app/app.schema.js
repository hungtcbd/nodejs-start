const Sequelize = require( "sequelize" );

class AppSchema {
  constructor() {
    this.id = {
      "type": Sequelize.INTEGER,
      "autoIncrement": true,
      "primaryKey": true
    };
    this.appName = Sequelize.STRING;
    this.appSlug = Sequelize.STRING;
    this.appDescription = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
  }
}

module.exports = AppSchema;
