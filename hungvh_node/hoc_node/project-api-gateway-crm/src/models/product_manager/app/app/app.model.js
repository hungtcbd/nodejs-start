/* eslint-disable no-param-reassign */
const config = require( "../../model" );
const Sequelize = require( "sequelize" );
const AppSchema = require( "./app.schema" );
const { AppVersionModel } = require( "../app_version/app_version.model" );
const { ResourceModel } = require( "../app_resource/resource.model" );
const slugify = require( "slugify" ),
  Op = Sequelize.Op;

class AppModel extends Sequelize.Model {}
AppModel.init( new AppSchema(), config( { "modelName": "apps" } ) );
AppModel.hasMany( AppVersionModel, { "foreignKey": "app_id", "sourceKey": "id" } );
AppModel.hasMany( ResourceModel, { "foreignKey": "app_id", "sourceKey": "id" } );
class AppRepository {
  constructor() {
    this.model = AppModel;
  }
  async list( filter, order = [], withVersion = false ) {
    let include = [], apps;

    filter.status = {
      [ Op.ne ]: 3
    };
    if ( withVersion ) {
      include.push( {
        "model": AppVersionModel,
        "where": {
          "status": 1
        },
        "required": false
      } );
      include.push( {
        "model": ResourceModel,
        "where": {
          "status": 1
        },
        "required": false
      } );
    }
    if ( order.length === 0 ) {
      order = [
        [ "created_at", "DESC" ]
      ];
    }
    
    apps = await this.model.findAll( {
      "where": filter,
      "order": order,
      "include": include
    } );

    return apps;
  }

  async create( data ) {
    data.appSlug = slugify( data.appName );
    if ( !data.status ) {
      data.status = 1;
    }
    let app = await this.model.create( data );

    return app;
  }

  async update( app, data ) {
   
    await app.update( data );
    await app.reload();

    return app;
  }

  async delete( app ) {
    
    data = {
      "status": 3
    };
    await app.update( data );
    await app.reload();
  
    return app;
  }

  async getAppById( appId, withVersion = false ) {
    let where, include = [], app;

    where = {
      "id": appId
    };

    if ( withVersion ) {
      include = [ {
        "model": AppVersionModel,
        "where": {
          "status": 1
        }
      } ];
    }

    app = await this.model.findOne( {
      "where": where,
      "include": include
    } );
    return app;
  }
}

module.exports = { AppModel, AppRepository };
