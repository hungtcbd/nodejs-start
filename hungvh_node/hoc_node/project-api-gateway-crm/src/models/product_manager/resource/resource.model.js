const config = require( "../model" );
const ResourceSchema = require( "./resource.schema" );
const Sequelize = require( "sequelize" );

class ResourceModel extends Sequelize.Model {}
ResourceModel.init( new ResourceSchema(), config( { "modelName": "resources" } ) );

class ResourceRepository {
  constructor() {
    this.model = ResourceModel;
  }

  async getResources( filter ) {
    let resources = await this.model.findAll( {
      "where": filter
    } );

    return resources;
  }

  async createResource( resourceCode, param = "", appId = 1 ) {
    let data = {
        "resourceCode": resourceCode,
        "param": param,
        "appId": appId,
        "status": 1
      },
      resource = await this.model.create( data );

    return resource;
  }

  async updateResource ( resourceId, data ) {
    let resource = await this.model.update( data, {
      "where": {
        "id": resourceId
      }
    } );

    return resource;
  }
}

module.exports = { ResourceModel, ResourceRepository };
