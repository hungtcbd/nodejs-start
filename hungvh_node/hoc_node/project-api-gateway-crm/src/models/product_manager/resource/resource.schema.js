const Sequelize = require( "sequelize" );

class ResourceSchema {
  constructor() {
    this.appId = Sequelize.INTEGER;
    this.resourceCode = Sequelize.STRING;
    this.param = Sequelize.STRING;
    this.status = Sequelize.INTEGER;
    this.createAt = Sequelize.DATE;
    this.updateAt = Sequelize.DATE;
  }
}

module.exports = ResourceSchema;
