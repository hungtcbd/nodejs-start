const config = require( "../model" );
const UserLaSoSchema = require( "./user_laso.schema" );
const Sequelize = require( "sequelize" );

class UserLaSo extends Sequelize.Model {}
UserLaSo.init( new UserLaSoSchema(), config( { "modelName": "user_la_so" } ) );

class LaSoRepository {
  constructor() {
    this.model = UserLaSo;
  }

  async getLaSo( userId, type = 0 ) {
    let where = {
        "userUuid": userId
      },
      laSo = [];

    if ( type !== 0 ) {
      where.type = type;
    }
    laSo = await this.model.findAll( {
      "where": where
    } );
    return laSo;
  }
}
module.exports = { LaSoRepository, UserLaSo };
