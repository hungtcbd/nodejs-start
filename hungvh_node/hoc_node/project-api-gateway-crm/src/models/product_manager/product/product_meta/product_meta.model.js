const config = require( "../../model" );
const ProductMetaSchema = require( "./product_meta.schema" );
const Sequelize = require( "sequelize" );

class ProductMetaModel extends Sequelize.Model {}
ProductMetaModel.init( new ProductMetaSchema(), config( { "modelName": "product_metas" } ) );
class ProductMetaRepository {

}

module.exports = { ProductMetaModel, ProductMetaRepository };
