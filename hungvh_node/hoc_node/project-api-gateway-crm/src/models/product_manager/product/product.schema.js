const Sequelize = require( "sequelize" );

class ProductSchema {
  constructor() {
    this.productId = {
      "type": Sequelize.UUID,
      "defaultValue": Sequelize.UUIDV4
    };
    this.appId = Sequelize.INTEGER;
    this.packageCode = Sequelize.STRING;
    this.planId = Sequelize.STRING;
    this.name = Sequelize.STRING;
    this.slug = Sequelize.STRING;
    this.description = Sequelize.STRING;
    this.type = Sequelize.STRING;
    this.price = Sequelize.STRING;
    this.priceMoney = Sequelize.STRING;
    this.time = Sequelize.STRING;
    this.duration = Sequelize.INTEGER;
    this.status = Sequelize.INTEGER;
    this.createdAt = Sequelize.DATE;
    this.updatedAt = Sequelize.DATE;
  }
}

module.exports = ProductSchema;
