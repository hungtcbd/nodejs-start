/* eslint-disable no-param-reassign */
const config = require( "../model" );
const ProductSchema = require( "./product.schema" );
const Sequelize = require( "sequelize" );
const { ProductMetaModel } = require( "./product_meta/product_meta.model" );
const { ResourceRepository } = require( "../app/app_resource/resource.model" );
const slugify = require( "slugify" );

class ProductModel extends Sequelize.Model {}
ProductModel.init( new ProductSchema(), config( { "modelName": "products" } ) );
ProductModel.hasMany( ProductMetaModel, { "foreignKey": "product_id", "sourceKey": "productId" } );

class ProductRepository {
  constructor() {
    this.model = ProductModel;
  }

  async getProductById( productId ) {
    let product = await this.model.findOne( {
      "where": {
        "productId": productId
      },
      "include": {
        "model": ProductMetaModel,
        "attributes": [ "key", "value" ],
        "where": { "status": 1 }
      }
    } );

    return product;
  }

  async getProducts( filter ) {
    let products = await this.model.findAll( {
      "where": filter,
      "include": {
        "model": ProductMetaModel,
        "attributes": [ "key", "value" ],
        "where": { "status": 1 }
      }
    } );

    return products;
  }
  /**
   *
   * @param {*} data
   */
  async createProduct( data ) {
    let product, resourceRepo = new ResourceRepository();

    if ( !data.packageCode || !data.name || !data.price || !data.resources ) {
      return null;
    }
    try {
      data.slug = slugify( data.name );
      product = await this.model.create( data );

      await product.reload();
      let resources = {};

      data.resources = JSON.parse( data.resources );
     
      if ( Array.isArray( data.resources ) ) {
        for ( let value of data.resources ) {
          let resource = await resourceRepo.getResourceById( value );

          if ( resource ) {
            if ( resources[ resource.source ] ) {
              resources[ resource.source ].push( resource.resourceCode );
            } else {
              resources[ resource.source ] = [];
              resources[ resource.source ].push( resource.resourceCode );
            }
          }
        }

      }
      await ProductMetaModel.create( {
        "productId": product.productId,
        "key": "resource",
        "value": JSON.stringify( resources ),
        "status": 1
      } );
  
      if ( data.default ) {
        await ProductMetaModel.create( {
          "productId": product.productId,
          "key": "default",
          "value": true,
          "status": 1
        } );
      }

      if ( data.promotion ) {
        await ProductMetaModel.create( {
          "productId": product.productId,
          "key": "promotion",
          "value": data.promotion,
          "status": 1
        } );
      }

      if ( data.manyTime ) {
        await ProductMetaModel.create( {
          "productId": product.productId,
          "key": "many_time",
          "value": data.manyTime,
          "status": 1
        } );
      }
  
    } catch ( e ) {
      console.log( e );
      product = null;
    }
    return product;
  }

  async updateProduct( product, data ) {

    if ( !data.packageCode || !data.name || !data.price || !data.resources ) {
      return null;
    }
    try {
      data.slug = slugify( data.name );
      await product.update( data );

      await product.reload();
  
      data.resources = JSON.parse( data.resources );
      let resources = {}, resourceRepo = new ResourceRepository();

      if ( Array.isArray( data.resources ) ) {
        for ( let value of data.resources ) {
          let resource = await resourceRepo.getResourceById( value );

          if ( resource ) {
            if ( resources[ resource.source ] ) {
              resources[ resource.source ].push( resource.resourceCode );
            } else {
              resources[ resource.source ] = [];
              resources[ resource.source ].push( resource.resourceCode );
            }
          }
        }

      }
      await ProductMetaModel.findOne( {
        "where": {
          "productId": product.productId,
          "key": "resource"
        }
      } ).then( ( meta ) => {
        meta.update( {
          "value": JSON.stringify( resources )
        } );
      } );
  
      if ( data.default ) {
        await ProductMetaModel.findOne( {
          "where": {
            "productId": product.productId,
            "key": "default"
          }
        } ).then( ( meta ) => {
          meta.update( { "value": data.default } );
        } );
       
      }

      if ( data.promotion ) {
        await ProductMetaModel.findOne( {
          "where": {
            "productId": product.productId,
            "key": "promotion"
          }
        } ).then( ( meta ) => {
          if ( meta ) {
            console.log( meta );
            meta.update( { "value": data.promotion } );
          } else {
            ProductMetaModel.create( {
              "productId": product.productId,
              "key": "promotion",
              "value": data.promotion,
              "status": 1
            } );
          }
        } ).catch( ( e ) => {
          console.log( e );
        } );
      }

      if ( data.manyTime ) {
        await ProductMetaModel.findOne( {
          "where": {
            "productId": product.productId,
            "key": "many_time"
          }
        } ).then( ( meta ) => {
          if ( meta ) {
            console.log( meta );
            meta.update( { "value": data.manyTime } );
          } else {
            ProductMetaModel.create( {
              "productId": product.productId,
              "key": "many_time",
              "value": data.manyTime,
              "status": 1
            } );
          }
        } ).catch( ( e ) => {
          console.log( e );
        } );
      }
  
    } catch ( e ) {
      console.log( e );
      product = null;
    }
    return product;
  }

  async updateProductStatus( product, status ) {
    await product.update( { "status": status } );
    product.reload();
    return product;
  }

  async getProductByPackageCode( packageCode ) {
    let product = await this.model.findOne( {
      "where": {
        "packageCode": packageCode
      },
      "include": {
        "model": ProductMetaModel,
        "attributes": [ "key", "value" ],
        "where": { "status": 1 }
      }
    } );

    return product;
  }
}
module.exports = { ProductRepository, ProductModel };
