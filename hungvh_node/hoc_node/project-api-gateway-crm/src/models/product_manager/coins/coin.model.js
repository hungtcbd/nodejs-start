const config = require( "../model" );
const CoinSchema = require( "./coin.schema" );
const Sequelize = require( "sequelize" );

class CoinModel extends Sequelize.Model {}
CoinModel.init( new CoinSchema(), config( { "modelName": "coins" } ) );

class CoinRepository {
  constructor() {
    this.model = CoinModel;
  }

  async getCoinBalance( userId ) {
    let userCoin = await this.model.findOne( {
      "where": {
        "user_id": userId
      }
    } );

    if ( !userCoin ) {
      userCoin = await this.model.create( {
        "userId": userId,
        "totalCoin": 0,
        "currentCoin": 0,
        "totalCoinLock": 0,
        "currentCoinLock": 0
      } );
    }
    userCoin.reload();

    return userCoin;
  }

  async addCoin( userId, amount, transactionId = "" ) {
    let currentCoin = await this.getCoinBalance( userId ),
      coin = await currentCoin.update( {
        "totalCoin": parseInt( currentCoin.totalCoin ) + parseInt( amount ),
        "currentCoin": parseInt( currentCoin.currentCoin ) + parseInt( amount ),
        "lastTransaction": transactionId
      } );

    return coin;
  }
}
module.exports = { CoinRepository, CoinModel };
