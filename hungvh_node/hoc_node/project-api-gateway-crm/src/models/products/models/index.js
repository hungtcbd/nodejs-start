"use strict";

const fs = require( "fs" );
const path = require( "path" );

const Sequelize = require( "sequelize" ),
  basename = path.basename( __filename ),
  db = {},
  Op = Sequelize.Op,
  operatorsAliases = {
    "$eq": Op.eq,
    "$ne": Op.ne,
    "$gte": Op.gte,
    "$gt": Op.gt,
    "$lte": Op.lte,
    "$lt": Op.lt,
    "$not": Op.not,
    "$in": Op.in,
    "$notIn": Op.notIn,
    "$is": Op.is,
    "$like": Op.like,
    "$notLike": Op.notLike,
    "$iLike": Op.iLike,
    "$notILike": Op.notILike,
    "$regexp": Op.regexp,
    "$notRegexp": Op.notRegexp,
    "$iRegexp": Op.iRegexp,
    "$notIRegexp": Op.notIRegexp,
    "$between": Op.between,
    "$notBetween": Op.notBetween,
    "$overlap": Op.overlap,
    "$contains": Op.contains,
    "$contained": Op.contained,
    "$adjacent": Op.adjacent,
    "$strictLeft": Op.strictLeft,
    "$strictRight": Op.strictRight,
    "$noExtendRight": Op.noExtendRight,
    "$noExtendLeft": Op.noExtendLeft,
    "$and": Op.and,
    "$or": Op.or,
    "$any": Op.any,
    "$all": Op.all,
    "$values": Op.values,
    "$col": Op.col
  },
  sequelize = new Sequelize( {
    "username": process.env.DB_SQL_USER,
    "password": process.env.DB_SQL_PASSWORD,
    "database": process.env.DB_SQL_NAME,
    "host": process.env.DB_SQL_HOST,
    "dialect": "mysql",
    "operatorsAliases": operatorsAliases,
    "timezone": "+07:00",
    "logging": false
  } );

fs
  .readdirSync( __dirname )
  .filter( ( file ) => {
    return ( file.indexOf( "." ) !== 0 ) && ( file !== basename ) && ( file.slice( -3 ) === ".js" );
  } )
  .forEach( ( file ) => {
    const model = sequelize.import( path.join( __dirname, file ) );

    db[ model.name ] = model;
  } );

Object.keys( db ).forEach( ( modelName ) => {
  if ( db[ modelName ].associate ) {
    db[ modelName ].associate( db );
  }
} );

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
