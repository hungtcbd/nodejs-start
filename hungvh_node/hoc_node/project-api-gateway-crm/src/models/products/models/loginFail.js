"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const login_fail = sequelize.define( "login_fail", {
    "device_id": DataTypes.STRING,
    "login_fail_number": DataTypes.INTEGER,
    "login_fail_time": DataTypes.INTEGER,
    "ip": DataTypes.STRING,
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false } );

  // eslint-disable-next-line no-unused-vars
  login_fail.associate = function( models ) {
    // associations can be defined here
  };
  return login_fail;
};

