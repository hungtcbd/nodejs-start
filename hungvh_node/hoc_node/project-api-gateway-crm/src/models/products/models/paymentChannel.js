"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const PaymentChannel = sequelize.define( "payment_channel", {
    "channel_code": DataTypes.STRING,
    "name": DataTypes.STRING,
    "slug": DataTypes.STRING,
    "description": DataTypes.STRING,
    "status": DataTypes.INTEGER,
    "created_at": {
      "type": DataTypes.DATE,
      "default": Date.now(),
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      "default": Date.now(),
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false, "freezeTableName": true } );

  // eslint-disable-next-line no-unused-vars
  PaymentChannel.associate = function( models ) {
    // associations can be defined here
  };
  return PaymentChannel;
};

