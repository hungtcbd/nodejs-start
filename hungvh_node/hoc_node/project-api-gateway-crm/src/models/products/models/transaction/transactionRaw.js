"use strict";
const db = require( "../../models" );
const logger = require( "../../../../helpers/services/log.service" );

module.exports = {
  "getTransactionInMonth": async ( table, condition ) => {
    // Fetch key and value condition
    const arrCon = Object.keys( condition ).filter( ( item ) => item !== "month" && item !== "year" && ( condition[ item ] ) !== "" ),
      valueCon = await Promise.all( arrCon.map( ( value ) => {
        return condition[ value ];
      } ) );

    // Create query default sql
    let sql = `SELECT ${table}.transaction_id,  users.username, ${table}.amount,  ${table}.package_code,  ${table}.payment_channel_code, ${table}.status, 
      DATE_FORMAT(${table}.created_at, '%Y-%m-%d %H:%i:%s') AS created_at
      FROM ${table} 
      INNER JOIN users ON users.user_uuid=${table}.user_id
      WHERE ${table}.type<>'ADD'`;

    // Check if not filter
    if ( arrCon.length === 0 ) {
      sql = `${sql } ORDER BY ${table}.created_at DESC`;
      const result = db.sequelize.query( sql, { "type": db.sequelize.QueryTypes.SELECT } ).catch( ( e ) => {
        logger.log( "Có vấn đề khi truy vấn tới database!", { "INFO": condition, "ERROR": e.original } );
      } ) ;


      return result;
    }
    // If filter
    arrCon.map( ( con ) => {
      if ( condition[ con ] !== "" ) {
        if ( con === "username" ) {
          sql = `${sql } AND users.username=?  `;
        } else if ( con === "fromDate" ) {
          sql = `${sql } AND DATE(${table}.created_at) >= ? `;
        } else if ( con === "toDate" ) {
          sql = `${sql } AND DATE(${table}.updated_at) <= ? `;
        } else if ( con === "date" ) {
          sql = `${sql } AND DATE(${table}.created_at) = ? `;
        } else if ( con === "package_code" || con === "payment_channel_code" || con === "amount" || con === "status" ) {
          sql = `${sql } AND ${table}.${ con } = ? `;
        }
      }
    } );
    // eslint-disable-next-line one-var
    const result = db.sequelize.query( `${sql } ORDER BY ${table}.created_at DESC`, { "replacements": valueCon, "type": db.sequelize.QueryTypes.SELECT } ).catch( ( e ) => {
      logger.log( "Có vấn đề khi truy vấn tới database!", { "INFO": condition, "ERROR": e.original } );
    } ) ;

    return result;
  }
};

