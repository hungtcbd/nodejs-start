"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const productFeedback = sequelize.define( "product_feedbacks", {
    "user_uuid": DataTypes.STRING,
    "subject": DataTypes.STRING,
    "msg": DataTypes.TEXT,
    "reply": DataTypes.TEXT,
    "logging": DataTypes.TEXT,
    "reply_logging": DataTypes.TEXT,
    "device_id": DataTypes.STRING,
    "status": DataTypes.BOOLEAN,
    "type": DataTypes.INTEGER,
    "star": DataTypes.INTEGER,
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false } );

  // eslint-disable-next-line no-unused-vars
  productFeedback.associate = function( models ) {
    // associations can be defined here
    productFeedback.belongsTo( models.user, {
      "foreignKey": "user_uuid",
      "targetKey": "user_uuid"
    } );
  };
  return productFeedback;
};

