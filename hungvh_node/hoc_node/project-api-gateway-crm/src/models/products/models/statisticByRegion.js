"use strict";
const moment = require( "moment" );

module.exports = ( sequelize, DataTypes ) => {
  const StatisticByRegion = sequelize.define( "report_user_by_region", {
    "dt_id": DataTypes.INTEGER, // mã đối tác
    "device_type_id": DataTypes.INTEGER, // mã loại thiết bị mobile,web,tv,...
    "region_id": DataTypes.INTEGER, // mã vùng...
    "platform_id": DataTypes.INTEGER, // nền tảng sử dụng: android, ios, window, macos,linux
    "total_user": DataTypes.INTEGER, // tổng số lượng user (cộng dồn qua tất cả các ngày)
    "total_active_user": DataTypes.INTEGER, // tổng số active user(vip user còn hạn) cộng dồn qua các ngày
    "total_open_app": DataTypes.INTEGER, // tổng số mở app
    "total_new_user": DataTypes.INTEGER, // tổng số user tạo mới trong ngày
    "date": {
      "type": DataTypes.DATE, // ngày thống kê
      get() {
        return moment( this.getDataValue( "date" ) ).format( "YYYY-MM-DD" );
      }
    },
    "created_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "created_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    },
    "updated_at": {
      "type": DataTypes.DATE,
      get() {
        return moment( this.getDataValue( "updated_at" ) ).format( "YYYY-MM-DD H:mm:ss" );
      }
    }
  }, { "timestamps": false, "freezeTableName": true } );

  // eslint-disable-next-line no-unused-vars
  StatisticByRegion.associate = function( models ) {
    // associations can be defined here
  };


  return StatisticByRegion;
};

