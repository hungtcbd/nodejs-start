const mongoose = require( "mongoose" );
const apiKeySchema = require( "./ApiKeySchema" );

class APIKeyModel {
  constructor() {
    this.model = mongoose.model( "api_keys", apiKeySchema );
  }
  
  async all() {
   
    let allData = await this.model.find( {} ).lean().exec();

    return allData;
  }

  async getAPIKey( userRequest ) {
    let apiKey = null;

    try {
      apiKey = await this.model.findOne( { "userRequest": userRequest } ).lean().exec();
    } catch ( e ) {
      apiKey = null;
    }
    return apiKey;
  }

  
}
module.exports = APIKeyModel ;
