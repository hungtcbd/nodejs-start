/**
 * Module route search Transaction
 * Creator: hocpv
 * Editor:
 * CreateAt: 09/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const TransactionController = require( "../../controllers/search/transaction.controller" );

router.route( "/" ).post( TransactionController.index );
// router.route( "/export" ).post( TransactionController.export );

module.exports = router;
