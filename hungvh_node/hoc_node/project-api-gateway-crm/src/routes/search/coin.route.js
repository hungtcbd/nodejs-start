/**
 * Module route search history coin
 * Creator: hocpv
 * Editor:
 * CreateAt: 11/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const CoinController = require( "../../controllers/search/coin.controller" );

router.route( "/" ).post( CoinController.index );
// router.route( "/export" ).post( CoinController.export );

module.exports = router;
