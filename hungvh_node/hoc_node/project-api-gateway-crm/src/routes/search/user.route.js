/**
 * Module route search User
 * Creator: hocpv
 * Editor:
 * CreateAt: 04/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const UserController = require( "../../controllers/search/user.controller" );

router.route( "/logged" ).post( UserController.logged );
router.route( "/not-logged" ).post( UserController.notLogged );

module.exports = router;
