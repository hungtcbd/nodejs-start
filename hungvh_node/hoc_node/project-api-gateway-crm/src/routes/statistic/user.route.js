/**
 * Module route statistic user detail
 * Creator: hocpv
 * Editor:
 * CreateAt: 10/10/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const UserController = require( "../../controllers/statistic/user.controller" );

router.route( "/ccu" ).get( UserController.ccu );
router.route( "/new-user-day" ).get( UserController.newUserByDay );
router.route( "/new-user-partner" ).get( UserController.newUserByPartner );
router.route( "/active-user-day" ).get( UserController.activeUserByDay );
router.route( "/active-user-device" ).get( UserController.activeUserByDevice );
router.route( "/active-user-partner" ).get( UserController.activeUserByPartner );
router.route( "/total-unsub" ).get( UserController.unSubByDay );
router.route( "/total-unsub-device" ).get( UserController.unSubByDevice );
router.route( "/total-unsub-partner" ).get( UserController.unSubByPartner );
router.route( "/renew-success" ).get( UserController.renewSuccessByDay );
router.route( "/renew-success-device" ).get( UserController.renewSuccesByDevice );
router.route( "/renew-success-partner" ).get( UserController.renewSuccessByPartner );

module.exports = router;
