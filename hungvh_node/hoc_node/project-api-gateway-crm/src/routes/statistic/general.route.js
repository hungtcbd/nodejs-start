/**
 * Module route statistic general
 * Creator: hocpv
 * Editor:
 * CreateAt: 30/09/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const GeneralController = require( "../../controllers/statistic/general.controller" );

// Tab statistic user
router.route( "/ccu" ).get( GeneralController.ccu );
router.route( "/new-user-day" ).get( GeneralController.newUserByDay );
router.route( "/active-user-day" ).get( GeneralController.activeUserByDay );
router.route( "/new-user-partner" ).get( GeneralController.newUserByPartner );
router.route( "/active-user-device" ).get( GeneralController.activeUserByDevice );
router.route( "/active-user-partner" ).get( GeneralController.activeUserByPartner );

// Tab statistic  device
router.route( "/open-app" ).get( GeneralController.openApp );
router.route( "/new-device" ).get( GeneralController.newDevice );
router.route( "/new-device-day" ).get( GeneralController.newDeviceByDay );
router.route( "/new-device-partner" ).get( GeneralController.newDeviceByPartner );
router.route( "/active-device-day" ).get( GeneralController.activeDeviceByDay );
router.route( "/active-device-device" ).get( GeneralController.activeDeviceByDevice );
router.route( "/active-device-region" ).get( GeneralController.activeDeviceByRegion );

// Tab statistic revenue
router.route( "/revenue-day" ).get( GeneralController.revenueByDay );
router.route( "/revenue-partner" ).get( GeneralController.revenueByPartner );


module.exports = router;
