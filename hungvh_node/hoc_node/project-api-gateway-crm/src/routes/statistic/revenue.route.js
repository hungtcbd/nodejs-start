/**
 * Module route statistic revenue detail
 * Creator: hocpv
 * Editor:
 * CreateAt: 10/10/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const RevenueController = require( "../../controllers/statistic/revenue.controller" );

router.route( "/revenue-amount" ).get( RevenueController.revenueByAmount );
router.route( "/revenue-money" ).get( RevenueController.revenueBySourceMoney );
router.route( "/revenue-partner" ).get( RevenueController.revenueByPartner );
router.route( "/revenue-device" ).get( RevenueController.revenueByDevice );
router.route( "/revenue-platform" ).get( RevenueController.revenueByPlatform );
router.route( "/" ).get( RevenueController.revenueByDay );

module.exports = router;
