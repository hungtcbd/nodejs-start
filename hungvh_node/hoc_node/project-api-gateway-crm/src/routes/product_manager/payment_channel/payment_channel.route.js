const router = require( "express-promise-router" )();
const PaymentChannelController = require( "../../../controllers/product_manager/payment_channel/payment_channel.controller" );

router.route( "/" ).get( PaymentChannelController.index );
router.route( "/" ).post( PaymentChannelController.create );
router.route( "/:id" ).patch( PaymentChannelController.update );
router.route( "/:id" ).delete( PaymentChannelController.delete );

module.exports = router;
