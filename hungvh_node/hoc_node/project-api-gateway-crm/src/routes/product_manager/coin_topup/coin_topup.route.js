const router = require( "express-promise-router" )();
const CoinTopupController = require( "../../../controllers/product_manager/coin_topup/coin_topup.controller" );

router.route( "/" ).get( CoinTopupController.list );
router.route( "/topup" ).post( CoinTopupController.coinTopup );
module.exports = router;
