const router = require( "express-promise-router" )();
const UserController = require( "../../../controllers/product_manager/users/user.controller" );

router.route( "/" ).get( UserController.list );
router.route( "/:userId" ).put( UserController.update );
router.route( "/:userId/status" ).put( UserController.status );
router.route( "/:userId" ).delete( UserController.delete );
router.route( "/reset-password/:userId" ).get( UserController.resetPassword );
router.route( "/export" ).get( UserController.export );
router.route( "/" ).post( UserController.create );

module.exports = router;
