const router = require( "express-promise-router" )();
const SystemConfigController = require( "../../../controllers/product_manager/system_config/system_config.controller" );

router.route( "/" ).get( SystemConfigController.list );
router.route( "/" ).post( SystemConfigController.create );
router.route( "/:configId" ).get( SystemConfigController.show );
router.route( "/:configId" ).put( SystemConfigController.update );
router.route( "/:configId" ).delete( SystemConfigController.delete );

module.exports = router;
