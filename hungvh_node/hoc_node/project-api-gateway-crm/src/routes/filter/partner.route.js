/**
 * Module route Partner
 * Creator: hocpv
 * Editor:
 * CreateAt: 14/10/19
 * UpdateAt:
 *
 */

const router = require( "express-promise-router" )();
const PartnerController = require( "../../controllers/filter/partner.controller" );

router.route( "/" ).get( PartnerController.index );

module.exports = router;
