## Quy định phản hồi đầu ra ##

### 1. Thông tin chung ###
- URL: http://...../api/v1
- URL Doc API:  http://...../api-docs



### 2. Quy định mã phản hồi đầu ra ###

 - Đầu ra là 1 json dạng:<br>
 Success
 
```json 
{ 
    "result": {
        "CODE": "",
        "MESSAGE": ""
    }    
}
```

Fail
 
```json 
{ 
    "error": {
        "CODE": "",
        "MESSAGE": ""
    }    
}
```


		
| STT    | Field | Mô tả | Required 
| :---------: | :------| --------- | :-----:|
|   1  | CODE | Mã phản hồi  | x |
|   2     |   MESSAGE | Mô tả phản hồi  | x
|   3     |   DATA | Dữ liệu trả thêm nếu có (tùy theo API) |

**Bảng quy định mã phản hồi**

| CODE  | MESSAGE |
| :----------: | :---------|
| 0     | Thành công
| 1     | Thất bại
| 101   | Máy chủ thủ đô đang hoạt động có vấn đề!
| 102   | Máy chủ product đang hoạt động có vấn đề!
| 103   | Yêu cầu đã tồn tại!
| 104   | Yêu cầu không được tìm thấy!
| 105   | Sai chữ ký xác thực!
| 2     | Thiếu tham số yêu cầu!
