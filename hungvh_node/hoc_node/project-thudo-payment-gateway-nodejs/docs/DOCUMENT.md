# Tài liệu tổng quan PKD Payment Gateway

### 1. Mô tả khái quát

- Ngôn ngữ: NodeJS
- Framework: ExpressJS 
- Phương thức kết nối DB: RESTfull API
- DB sử dụng: MongoDB
- Hệ thống lưu log: GrayLog

### 2. Luồng hoạt động

![PGD-Payment-Gateway.png](https://i.postimg.cc/J7p9Mh3B/PGD-Payment-Gateway.png)

1. User truy cập vào server product và tiến hành giao dịch
   - Server Product gửi yêu cầu giao dịch của user tới server PKD
   - Sever PKD xử lý, lưu thông t giao dịch và tiến hành gửi yêu cầu giao dịch của user tới server Thudo
   - Server Thudo xử lý yêu cầu giao dịch và trả về 1 URL cho server PKD 
   - Server PKD nhận URL từ server Thudo và trả về cho server Product để hiện thị Web View <br>
   => Kết thúc tiến trình 1.  
2. Submit và thông báo
   - Sau khi nhận URL và hiển thị Web View trên server Product user tiến hành xác nhận thanh toán 
   - Việc xác nhận thanh toán được server Product gửi yêu cầu qua server Thudo xử lý 
   - Sau khi xử lý thành công server Thudo sẽ gửi thông báo tới serve PKD 
   - Server PKD nhận thông báo từ server Thudo tiến hành cập nhật lại thông tin giao dịch và gửi lại thông báo giao dịch thành công (thất bại) cho server Product <br>
   => Kết thúc tiến trình 2


### 3. Cài đặt và sử dụng hệ thống

**Cài đặt:**
   - Nodejs (v10 hoặc mới hơn)
   - MongoDB (v4 hoặc mới hơn)
   - Graylog (v2 hoặc mới hơn)
   - Cài đặt các gói node_module yêu cầu: sử dụng câu lệnh bên dưới để cài đặt tự động
   ```sh
   $ npm install or npm i
   ``` 
   - Nhập cơ sở dữ liệu mẫu: tham khảo tại [link](https://bitbucket.org/pkt-tv-payment-gateway/project-thudo-payment-gateway-nodejs/src/hocpv/db_example/import.md) <br>
   
**Sử dụng:**
  - Khởi chạy hệ thống: sử dụng câu lệnh bên dưới
  ```sh
  $ npm run serve
  ``` 
  - Kiểm thử hệ thống: sử dụng câu lệnh bên dưới  
  ```sh
  $ npm run test
  ```
### 4. Kết nối 

#### Kết nối với Gateway Thudo
 **API Check User:**
 - API: https://api-lichcatnhat.gviet.vn/thudo/v1/check-user
 - Method: get
 - Request:	
 		
 | Params    | Required | DataType | Example | Note
 | --------- | -----:| --------- | -----:| --------- |
 | username     |   true | string |'0359426985'|username của khách hàng|
 | signature     |   true |string |xxxx|md5(username.'$'.secretKey)|
 
 
- Response:

		+ Success: {result: { CODE: 0, MESSAGE: "Thành công", DATA:  "Người dùng tồn tại" }}; 
		+ Error: other
		
**API nhận kết quả giao dịch:**
- API: https://api-lichcatnhat.gviet.vn/thudo/v1/log
- Method: get
- Request:	
		
| Params    | Required | DataType | Example|Note| 
| --------- | -----:| --------- | -----:|---|
| username     |   true | string |'0359426985'|
| channel     |   true | string |'bank'|
| amount     |   true | string |'0359426985'|
| method     |   true | string |'bank'|	`vnPay: method="bank"`, `thẻ cào: method ="card"`
| orderId     |   true | string |'S1HydEJ2W'|
| error     |   true | string | '0' | `Thành công: error="0"`, `Thất bại: error="1"`
| msg       |        | string | "thanhcong"|
| orderType     |   true | string |'chargeCard'|
| orderInfo     |   true | string |'OrderId:PAY_1562915108964263;Amount:66000'|
| signature     |   true |string |xxxx|md5(username.'$'.amount'$'.orderId.'$'.secretKey)|


- Response:

		+ Success: {result: { CODE: 0, MESSAGE: "Thành công", DATA: { username: "0359426985", info: {} } }}; 
      + Error: other

