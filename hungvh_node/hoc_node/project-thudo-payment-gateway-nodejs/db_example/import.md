# How to import DB for test?

### Import database example

Recomment [Mongo](https://www.mongodb.com/download-center/community) v4.0+ to run.

Import data:

```sh
$  mongorestore -d db_name /your_path/db_example/payment/
```

Check import success:

```sh
$ mongo
$ show dbs
```

> If have db_name => import successful!

To export new data:

```sh
$  mongodump -d db_name -o /your_path
```

License
----
- MIT


