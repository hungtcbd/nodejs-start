// During the test the env variable is set to test
process.env.NODE_ENV = "test";

const chai = require( "chai" );
const chaiHttp = require( "chai-http" );

const server = require( "../index" ),
  should = chai.should();

chai.use( chaiHttp );

const app = require( "./e2e/app" );
const payment = require( "./e2e/payment" );
const log = require( "./e2e/log" );

app( chai, server, should );
payment( chai, server, should );

log( chai, server, should );
