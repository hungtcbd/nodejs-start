// eslint-disable-next-line no-unused-vars
const Payment = require( "../../src/models/Payment.model.js" );

// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {
  describe( "Payments Controllers", () => {
    let id = null;

    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );
    /*
     * Test the /POST route VNPay
     */
    describe( " /GET VNPay", () => {
      it( "it should get a request payment using VNPay", ( done ) => {
        chai.request( server )
          .get( "/api/v1/pay?orderId=PAY_156291510189642353&orderInfo=OrderId:PAY_156291510823964263;Amount:660201&orderType=other&amount=500002&ip=127.0.0.1&bankCode=all&username=0345267633&appId=090e373e19cc40a1499ad1c7c3e91fe0" )
          .end( async ( err, res ) => {
            const findPayment = await Payment.findOne( { "orderId": "PAY_156291510189642353" } );

            id = findPayment._id;
            res.should.have.status( 200 );
            res.body.result.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "LINK" );
            done();
          } );
      } );
    } );

    describe( "/DELETE?_id payment", () => {
      it( "it should DELETE a payment given the id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .delete( `/api/v1/payment?_id=${ id }` )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.should.have.property( "DATA" ).eql( null );
            done();
          } );
      } );
    } );

    /*
     * Test the /POST route Card
     */
    describe( " /GET Card", () => {
      it( "it should get a request payment using Card", ( done ) => {
        chai.request( server )
          .get( "/api/v1/card?orderType=chargeCard&amount=50000&channel=1&telco=VINA&ip=27.0.0.1&username=0345267633&cardCode=59449443753241266213&serial=ID035173323214243&orderId=CARD_15629152109642634231&orderInfo=OrderId:CARD_1562915108196112368;Amount:66001&appId=090e373e19cc40a1499ad1c7c3e91fe0" )
          .end( async ( err, res ) => {
            const findPayment = await Payment.findOne( { "orderId": "CARD_15629152109642634231" } );

            id = findPayment._id;
            res.should.have.status( 200 );
            res.body.result.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.should.have.property( "AMOUNT" );
            done();
          } );
      } );
    } );

    /*
    * Test the /DELETE?_id= route
    */
    describe( "/DELETE?_id payment", () => {
      it( "it should DELETE a payment given the id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .delete( `/api/v1/payment?_id=${ id }` )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.should.have.property( "DATA" ).eql( null );
            done();
          } );
      } );
    } );

    describe( " /GET Check User", () => {
      it( "it should get method to check user", ( done ) => {
        chai.request( server )
          .get( "/api/v1/check-user?username=0398662219&signature=728fbb18abbb9232299d27c6832d21e3" )
          .end( async ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.should.have.property( "DATA" ).eql( "Người dùng tồn tại" );
            done();
          } );
      } );
    } );

  } );
};
