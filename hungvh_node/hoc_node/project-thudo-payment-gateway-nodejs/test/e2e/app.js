// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {
  let id = null,
    appProductId = null;

  // App block test
  describe( "App Controller", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );
    /*
         * Test the /GET route
         */
    describe( "/GET apps", () => {
      it( "it should GET all the app", ( done ) => {
        chai.request( server )
          .get( "/api/v1/app" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
     * Test the /POST route
     */
    describe( "/POST app", () => {
      it( "it should POST a app", ( done ) => {
        let app = {
          "name": "test",
          "secretKey": "abcd",
          "appId": "4234239095253"
        };

        chai.request( server )
          .post( "/api/v1/app" )
          .send( app )
          .end( ( err, res ) => {
            id = res.body.result.DATA._id;
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.DATA.should.have.property( "_id" );
            res.body.result.DATA.should.have.property( "name" ).eql( app.name );
            res.body.result.DATA.should.have.property( "appId" ).eql( app.appId );
            done();
          } );
      } );
      it( "it should not POST a book without status field", ( done ) => {
        let app = {
          "name": "A2"
        };

        chai.request( server )
          .post( "/api/v1/app" )
          .send( app )
          .end( ( err, res ) => {
            res.should.have.status( 403 );
            res.body.should.be.a( "object" );
            res.body.error.should.have.property( "CODE" ).eql( 2 );
            res.body.error.should.have.property( "MESSAGE" ).eql( "Thiếu tham số yêu cầu!" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET?_id= route
    */
    describe( "/GET?_id= app", () => {
      it( "it should GET a app by the given id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( `/api/v1/app?_id=${ id }` )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "object" );
            res.body.result.DATA.should.have.property( "appId" );
            res.body.result.DATA.should.have.property( "_id" ).eql( id );
            res.body.result.DATA.should.have.property( "name" );
            done();
          } );
      } );
    } );

    /*
     * Test the /PATCH?id= route
     */
    describe( "/PATCH?_id= app", () => {
      it( "it should UPDATE a app given the id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        const app = {
          "name": "test_update"
        };

        chai.request( server )
          .patch( `/api/v1/app?_id=${ id }` )
          .send( app )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.DATA.should.have.property( "name" ).eql( app.name );
            done();
          } );
      } );
    } );

    describe( "/GET apps product", () => {
      it( "it should GET all the app product", ( done ) => {
        chai.request( server )
          .get( "/api/v1/app-product" )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "array" );
            done();
          } );
      } );
    } );

    /*
     * Test the /POST route
     */
    describe( "/POST app product", () => {
      it( "it should POST a app product", ( done ) => {
        let app = {
          "name": "test",
          "secretKey": "abcd",
          "app": id
        };

        chai.request( server )
          .post( "/api/v1/app-product" )
          .send( app )
          .end( ( err, res ) => {
            appProductId = res.body.result.DATA._id;
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.DATA.should.have.property( "_id" );
            res.body.result.DATA.should.have.property( "name" ).eql( app.name );
            res.body.result.DATA.should.have.property( "secretKey" ).eql( app.secretKey );
            done();
          } );
      } );
      it( "it should not POST a book without status field", ( done ) => {
        let app = {
          "name": "A2"
        };

        chai.request( server )
          .post( "/api/v1/app-product" )
          .send( app )
          .end( ( err, res ) => {
            res.should.have.status( 403 );
            res.body.should.be.a( "object" );
            res.body.error.should.have.property( "CODE" ).eql( 2 );
            res.body.error.should.have.property( "MESSAGE" ).eql( "Thiếu tham số yêu cầu!" );
            done();
          } );
      } );
    } );

    /*
    * Test the /GET?_id= route
    */
    describe( "/GET?_id= app product", () => {
      it( "it should GET a app product by the given id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .get( `/api/v1/app-product?_id=${ appProductId }` )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "object" );
            res.body.result.DATA.should.have.property( "appId" );
            res.body.result.DATA.should.have.property( "_id" ).eql( appProductId );
            res.body.result.DATA.should.have.property( "name" );
            done();
          } );
      } );
    } );

    /*
     * Test the /PATCH?id= route
     */
    describe( "/PATCH?_id= app product", () => {
      it( "it should UPDATE a app product given the id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        const app = {
          "name": "test_update"
        };

        chai.request( server )
          .patch( `/api/v1/app-product?_id=${ appProductId }` )
          .send( app )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.result.DATA.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.DATA.should.have.property( "name" ).eql( app.name );
            done();
          } );
      } );
    } );

    /*
     * Test the /DELETE?_id= route
     */
    describe( "/DELETE?_id app product", () => {
      it( "it should DELETE a app product given the id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .delete( `/api/v1/app-product?_id=${ appProductId}` )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.should.have.property( "DATA" ).eql( null );
            done();
          } );
      } );
    } );

    /*
     * Test the /DELETE?_id= route
     */
    describe( "/DELETE?_id app", () => {
      it( "it should DELETE a app given the id", ( done ) => {
        // TODO add a model to db then get that id to take this test
        chai.request( server )
          .delete( `/api/v1/app?_id=${ id}` )
          .end( ( err, res ) => {
            res.should.have.status( 200 );
            res.body.should.be.a( "object" );
            res.body.result.should.have.property( "CODE" ).eql( 0 );
            res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
            res.body.result.should.have.property( "DATA" ).eql( null );
            done();
          } );
      } );
    } );
  } );
};
