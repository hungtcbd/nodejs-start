// eslint-disable-next-line no-unused-vars
module.exports = ( chai, server, should ) => {
  describe( "Log Controller", () => {
    beforeEach( ( done ) => {
      // Before each test we empty the database in your case
      done();
    } );
  } );
  /*
   * Test the /GET index route
   */
  describe( "/POST Logs", () => {
    it( "it should GET all the log", ( done ) => {
      chai.request( server )
        .post( "/api/v1/log" )
        .end( ( err, res ) => {
          res.should.have.status( 200 );
          res.body.result.DATA.should.be.a( "array" );
          done();
        } );
    } );
  } );

  /*
   * Test the /GET analysis route
   */
  describe( "/GET analysis", () => {
    it( "it should GET analysis", ( done ) => {
      chai.request( server )
        .get( "/api/v1/log/analysis?date=1" )
        .end( ( err, res ) => {
          res.should.have.status( 200 );
          res.body.result.should.have.property( "CODE" ).eql( 0 );
          res.body.result.should.have.property( "MESSAGE" ).eql( "Thành công" );
          res.body.result.DATA.should.be.a( "object" );
          res.body.result.DATA.should.have.property( "vnPaySuccess" );
          res.body.result.DATA.should.have.property( "vnPayFault" );
          res.body.result.DATA.should.have.property( "cardSuccess" );
          res.body.result.DATA.should.have.property( "cardFault" );
          res.body.result.DATA.should.have.property( "totalSuccess" );
          res.body.result.DATA.should.have.property( "totalFault" );
          done();
        } );
    } );
  } );
};
