const router = require( "express" ).Router();

// General Route
router.use( "/", require( "./modules/payment.route" ) );
router.use( "/app", require( "./modules/app.route" ) );
router.use( "/app-product", require( "./modules/appProduct.route" ) );
router.use( "/log", require( "./modules/log.route" ) );
router.use( "/srv", require( "./modules/srvThudo.route" ) );


module.exports = router;
