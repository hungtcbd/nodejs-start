const router = require( "express-promise-router" )();
const AppProductController = require( "../../controllers/appProduct.controller" );

router.route( "/" )
  .get( AppProductController.index )
  .post( AppProductController.create )
  .patch( AppProductController.update )
  .delete( AppProductController.delete );

module.exports = router;
