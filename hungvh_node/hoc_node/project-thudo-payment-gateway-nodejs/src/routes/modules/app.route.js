const router = require( "express-promise-router" )();
const AppController = require( "../../controllers/app.controller" );

router.route( "/" )
  .get( AppController.index )
  .post( AppController.create )
  .patch( AppController.update )
  .delete( AppController.delete );

module.exports = router;
