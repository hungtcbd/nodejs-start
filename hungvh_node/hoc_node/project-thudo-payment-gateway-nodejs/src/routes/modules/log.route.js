const router = require( "express-promise-router" )();
const LogController = require( "../../controllers/log.controller" );

router.route( "/" )
  .post( LogController.index )
  .get( LogController.create )
  .delete( LogController.delete );

router.route( "/analysis" ).get( LogController.analysis );


module.exports = router;
