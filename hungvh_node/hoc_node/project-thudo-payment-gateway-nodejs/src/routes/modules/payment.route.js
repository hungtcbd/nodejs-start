const router = require( "express-promise-router" )();
const PaymentController = require( "../../controllers/payment.controller" );

router.route( "/pay" ).post( PaymentController.vnPay );
router.route( "/card" ).post( PaymentController.card );
router.route( "/pay" ).get( PaymentController.vnPay );
router.route( "/card" ).get( PaymentController.card );
router.route( "/payment" ).delete( PaymentController.delete );
router.route( "/check-user" ).get( PaymentController.checkUser );


module.exports = router;
