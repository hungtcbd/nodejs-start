const router = require( "express-promise-router" )();
const srvThudoController = require( "../../controllers/srvThudo.controller" );

router.route( "/pay" ).get( srvThudoController.vnPay );
router.route( "/charge" ).get( srvThudoController.card );
router.route( "/payment" ).get( srvThudoController.log );


module.exports = router;
