/**
 * Create service sync info from server product, server thudo
 * author: hocpv
 * create_at: 07/08/2019
 * editor:
 * update_at:
 */
const request = require( "axios" );
const graylog2 = require( "graylog2" ),
  logger = new graylog2.graylog( {
    "servers": [
      { "host": process.env.HOST_GRAY_LOG, "port": process.env.PORT_GRAY_LOG }
    ],
    // the name of this host
    // (optional, default: os.hostname())
    // "hostname": "server.name",

    // the facility for these log messages
    // (optional, default: "Node.js")
    "facility": "Node.js",

    // max UDP packet size, should never exceed the
    // MTU of your system (optional, default: 1400)
    "bufferSize": 1350
  } );

logger.on( "error", function ( error ) {
  console.error( "Error while trying to write to graylog2:", error );
} );

module.exports = {
  "paymentSync": ( url, data ) => {
    return request( {
      "method": "get",
      "url": `${process.env.APP_URL_THUDO}/${url}`,
      "data": data
    } ).catch( ( error ) => {
      console.log( error );
      logger.log( "Máy chủ thu do đang hoạt động có vấn đề!", { "CODE": 500, "INFO": `${process.env.APP_URL_THUDO}/${url}` } );
    } );
  },
  "notificationSync": ( url, data ) => {
    return request( {
      "method": "get",
      "url": `${process.env.APP_URL_PRODUCT}/payment/receivedNotifier?${url}`,
      "data": data
    } ).catch( ( error ) => {
      console.log( error );
      logger.log( "Máy chủ thu do đang hoạt động có vấn đề!", { "CODE": 500, "INFO": `${process.env.APP_URL_THUDO}/recievedNotifier?${url}` } );
    } );
  },
  "checkUserSync": ( url, data ) => {
    return request( {
      "method": "get",
      "url": `${process.env.APP_URL_PRODUCT}/user/check?${url}`,
      "data": data
    } ).catch( ( error ) => {
      console.log( error );
      logger.log( "Máy chủ thu do đang hoạt động có vấn đề!", { "CODE": 500, "INFO": `${process.env.APP_URL_THUDO}/user?${url}` } );
    } );
  }
};
