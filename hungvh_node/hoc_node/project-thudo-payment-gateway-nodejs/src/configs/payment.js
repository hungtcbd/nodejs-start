module.exports = {
  "APP_NAME_1": "app1",
  "USER_REQUEST": "dangian1",
  "PAYMENT_VNPAY_REQUEST": ( param0, param1, param2, param3, param4, param5 ) => {
    return `pay?cmdType=1&appId=${param0}&provider=vnpay&orderInfo=${param1}&orderType=other&amount=${param2}&ip=${param3}&bankCode=all&username=${param4}&orderId=${param5}`;
  },
  "PAYMENT_CARD_REQUEST": ( param0, param1, pram2, param3, param4, param5, param6, param7, param8 ) => {
    return `charge?cmdType=3&appId=${param0}&orderInfo=${param1}&orderType=chargeCard&amount=${pram2}&serial=${param3}&cardCode=${param4}&channel=${param5}&telco=${param6}&ip=${param7}&username=${param8}`;
  },
  "PAYMENT_NOTIFICATION": ( param0, param1, param2, param3, param4, param5, param6, param7, param8 ) => {
    return `username=${param0}&requestId=${param1}&sourceId=${param2}&amount=${param3}&orderId=${param4}&orderType=${param5}&orderInfo=${param6}&data={}&userRequest=${param7}&signature=${param8}&distId=1`;
  },
  "CHECK_USER": ( param0, param1, param2 ) => {
    return `username=${param0}&requestId=${param1}&userRequest=hocpv&signature=${param2}`;
  },
  "URL_PAYMENT_BANK": "https://shop.vgame.us/App/5db175fc6378ecbf275c8159/Payment/Bank/Form",
  "URL_PAYMENT_CARD": ( telco ) => {
    return `https://shop.vgame.us/App/5db175fc6378ecbf275c8159/Payment/Card/${ telco }/Form`;
  }
};
