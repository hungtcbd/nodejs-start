module.exports = {
  "CODE": {
    "SUCCESS": 0,
    "FAIL": 1,
    "SERVER_TD_ERROR": 101,
    "SERVER_PRODUCT_ERROR": 102,
    "INVALID_EXIST": 103,
    "NOTFOUND": 104,
    "INVALID_SIGNATURE": 105,
    "INVALID_PARAMS": 2
  },
  "MESSAGE": {
    "SUCCESS": "Thành công",
    "FAIL": "Thất bại",
    "SERVER_TD_ERROR": "Máy chủ thủ đô đang hoạt động có vấn đề!",
    "SERVER_PRODUCT_ERROR": "Máy chủ product đang hoạt động có vấn đề!",
    "INVALID_EXIST": "Yêu cầu đã tồn tại!",
    "NOTFOUND": "Yêu cầu không được tìm thấy!",
    "INVALID_SIGNATURE": "Sai chữ ký xác thực!",
    "INVALID_PARAMS": "Thiếu tham số yêu cầu!"
  }
};
