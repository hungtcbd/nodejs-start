/**
 * Create controller app for api payment ( save apps from product provider ) => scale
 * author: hocpv
 * create_at: 07/08/2019
 * editor:
 * update_at:
 */
const App = require( "../models/App.model" );
const response = require( "../configs/response" );

module.exports = {
  /**
   * Module index app ( get by id, get all )
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "index": async ( req, res ) => {
    let dataResponse = null;
    // Get app by _id

    if ( req.query._id ) {
      dataResponse = await App.findOne( { "_id": req.query._id }, "-__v" ).lean();
    } else if ( Object.entries( req.query ).length === 0 && req.query.constructor === Object ) {
      dataResponse = await App.find( {}, "-__v" ).lean();
    }
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": dataResponse } } );
  },
  /**
   * Module create app follow app product
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "create": async ( req, res ) => {
    const newApp = await new App( req.body ),
      arrDefault = [ "appId", "name", "secretKey" ];

    if ( arrDefault.every( ( element ) => Object.keys( req.body ).includes( element ) ) === false ) {
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_PARAMS, "MESSAGE": response.MESSAGE.INVALID_PARAMS } } );
    }

    await newApp.save( ( err ) => {
      if ( err ) {
        return res.status( 405 ).json( { "error": { "CODE": response.CODE.FAIL, "MESSAGE": response.MESSAGE.FAIL, "ERROR": err } } );
      }
      return res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": newApp } } );
    } );
  },
  /**
   * Module update info app
   * @param req
   * @param res
   * @returns {Promise<*|Promise<any>>}
   */
  "update": async ( req, res ) => {
    const findApp = await App.findOne( { "_id": req.query._id }, "-__v" );

    if ( !findApp ) {
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }
    // eslint-disable-next-line one-var
    const appUpdate = await App.findByIdAndUpdate( req.query._id, { "$set": req.body }, { "new": true } );

    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": appUpdate } } );
  },
  /**
   * Module delete app
   * @param req
   * @param res
   * @returns {Promise<*|Promise<any>>}
   */
  "delete": async ( req, res ) => {
    const findApp = await App.findOne( { "_id": req.query._id }, "-__v" );

    if ( !findApp ) {
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }

    await findApp.remove();
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": null } } );
  }
};
