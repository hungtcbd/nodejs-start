/**
 * Create controller app product for api payment ( save apps from product provider ) => scale
 * author: hocpv
 * create_at: 25/10/2019
 * editor:
 * update_at:
 */
const AppProduct = require( "../models/AppProduct.model" );
const AppThudo = require( "../models/App.model" );
const response = require( "../configs/response" );
const md5 = require( "md5" );

module.exports = {
  /**
   * Module index app ( get by id, get all )
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "index": async ( req, res ) => {
    let dataResponse = null;
    // Get app by _id

    if ( req.query._id ) {
      dataResponse = await AppProduct.findOne( { "_id": req.query._id }, "-__v" ).lean();
    } else if ( Object.entries( req.query ).length === 0 && req.query.constructor === Object ) {
      dataResponse = await AppProduct.find( {}, "-__v" ).lean();
    }
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": dataResponse } } );
  },
  /**
   * Module create app follow app product
   * @param req
   * @param res
   * @returns {Promise<void>}
   */
  "create": async ( req, res ) => {
    const newApp = await new AppProduct( req.body ),
      arrDefault = [ "app", "name", "secretKey" ];

    if ( arrDefault.every( ( element ) => Object.keys( req.body ).includes( element ) ) === false ) {
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_PARAMS, "MESSAGE": response.MESSAGE.INVALID_PARAMS } } );
    }

    let findAppThudo = await AppThudo.findOne( { "_id": req.body.app } );

    if ( !findAppThudo ) {
      return res.status( 404 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }

    newApp.appId = md5( process.env.API_GATEWAY_PREFIX + req.body.name + process.env.API_GATEWAY_PREFIX );
    newApp._app = req.body.app;
    await newApp.save( ( err ) => {
      if ( err ) {
        return res.status( 405 ).json( { "error": { "CODE": response.CODE.FAIL, "MESSAGE": response.MESSAGE.FAIL, "ERROR": err } } );
      }
      return res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": newApp } } );
    } );
  },
  /**
   * Module update info app
   * @param req
   * @param res
   * @returns {Promise<*|Promise<any>>}
   */
  "update": async ( req, res ) => {
    const findAppProduct = await AppProduct.findOne( { "_id": req.query._id }, "-__v" );

    if ( !findAppProduct ) {
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }
    // eslint-disable-next-line one-var
    const appUpdate = await AppProduct.findByIdAndUpdate( req.query._id, { "$set": req.body }, { "new": true } );

    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": appUpdate } } );
  },
  /**
   * Module delete app
   * @param req
   * @param res
   * @returns {Promise<*|Promise<any>>}
   */
  "delete": async ( req, res ) => {
    const findApp = await AppProduct.findOne( { "_id": req.query._id }, "-__v" );

    if ( !findApp ) {
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }

    await findApp.remove();
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": null } } );
  }
};
