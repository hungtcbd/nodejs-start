const Log = require( "../models/Log.model" );
const Payment = require( "../models/Payment.model" );
const { notificationSync } = require( "../microservices/synchronize/payment.service" );
const config = require( "../configs/payment" );
const logger = require( "../helpers/services/log.service" );
const response = require( "../configs/response" );
const md5 = require( "md5" );

module.exports = {
  /**
     *  Function index log ( get by id, get all )
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
  "index": async ( req, res ) => {
    let dataResponse = null;
    // Todo: Get log by _id

    if ( req.query._id ) {
      dataResponse = await Log.findOne( { "_id": req.query._id }, "-__v" ).lean();
    } else if ( Object.entries( req.query ).length === 0 && req.query.constructor === Object ) {
      dataResponse = await Log.find( {}, "-__v" ).lean();
    }
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": dataResponse } } );
  },

  /**
     * Function create & update log for user
     * @param req
     * @param res
     * @returns {Promise<*|void|Chai.Assertion|Promise<any>>}
     */
  "create": async ( req, res ) => {
    const arrDefault = [ "amount", "channel", "error", "method", "orderId", "orderInfo", "orderType", "username", "signature" ];

    // check params transfer
    if ( arrDefault.every( ( element ) => Object.keys( req.query ).includes( element ) ) === false ) {
      logger.log( response.MESSAGE.INVALID_PARAMS, { "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_PARAMS, "MESSAGE": response.MESSAGE.INVALID_PARAMS } } );
    }
    let signature = md5( req.query.username + process.env.API_GATEWAY_PREFIX + req.query.amount + process.env.API_GATEWAY_PREFIX + req.query.orderId + process.env.API_GATEWAY_PREFIX + process.env.API_GATEWAY_SECRET_KEY );

    // check signature
    if ( signature.toString() !== req.query.signature ) {
      logger.log( response.MESSAGE.INVALID_SIGNATURE, { "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_SIGNATURE, "MESSAGE": response.MESSAGE.INVALID_SIGNATURE } } );
    }
    // eslint-disable-next-line one-var
    const findLogOfUser = await Log.findOne( { "username": req.query.username } ),
      findPayment = await Payment.findOne( { "orderInfo": req.query.orderInfo, "username": req.query.username } );

    // Todo: Check info payment
    if ( !findPayment ) {
      logger.log( "Có vấn đề khi xác nhận giao dịch!", { "INFO": req.query } );
      return res.status( 405 ).json( { "error": { "CODE": response.CODE.FAIL, "MESSAGE": response.MESSAGE.FAIL, "ERROR": "Không có bất kỳ giao dịch nào có thông tin thanh toán trên!" } } );
    }
    if ( findPayment.status === true ) {
      logger.log( "Có vấn đề khi xác nhận giao dịch!", { "INFO": req.query } );
      return res.status( 405 ).json( { "error": { "CODE": response.CODE.INVALID_EXIST, "MESSAGE": response.MESSAGE.INVALID_EXIST, "ERROR": "Bạn đã thực hiện giao dịch với thông tin này!" } } );
    }

    // eslint-disable-next-line one-var
    let objectSave = {
      "requestId": findPayment.requestId,
      "status": parseFloat( req.query.error ) === 1 ? "Thất bại" : "Thành công",
      "msg": req.query.msg ? req.query.msg : "",
      "method": req.query.method,
      "date": new Date()
    };

    // Todo: Check log of user
    if ( !findLogOfUser ) {
      const newLog = await new Log( { "username": req.query.username } );

      // Todo: Check status of payment from srv thudo
      if ( parseFloat( req.query.error ) === 0 ) {
        findPayment.status = true;
        await findPayment.save();
      }

      newLog.logs.push( objectSave );
      await newLog.save();
      // eslint-disable-next-line one-var
      const resResult = await notificationSync( config.PAYMENT_NOTIFICATION( req.query.username, findPayment.requestId, req.query.channel, req.query.amount, req.query.orderId, req.query.orderType, req.query.orderInfo, config.USER_REQUEST, findPayment.signature ) );

      if ( resResult === undefined || resResult.data.error_code !== 0 ) {
        logger.log( response.MESSAGE.SERVER_PRODUCT_ERROR, { "CODE": 405, "INFO": objectSave } );
        return res.status( 405 ).json( { "error": { "CODE": response.CODE.SERVER_PRODUCT_ERROR, "MESSAGE": response.MESSAGE.SERVER_PRODUCT_ERROR, "DATA": resResult.data } } );
      }
      return res.status( 200 ).json( { "ec": 0, "msg": "thực hiện thành công" } );
    }

    // Todo: if exist log, check status of payment from srv thudo
    if ( parseFloat( req.query.error ) === 0 ) {
      findPayment.status = true;
      await findPayment.save();
    }

    findLogOfUser.logs.push( objectSave );
    // eslint-disable-next-line one-var
    const resResult = await notificationSync( config.PAYMENT_NOTIFICATION( req.query.username, findPayment.requestId, req.query.channel, req.query.amount, req.query.orderId, req.query.orderType, req.query.orderInfo, config.USER_REQUEST, findPayment.signature ) );

    if ( resResult === undefined || resResult.data.error_code !== 0 ) {
      logger.log( response.MESSAGE.SERVER_PRODUCT_ERROR, { "CODE": 405, "INFO": objectSave } );
      return res.status( 405 ).json( { "error": { "CODE": response.CODE.SERVER_PRODUCT_ERROR, "MESSAGE": response.MESSAGE.SERVER_PRODUCT_ERROR, "DATA": resResult.data } } );
    }

    await findLogOfUser.save();

    return res.status( 200 ).json( { "ec": 0, "msg": "thực hiện thành công" } );
  },

  /**
     *  Function delete log of user
     * @param req
     * @param res
     * @returns {Promise<*|void|Chai.Assertion|Promise<any>>}
     */
  "delete": async ( req, res ) => {
    const findLog = await Log.findOne( { "_id": req.query._id }, "-__v" );

    if ( !findLog ) {
      return res.status( 403 ).json( { "error": { "CODE": 1, "MESSAGE": "Khong tim thay log cua tai khoan nay!" } } );
    }

    await findLog.remove();
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": null } } );
  },

  /**
     * Function analysis log of user
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
  "analysis": async ( req, res ) => {
    let date = new Date();

    date = date.setDate( date.getDate() - parseFloat( req.query.date ) );
    const findLog = await Log.find( { "updatedAt": { "$gte": date, "$lte": Date.now() }, "logs.date": { "$gte": date, "$lte": Date.now() } } ).lean(),
      dataResPonse = {
        "vnPaySuccess": ( await Promise.all( findLog.map( async ( user ) => {
          return user.logs.filter( ( item ) => {
            return item.status === "Thành công" && item.method === "bank" && item.code === 200 && new Date( item.date ).getTime() >= date && new Date( item.date ).getTime() <= Date.now();
          } ).length;
        } ) ) ).reduce( ( a, b ) => a + b, 0 ),
        "vnPayFault": ( await Promise.all( findLog.map( async ( user ) => {
          return user.logs.filter( ( item ) => {
            return item.status === "Thất bại" && item.method === "bank" && item.code !== 200 && new Date( item.date ).getTime() >= date && new Date( item.date ).getTime() <= Date.now();
          } ).length;
        } ) ) ).reduce( ( a, b ) => a + b, 0 ),
        "cardSuccess": ( await Promise.all( findLog.map( async ( user ) => {
          return user.logs.filter( ( item ) => {
            return item.status === "Thành công" && item.method === "card" && item.code === 200 && new Date( item.date ).getTime() >= date && new Date( item.date ).getTime() <= Date.now();
          } ).length;
        } ) ) ).reduce( ( a, b ) => a + b, 0 ),
        "cardFault": ( await Promise.all( findLog.map( async ( user ) => {
          return user.logs.filter( ( item ) => {
            return item.status === "Thất bại" && item.method === "card" && item.code !== 200 && new Date( item.date ).getTime() >= date && new Date( item.date ).getTime() <= Date.now();
          } ).length;
        } ) ) ).reduce( ( a, b ) => a + b, 0 )
      };

    dataResPonse.totalSuccess = dataResPonse.vnPaySuccess + dataResPonse.cardSuccess;
    dataResPonse.totalFault = dataResPonse.vnPayFault + dataResPonse.cardFault;
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": dataResPonse } } );
  }
};
