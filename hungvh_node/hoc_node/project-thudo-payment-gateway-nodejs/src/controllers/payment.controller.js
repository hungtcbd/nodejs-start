/* eslint-disable one-var */
/**
 * Create controller payment for api payment
 * author: hocpv
 * create_at: 06/08/2019
 * editor:
 * update_at:
 */
const Payment = require( "../models/Payment.model" );
// const Log = require( "../models/Log.model" );
const AppProduct = require( "../models/AppProduct.model" );
const AppThudo = require( "../models/App.model" );
const md5 = require( "md5" );
const { randomNumberString } = require( "../helpers/utils/functions/string" );
// eslint-disable-next-line no-unused-vars
const { paymentSync, checkUserSync } = require( "../microservices/synchronize/payment.service" );
const unicode = require( "../helpers/utils/functions/unicode.js" );
const config = require( "../configs/payment" );
const logger = require( "../helpers/services/log.service" );
const response = require( "../configs/response" );

module.exports = {
  /**
     * Module payment using VNPay
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
  "vnPay": async ( req, res ) => {
    const arrDefault = [ "amount", "ip", "orderId", "orderInfo", "orderType", "username", "appId" ];

    if ( arrDefault.every( ( element ) => Object.keys( req.query ).includes( element ) ) === false ) {
      logger.log( response.MESSAGE.INVALID_PARAMS, { "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_PARAMS, "MESSAGE": response.MESSAGE.INVALID_PARAMS } } );
    }

    const date = new Date(),
      formatId = date.getFullYear().toString() + ( date.getMonth() + 1 ).toString() + date.getDate().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString(),
      objectSave = {
        "requestId": formatId + randomNumberString( 6 ),
        "orderId": req.query.orderId,
        "orderInfo": unicode( req.query.orderInfo ),
        "orderType": req.query.orderType === "vnpay" ? "other" : req.query.orderType,
        "amount": parseFloat( req.query.amount ),
        "ip": req.query.ip,
        "bankCode": "all",
        "username": req.query.username,
        "signature": md5( req.query.requestId + process.env.API_GATEWAY_PREFIX + "bank" + process.env.API_GATEWAY_PREFIX + req.query.username + process.env.API_GATEWAY_PREFIX + req.query.amount + process.env.API_GATEWAY_PREFIX + req.query.orderId + process.env.API_GATEWAY_PREFIX + req.query.orderType + process.env.API_GATEWAY_PREFIX + process.env.API_GATEWAY_SECRET_KEY )
      },
      findApp = await AppProduct.findOne( { "appId": req.query.appId } ).lean(),
      findPayment = await Payment.findOne( { "orderId": req.query.orderId } ).lean();

    // check app use to request
    if ( !findApp ) {
      logger.log( `${response.MESSAGE.NOTFOUND} ứng dụng`, { "INFO": objectSave } );
      return res.status( 404 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }

    if ( findPayment ) {
      logger.log( response.MESSAGE.INVALID_EXIST, { "INFO": objectSave } );
      return res.status( 405 ).json( { "error": { "CODE": response.CODE.INVALID_EXIST, "MESSAGE": response.MESSAGE.INVALID_EXIST } } );
    }
    let findAppThudo = await AppThudo.findOne( { "_id": findApp._app } ).lean();

    // check app thudo use to request
    if ( !findAppThudo ) {
      logger.log( `${response.MESSAGE.NOTFOUND} ứng dụng`, { "INFO": objectSave } );
      return res.status( 404 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }
    objectSave.appId = req.query.appId;
    const newPayment = await new Payment( objectSave );

    // Send request to srv Thudo to receive response
    //   resResult = await paymentSync( config.PAYMENT_VNPAY_REQUEST( findAppThudo.appId, req.query.orderInfo, req.query.amount, req.query.ip, req.query.username, req.query.orderId ) );
    //
    // if ( resResult === undefined || resResult.data.result.CODE !== 0 ) {
    //   logger.log( response.MESSAGE.SERVER_TD_ERROR, { "CODE": 405, "INFO": objectSave } );
    //   return res.status( 405 ).json( { "error": { "CODE": response.CODE.SERVER_TD_ERROR, "MESSAGE": response.MESSAGE.SERVER_TD_ERROR, "DATA": resResult.data } } );
    // }
    await newPayment.save( ( err ) => {
      if ( err ) {
        logger.log( "issue to database!", { "ERROR": err, "INFO": objectSave } );
        return res.status( 405 ).json( { "error": { "CODE": response.CODE.FAIL, "MESSAGE": err } } );
      }
      return res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "LINK": config.URL_PAYMENT_BANK } } );
    } );
  },
  /**
     * Module payment using card
     * @param req
     * @param res
     * @returns {Promise<void>}
     */
  "card": async ( req, res ) => {
    const arrDefault = [ "amount", "cardCode", "channel", "ip", "orderId", "orderInfo", "orderType", "serial", "telco", "username", "appId" ];

    if ( arrDefault.every( ( element ) => Object.keys( req.query ).includes( element ) ) === false ) {
      logger.log( response.MESSAGE.INVALID_PARAMS, { "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_PARAMS, "MESSAGE": response.MESSAGE.INVALID_PARAMS } } );
    }

    const date = new Date(),
      formatId = date.getFullYear().toString() + ( date.getMonth() + 1 ).toString() + date.getDate().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString(),
      objectSave = {
        "requestId": formatId + randomNumberString( 6 ),
        "orderId": req.query.orderId,
        "orderInfo": unicode( req.query.orderInfo ),
        "orderType": "other",
        "amount": parseFloat( req.query.amount ),
        "ip": req.query.ip,
        "serial": req.query.serial,
        "cardCode": req.query.cardCode,
        "channel": parseFloat( req.query.channel ),
        "telco": req.query.telco,
        "username": req.query.username,
        "signature": md5( req.query.requestId + process.env.API_GATEWAY_PREFIX + "card" + process.env.API_GATEWAY_PREFIX + req.query.username + process.env.API_GATEWAY_PREFIX + req.query.amount + process.env.API_GATEWAY_PREFIX + req.query.orderId + process.env.API_GATEWAY_PREFIX + req.query.orderType + process.env.API_GATEWAY_PREFIX + process.env.API_GATEWAY_SECRET_KEY )
      },
      findApp = await AppProduct.findOne( { "appId": req.query.appId } ).lean(),
      findPayment = await Payment.findOne( { "orderId": req.query.orderId } ).lean();

    // check app use to request
    if ( !findApp ) {
      logger.log( `${response.MESSAGE.NOTFOUND} ứng dụng`, { "INFO": objectSave } );
      return res.status( 404 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }

    if ( findPayment ) {
      logger.log( response.MESSAGE.INVALID_EXIST, { "INFO": objectSave } );
      return res.status( 405 ).json( { "error": { "CODE": response.CODE.INVALID_EXIST, "MESSAGE": response.MESSAGE.INVALID_EXIST } } );
    }
    let findAppThudo = await AppThudo.findOne( { "_id": findApp._app } ).lean();

    // check app thudo use to request
    if ( !findAppThudo ) {
      logger.log( `${response.MESSAGE.NOTFOUND} ứng dụng`, { "INFO": objectSave } );
      return res.status( 404 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }
    objectSave.appId = req.query.appId;
    const newPayment = await new Payment( objectSave );

    // Send request to srv Thudo to receive response
    //   resResult = await paymentSync( config.PAYMENT_CARD_REQUEST( findAppThudo.appId, req.query.orderInfo, req.query.amount, req.query.serial, req.query.cardCode, req.query.channel, req.query.cardCode, req.query.ip, req.query.username ) );
    //
    // if ( resResult === undefined || resResult.data.result.CODE !== 0 ) {
    //   logger.log( response.MESSAGE.SERVER_TD_ERROR, { "CODE": 405, "INFO": objectSave } );
    //   return res.status( 405 ).json( { "error": { "CODE": response.CODE.SERVER_TD_ERROR, "MESSAGE": response.MESSAGE.SERVER_TD_ERROR, "DATA": resResult.data } } );
    // }
    await newPayment.save( ( err ) => {
      if ( err ) {
        logger.log( "issue to database!", { "ERROR": err, "INFO": objectSave } );
        return res.status( 405 ).json( { "error": { "CODE": response.CODE.FAIL, "MESSAGE": err } } );
      }
      return res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "LINK": config.URL_PAYMENT_CARD( req.query.telco ) } } );
    } );
  },

  /**
   * API Check user
   * @param req
   * @param res
   * @returns {Promise<*|void|Chai.Assertion|Promise<any>>}
   */
  "checkUser": async ( req, res ) => {
    const arrDefault = [ "username", "signature" ];

    if ( arrDefault.every( ( element ) => Object.keys( req.query ).includes( element ) ) === false ) {
      logger.log( response.MESSAGE.INVALID_PARAMS, { "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 403 ).json( { "error": { "CODE": response.CODE.INVALID_PARAMS, "MESSAGE": response.MESSAGE.INVALID_PARAMS } } );
    }
    const date = new Date(),
      formatId = date.getFullYear().toString() + ( date.getMonth() + 1 ).toString() + date.getDate().toString() + date.getHours().toString() + date.getMinutes().toString() + date.getSeconds().toString(),
      requestId = formatId + randomNumberString( 6 ),
      signature = md5( req.query.username + process.env.API_GATEWAY_PREFIX + process.env.API_GATEWAY_SECRET_KEY ).toString() ;

    if ( signature !== req.query.signature ) {
      logger.log( response.MESSAGE.INVALID_SIGNATURE, { "CODE": 405, "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 405 ).json( { "error": { "CODE": response.CODE.INVALID_SIGNATURE, "MESSAGE": response.MESSAGE.INVALID_SIGNATURE } } );
    }

    // Send request to srv product to receive response
    const resResult = await checkUserSync( config.CHECK_USER( req.query.username, requestId, md5( req.query.username + process.env.API_GATEWAY_PREFIX + process.env.API_GATEWAY_SECRET_KEY_PRODUCT ).toString() ) );

    if ( resResult === undefined || resResult.data.status !== 0 ) {
      logger.log( response.MESSAGE.SERVER_PRODUCT_ERROR, { "CODE": 405, "INFO": { "query": req.query, "body": req.body } } );
      return res.status( 200 ).json( { "error": { "CODE": response.CODE.SERVER_PRODUCT_ERROR, "MESSAGE": response.MESSAGE.SERVER_PRODUCT_ERROR, "DATA": resResult.data } } );
    }

    res.status( 200 ).json( { "exists": 1 } );
  },
  "delete": async ( req, res ) => {
    const findPayment = await Payment.findOne( { "_id": req.query._id }, "-__v" );

    if ( !findPayment ) {
      return res.status( 404 ).json( { "error": { "CODE": response.CODE.NOTFOUND, "MESSAGE": response.MESSAGE.NOTFOUND } } );
    }

    await findPayment.remove();
    res.status( 200 ).json( { "result": { "CODE": response.CODE.SUCCESS, "MESSAGE": response.MESSAGE.SUCCESS, "DATA": null } } );
  }
};

