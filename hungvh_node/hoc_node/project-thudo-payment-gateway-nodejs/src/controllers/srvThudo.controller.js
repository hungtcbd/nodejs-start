const md5 = require( "md5" );

module.exports = {
  "vnPay": async ( req, res ) => {
    return res.status( 200 ).json( { "result": { "CODE": 0, "LINK": `https://api-lichcatnhat.gviet.vn/thudo/v1/log?amount=${req.query.amount}&channel=bank&code=200&method=bank&msg=thanhcong&orderId=${ req.query.orderId }&orderInfo=${req.query.orderInfo}&orderType=${req.query.orderType}&error=0&username=${req.query.username}&signature=${md5( req.query.username + process.env.API_GATEWAY_PREFIX + req.query.amount + process.env.API_GATEWAY_PREFIX + req.query.orderId + process.env.API_GATEWAY_PREFIX + process.env.API_GATEWAY_SECRET_KEY )}` } } );
  },
  "card": async ( req, res ) => {
    return res.status( 200 ).json( { "result": { "CODE": 0, "MESSAGE": "Thanh cong", "AMOUNT": req.query.amount } } );
  },
  "log": async ( req, res ) => {
    return res.status( 200 ).json( { "error_code": 0, "error_message": "Thành công" } );
  }
};
