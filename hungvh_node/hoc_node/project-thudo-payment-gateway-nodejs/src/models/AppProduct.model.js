/**
 * Create App Product Schema save info of app (product) to db ( mongodb )
 * author: hocpv
 * create_at: 25/10/2019
 * editor:
 * update_at:
 */

const mongoose = require( "mongoose" ),
  Schema = mongoose.Schema,

  AppProductSchema = new Schema( {
    "name": {
      "type": String,
      "unique": true
    },
    "appId": {
      "type": String,
      "unique": true
    },
    "describe": String,
    "secretKey": String,
    "_app": {
      "type": Schema.Types.ObjectId,
      "ref": "App"
    },
    "createdAt": {
      "type": Date,
      "default": Date.now()
    },
    "updatedAt": Date
  } );

AppProductSchema.pre( "save", function( next ) {
  this.updatedAt = Date.now();
  next();
} );

// eslint-disable-next-line one-var
const AppProduct = mongoose.model( "AppProduct", AppProductSchema );

module.exports = AppProduct;
