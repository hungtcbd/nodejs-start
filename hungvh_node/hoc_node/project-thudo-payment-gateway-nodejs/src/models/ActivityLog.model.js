/**
 * Create App Schema save info of app (product) to db ( mongodb )
 * author: hocpv
 * create_at: 07/08/2019
 * editor:
 * update_at:
 */

const mongoose = require( "mongoose" ),
  Schema = mongoose.Schema,

  ActivityLogSchema = new Schema( {
    "username": String,
    "request": Object,
    "response": Object,
    "createdAt": {
      "type": Date,
      "default": Date.now()
    },
    "updatedAt": Date
  } );

ActivityLogSchema.pre( "save", function( next ) {
  this.updatedAt = Date.now();
  next();
} );

// eslint-disable-next-line one-var
const ActivityLog = mongoose.model( "ActivityLog", ActivityLogSchema );

module.exports = ActivityLog;
