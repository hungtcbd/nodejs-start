/**
 * Create Payment Schema save info payment to db ( mongodb )
 * author: hocpv
 * create_at: 06/08/2019
 * editor:
 * update_at:
 */
const mongoose = require( "mongoose" ),
  Schema = mongoose.Schema,

  PaymentSchema = new Schema( {
    "requestId": {
      "type": String,
      "unique": true
    },
    "orderId": {
      "type": String,
      "unique": true
    },
    "orderInfo": {
      "type": String,
      "unique": true
    },
    "orderType": String, // "other": VNPay, "chargeCard": Card
    "amount": Number,
    "ip": String,
    "bankCode": String,
    "username": String,
    "signature": String,
    "serial": String,
    "cardCode": String,
    "channel": Number, // 1: 'VINA', 2: 'MOBI', 3: 'VTT', 4: 'VIETNAMMOBILE', 5: 'FPT', 6: 'MGC', 7: 'GATE', 8: 'VCOIN'
    "telco": String, // 'VINA' 'MOBI' 'VTT' 'VIETNAMMOBILE' 'FPT' 'MGC' 'GATE' 'VCOIN' 'GATE'
    "appId": String, // appId of App Product
    "status": {
      "type": Boolean,
      "default": false
    },
    "createdAt": {
      "type": Date,
      "default": Date.now()
    },
    "updatedAt": Date
  } );

PaymentSchema.pre( "save", function ( next ) {
  this.updatedAt = Date.now();
  next();
} );

// eslint-disable-next-line one-var
const Payment = mongoose.model( "Payment", PaymentSchema );

module.exports = Payment;
