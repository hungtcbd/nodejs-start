/**
 * Create Log Schema save log if err of payment to db ( mongodb )
 * author: hocpv
 * create_at: 06/08/2019
 * editor:
 * update_at:
 */

const mongoose = require( "mongoose" ),
  Schema = mongoose.Schema,

  LogSchema = new Schema( {
    "username": {
      "type": String,
      "unique": true
    },
    "logs": [ {
      "requestId": String,
      "status": String,
      "code": Number,
      "msg": String,
      "method": String, // vnPay = bank, thẻ cào = card
      "date": Date
    } ],
    "createdAt": {
      "type": Date,
      "default": Date.now()
    },
    "updatedAt": Date
  } );

LogSchema.pre( "save", function( next ) {
  this.updatedAt = Date.now();
  next();
} );

// eslint-disable-next-line one-var
const Log = mongoose.model( "Log", LogSchema );

module.exports = Log;
