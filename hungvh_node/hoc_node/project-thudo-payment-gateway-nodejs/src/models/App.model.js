/**
 * Create App Schema save info of app (product) to db ( mongodb )
 * author: hocpv
 * create_at: 07/08/2019
 * editor:
 * update_at:
 */

const mongoose = require( "mongoose" ),
  Schema = mongoose.Schema,

  AppSchema = new Schema( {
    "name": {
      "type": String,
      "unique": true
    },
    "appId": {
      "type": String,
      "unique": true
    },
    "describe": String,
    "secretKey": String,
    "createdAt": {
      "type": Date,
      "default": Date.now()
    },
    "updatedAt": Date
  } );

AppSchema.pre( "save", function( next ) {
  this.updatedAt = Date.now();
  next();
} );

// eslint-disable-next-line one-var
const App = mongoose.model( "App", AppSchema );

module.exports = App;
