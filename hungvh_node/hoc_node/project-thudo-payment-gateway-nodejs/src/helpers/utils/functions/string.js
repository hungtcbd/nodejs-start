/**
 * function handle string
 * author: hocpv
 * create_at: 08/08/2019
 * editor:
 * update_at:
 */
module.exports = {
  "randomString": ( num ) => {
    const character = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    let string = "", i = 0;

    while ( i < num ) {
      string += character.charAt( Math.floor( Math.random() * character.length ) );
      i++;
    }
    return string;
  },
  "randomNumberString": ( num ) => {
    const character = "0123456789";

    let string = "", i = 0;

    while ( i < num ) {
      string += character.charAt( Math.floor( Math.random() * character.length ) );
      i++;
    }
    return string;
  }

};
