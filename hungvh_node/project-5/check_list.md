# CMS

## Kiểm duyệt
### kiểm duyệt tin tức
- Filter: author, ngày đăng ( khoảng thời gian ), title, status, 
- Field: 
- Button:

### kiểm duyệt quảng cáo
- Filter:
- Field:
- Button:

### kiểm duyệt tuyển dụng
- Filter:
- Field:
- Button:

### kiểm duyệt hồ sơ
- Filter:
- Field:
- Button:

## Biên tập
### Biên tập màn hình trang chủ
- Filter:
- Button:

### Biên tập màn hình quảng cáo
- Filter:
- Button:

### Biên tập màn hình tuyển dụng
- Filter:
- Button: 

## Quản lý user
### Danh sách user
- Filter: username, status, ngày tạo, email, sđt, fullname, address,...
- Field: username, fullname, email, sđt, status, address, ngày tạo
- Button: filter, export

### Thêm mới user với mật khẩu mặc định
- Option: username, fullname, email, sđt, address

### Reset password
- Button: click -> button -> random password

### Sửa thông tin user
- Option: status, sđt, address, khu vực
- Button:

### Khóa/Mở user
- Option:
- Button:

### Lịch sử hoạt động user
- Filter:
- Field:
- Button:

## Quản lý đối tác
### Danh sách salon
- Filter:
- Field:
- Button:

### Thêm mới salon
- Option:
- Button:

### Sửa thông tin salon
- Option:
- Button:

### Khóa/Mở salon
- Button:

### Lịch sử salon
- Filter:
- Field: 
- Button:

### Danh sách đối tác quảng cáo
- Filter:
- Field:
- Button:

### Thêm mới đối tác quảng cáo
- Option:
- Button:

### Sửa thông tin đối tác quảng cáo
- Option:
- Button:

### Khóa/Mở đối tác quảng cáo
- Button:

## Quản lý voucher khuyến mãi
- Danh sách voucher khuyến mãi:
- Thêm mới voucher khuyến mãi:
- Sửa voucher khuyến mãi:
- Khóa/Mở voucher khuyến mãi:

## Tra cứu
### Tra cứu người dùng
- Tra cứu người dùng đăng nhập thành công:
    + Filter:
    + Field:
- Tra cứu người dùng đăng nhập không thành công:
    + Filter:
    + Field:
### Tra cứu đối tác
- Filter: 
- Field:
- Button:

### Tra cứu salon
- Filter:
- Field:
- Button:

### Tra cứu otp
- Field:
- Button:

### Tra cứu lịch sử hoạt động người dùng
- Filter:
- Field:
- Button: 

### Tra cứu lịch sử hoạt động salon
- Filter:
- Field:
- Button:

## Thống kê
### Thống kê chung
- Thống kê user:
- Thống kê salon:
- Thống kê doanh thu:
### Thống kê chi tiết user
- Thống kê CCU:
- Thống kê new User:
- Thống kê active User:
- Thống kê deactive User:
- Thống kê lượt tìm kiếm:

### Thống kê chi tiết salon
- Thống kê new salon:
- Thống kê active salon:
- Thống kê unsub salon:
- Thống kê renew salon:
- Thống kê type salon:

### Thống kê chi tiết doanh thu
- Filter:
- Biểu đồ:
    + Doanh thu theo ngày: biểu đồ cột
    + Doanh thu theo salon: biểu đồ tròn
    + Doanh thu theo đối tác ( quảng cáo): biểu đồ tròn
- Bảng biểu:
    + Doanh thu theo ngày
    + Doanh thu theo salon
    + Doanh thu theo đối tác( quảng cáo)