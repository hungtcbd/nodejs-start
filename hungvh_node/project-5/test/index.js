// During the test the env variable is set to test
process.env.NODE_ENV = "test";

const chai = require( "chai" );
const chaiHttp = require( "chai-http" );

// eslint-disable-next-line no-unused-vars
const serve = require( "../index" ),
    // eslint-disable-next-ligetne no-unused-vars
    should = chai.should();

chai.use( chaiHttp );

const users = require( "./user/user.test" );

users( chai, serve, should );