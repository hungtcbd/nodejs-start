// eslint-disable-next-line no-unused-vars
module.exports = ( chai, serve, should ) => {

    // App block test
    describe( "Search User Controller", () => {
        beforeEach( ( done ) => {
            // Before each test we empty the database in your case
            done();
        } );

        /*
        * Test the /POST search User Logged In
        */
        describe( "lấy danh sách users ", () => {
            it( "get success => status: 200", ( done ) => {
                let keyword = {
                    "userName": "Vu Van C",
                    "age": "21",
                    "perPage": "10",
                    "page": "1",
                    "isExport": "true",
                    "fromDate": "2019-12-01",
                    "toDate": "2019-12-09",
                    "groupName": "Admin",
                    "userId": "3"
                };

                // TODO add a model to db then get that id to take this test
                chai.request( serve )
                    .get( "/api/v1/users")
                    .send( keyword )
                    .end( ( err, res ) => {
                        should.not.exist( err );
                        res.should.have.status(200);
                        res.body.should.be.a("object");
                        res.body.DATA.should.be.a("object");
                        res.body.should.have.property("CODE").eql(0);
                        res.body.should.have.property("MESSAGE").eql("THÀNH CÔNG");
                        res.body.should.have.property("DATA");
                        res.body.DATA.users.should.be.a("array");//kiểm tra kiểu dữ liệu users
                        done();
                    });
            });
        });


        describe( "tạo user ", () => {
            it( "tạo thành công => status: 200", ( done ) => {
                let keyword = {
                    "userName": "Vu Van C",
                    "age": "21",
                    "email": "C.dev@.vn",
                    "groupId": "1",
                    "note": "like to m",
                    "groupName": "Admin",
                    "userId": "3"
                };

                // TODO add a model to db then get that id to take this test
                chai.request( serve )
                    .post( "/api/v1/users" )
                    .send( keyword )
                    .end( ( err, res ) => {
                        should.not.exist( err );
                        res.should.have.status(200);
                        res.body.should.be.a("object");
                        res.body.DATA.should.be.a("object");
                        res.body.should.have.property("CODE").eql(0);
                        res.body.should.have.property("MESSAGE").eql("THÀNH CÔNG");
                        res.body.should.have.property("DATA");
                        res.body.should.have.property("DATA").be.a("object");
                        done();
                    });
            });
        });
    });
};
