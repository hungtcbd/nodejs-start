module.exports = {
    /**
     *  return true: nếu ko phải số
     *  return false: là số
     */
    "checkNumber": (data) => {
        let value = data,

            re = /[a-z~!@#$%^&*()_+={}<>?"'|]/;

        if (!value) {
            return true;
        } else {
            if (re.test(value)) {
                return true;
            }
        }
        return false;
    }
};