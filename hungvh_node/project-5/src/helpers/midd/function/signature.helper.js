const md5 = require( "md5" );

class Signature {
  constructor( prefix = "$" ) {
    this.prefix = prefix;
  }

  async checkSignature( apiKey, signature ) {
    let checker;

    if ( !apiKey ) {
      return false;
    }
    checker = this.generateSignature( apiKey.apiKey, apiKey.userRequest );
    return checker === signature;

  }

  generateSignature( apiKey, userRequest ) {
    let signatureDataString = `${userRequest }${ this.prefix}${apiKey}`;

    return md5( signatureDataString );
  }

}

module.exports = new Signature( process.env.GATEWAY_PREFIX );
