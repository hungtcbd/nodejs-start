var mongoose = require('mongoose');

// mongoose.connect('mongodb://localhost/first_one');

var userSchema = mongoose.Schema({
    "userId": {
        "type": Number,
        "unique": true
    },
    "userName": String,
    "password": String,
    "email": String,
    "status": Number,
    "group": Object,
    "age": Number,
    "note": String,
    "create_at": Date,
    "update_at": Date
});
// userSchema.pre( "save", function( next ) {
//     this.update_at = Date.now();
//     next();
// } );
const users = mongoose.model( "users", userSchema );
module.exports = users;