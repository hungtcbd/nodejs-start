var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/first_one');

var newSchema = mongoose.Schema({
    "newId": {
        "type": Number,
        "unique": true
    },
    "newTitle": String,
    "status": Number,
    "newContentShort": String,
    "newContent": String,
    "author": {
        "type": mongoose.Schema.Types.ObjectId,
        "ref": "users"
    },
    "create_at": Date,
    "update_at": Date
});
// userSchema.pre( "save", function( next ) {
//     this.update_at = Date.now();
//     next();
// } );


const news = mongoose.model( "news", newSchema );
module.exports = news;

