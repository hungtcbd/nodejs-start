var mongoose = require('mongoose');
// mongoose.connect('mongodb://localhost/first_one');

var postSchema = mongoose.Schema( {
    "id": Number,
    "uuid": String,
    "language": String,
    "comment_status": String,
    "status": Number,
    "type": Number,
    "is_hot": Number,
    "show_top": Number,
    "categoryId": Number,
    "topicId": Number,
    "authorId": Number,
    "name": String,
    "slugs": String,
    "photo": String,
    "thumb": String,
    "photo_data": Object,
    "summary": String,
    "content": String,
    "title": String,
    "description": String,
    "tags": String,
    "source": String,
    "viewed": Object,
    "view_total": Number,
    "view_day": Number,
    "view_week": Number,
    "view_month": Number,
    "view_year": Number,
    "release_time": Date,
    "outdated_at": Date,
    "created_at": Date,
    "updated_at": Date
} );
// userSchema.pre( "save", function( next ) {
//     this.update_at = Date.now();
//     next();
// } );


const posts = mongoose.model( "data_news_version_2_posts", postSchema );
module.exports = posts;

