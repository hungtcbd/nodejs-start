// const Signature = require( "../helpers/utils/functions/signature.helper" );
// const RequestHelper = require( "../helpers/utils/functions/request.helper" );
const ErrorCode = require( "../configs/ErrorCode" );
const ErrorMessage = require( "../configs/ErrorMessage" );
const APIKeyModel = require( "../models/Auth/APIKey/ApiKeyModel" );


module.exports = async ( req, res, next ) => {
  let check = false,
      apiKeyModel = new APIKeyModel();

  if ( !req.userRequest || !req.signature ) {
    res.status( 200 ).json( {
      "CODE": ErrorCode.INVALID_SIGNATURE,
      "MESSAGE": ErrorMessage.MESSAGE_INVALID_SIGNATURE
    } );
    return;
  }
  userRequest = req.userRequest;
  signature = req.signature;

  apiKey = await apiKeyModel.getAPIKey( userRequest );
  if ( apiKey && apiKey.role === 1 ) {
    check = await Signature.checkSignature( apiKey, signature );
    if ( !check ) {
      response.status( 200 ).json( {
        "CODE": ErrorCode.INVALID_SIGNATURE,
        "MESSAGE": ErrorMessage.MESSAGE_INVALID_SIGNATURE
      } );
      return;
    }
    next();
    return;
  }
  if ( !check ) {
    response.status( 200 ).json( {
      "CODE": ErrorCode.INVALID_SIGNATURE,
      "MESSAGE": ErrorMessage.MESSAGE_INVALID_SIGNATURE
    } );
    return;
  }
  next( );
};
