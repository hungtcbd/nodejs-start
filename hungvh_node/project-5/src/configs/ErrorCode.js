module.exports = {
    "SUCCESS": 0, // THÀNH CÔNG
    "FAIL": 1, // THẤT BẠI
    "INVALID_PARAM": 2, // SAI HOẶC THIẾU THAM SỐ
    "INVALID_SIGNATURE": 3, // SAI CHỮ KÝ XÁC THỰC
    "QUERY_DB_FAIL": 4
};