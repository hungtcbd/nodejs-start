const router = require( "express-promise-router" )();
const UserController = require( "../../controllers/users/user.controller" );

router.route( "/" ).get( UserController.index );
router.route( "/" ).post( UserController.create );
router.route( "/:userId" ).put( UserController.update );
router.route( "/:userId" ).delete( UserController.delete );
router.route( "/password/:userId" ).put( UserController.resetPassword );
router.route( "/status/:userId" ).put( UserController.changeStatus );

module.exports = router;
