const router = require( "express-promise-router" )();
const NewController = require( "../../controllers/news/new.controller" );

router.route( "/" ).get( NewController.index );
router.route( "/users" ).get( NewController.joinUser );
router.route( "/:newId" ).put( NewController.update );
router.route( "/" ).post( NewController.create );


module.exports = router;
