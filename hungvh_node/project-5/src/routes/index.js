const router = require("express").Router();

router.use( "/users", require( "./user/user.route" ) );
router.use( "/news", require( "./new/new.route" ) );
router.use( "/posts", require( "./post/post.route" ) );

module.exports = router;