const router = require( "express-promise-router" )();
const PostController = require( "../../controllers/post/post.controller" );

// router.route( "/" ).get( PostController.index );
// router.route( "/users" ).get( NewController.joinUser );
// router.route( "/:newId" ).put( NewController.update );
// router.route( "/" ).post( NewController.create );
router.route( "/" ).get( PostController.newfeed );

module.exports = router;
