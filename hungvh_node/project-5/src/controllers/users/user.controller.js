const user = require( "../../models/user/user.model" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const isNumber = require( "../../helpers/product/function/hungvh.function" );
const md5 = require( "md5" );

module.exports = {
    "index": async ( req, res ) => {
        let dataUser = null,
            users = null,
            condition = {},
            page = 1,
            perPage = 10,
            isExport = "false",
            attributes = {
                userId: 1,
                userName: 1,
                password: 1,
                email: 1,
                status:1,
                group: 1,
                age: 1,
                note: 1,
                create_at: 1,
                update_at: 1
            };
        if ( req.query.perPage && req.query.page ) {
            if ( isNumber.checkNumber( req.query.page ) || isNumber.checkNumber( req.query.perPage ) ) {
                return res.status(200).json({"CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM});
            } else {
                perPage = parseInt(req.query.perPage);
                page = parseInt(req.query.page);
            }
        }
        if ( req.query.userId && req.query.userId.trim() !== "" ) {
            condition [ "userId" ] = req.query.userId;
        }
        if ( req.query.userName && req.query.userName.trim() !== "" ) {
            condition [ "userName" ] = req.query.userName;
        }
        if ( req.query.age ) {
            condition [ "age" ] = { $gte: parseInt( req.query.age ) };
        }
        if ( req.query.groupName && req.query.groupName.trim() !== "" ) {
            condition [ "group.groupName" ] = req.query.groupName;
        }
        if ( req.query.status && req.query.status.trim() !== "" ) {
            condition [ "status" ] = parseInt( req.query.status );
        }
        if ( req.query.fromDate && req.query.toDate ) {
            const fromDate = req.query.fromDate;
            const toDate = req.query.toDate;
            condition [ "create_at" ] = { $gt:new Date( fromDate ),$lte:new Date( toDate ) };
        }
        // if ( req.query.perPage && req.query.page ) {
        //     page = req.query.page;
        //     perPage = req.query.perPage;
        // }
        if ( req.query.isExport && req.query.isExport.trim() !== "" ) {
            isExport = req.query.isExport;
        }
        try {
            users = await user.find(condition);
            dataUser = await user.find(condition, attributes).limit(perPage).skip((page - 1) * perPage).sort({"create_at":-1});
        } catch (e) {
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        if (isExport === "true") {
            return res.status(200).json({
                "CODE": code.SUCCESS,
                "MESSAGE": msg.MESSAGE_SUCCESS,
                "DATA": {
                    "users": users,
                    "pagination": {
                        "total": users.length,
                        "perPage": users.length,
                        "page": 1
                    }
                }
            })
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "users": dataUser,
                "pagination": {
                    "total": users.length,
                    "perPage": perPage,
                    "page": page
                }
            }
        });
    },

    "create": async (req, res) => {
        let dataUser = null,
            condition = {};
            condition[ "create_at" ] = new Date();
            condition[ "update_at" ] = new Date();
            condition [ "password" ] = "abc123";
            condition [ "skills" ] = ["PHP","..."];
            condition [ "userId" ] = null;
            condition [ "userName" ] = null;
            condition [ "status" ] = 2,
            condition [ "email" ] = null;
            condition [ "group" ] = null;
            condition [ "age" ] = null;
            condition [ "note" ] = null;

        if ( req.query.userId && req.query.userId.trim() !== "" ) {
            condition [ "userId" ] = parseInt( req.query.userId );
        }
        if ( req.query.userName && req.query.userName.trim() !== "" ) {
            condition [ "userName" ] = req.query.userName;
        } 
        if ( req.query.email && req.query.email.trim() !== "" ) {
            condition [ "email" ] = req.query.email;
        }
        if ( req.query.groupId && req.query.groupId.trim() !== "" && req.query.groupName && req.query.groupName.trim() !== "") {
            condition [ "group" ] = {
                "groupId" : parseInt( req.query.groupId ),
                "groupName" : req.query.groupName
            }
        }
        if ( req.query.age && req.query.age.trim() !== "" ) {
            condition [ "age" ] = parseInt( req.query.age );
        }
        if ( req.query.note && req.query.note ) {
            condition [ "note" ] = req.query.note;
        }
        // if ( true ) {
        //     return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": condition});
        // }
        try {
            dataUser = await new user( condition );
            await  dataUser.save();
        } catch (e) {
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": e});
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "user": dataUser,
            }
        });
    },

    "update": async (req, res) => {
        let dataUser = null,
            users = null,
            condition = {};
        if ( req.query.userName && req.query.userName.trim() !== "" ) {
            condition [ "userName" ] = req.query.userName;
        }
        if ( req.query.groupId && req.query.groupName && req.query.groupId.trim() !== "" && req.query.groupName.trim() !== "" ) {
            condition [ "group" ] = {
                "groupId": req.query.groupId,
                "groupName": req.query.groupName
            };
        }
        if ( req.query.age ) {
            condition [ "age" ] = req.query.age;
        }
        if ( req.query.email && req.query.email.trim() !== "" ) {
            condition [ "email" ] = req.query.email;
        }
        if ( req.query.skills ) {
            condition [ "skills" ] = [ req.query.skills ];
        }
        condition [ "update_at" ] = new Date();
        // if ( true ) {
        //     return res.status(200).json({"id": req.params.userId, "set": condition });
        // }
        if ( req.params.userId && req.params.userId.length != 24 ) {
            return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
        }
        try {
            users = await user.findOne({ "_id": req.params.userId });
            if ( users == null ) {
                return res.status(200).json({
                    "CODE": code.SUCCESS,
                    "MESSAGE": msg.MESSAGE_SUCCESS,
                    "NOTE": "KHÔNG TÌM THẤY USER"
                });
            }
            dataUser = await user.updateOne( { "_id": req.params.userId }, { "$set": condition }, { "upsert": true } );

            users = await user.findOne({ "_id": req.params.userId });
        } catch (e) {
            // return res.status(200).json({"id": req.params.userId, "set": condition });
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": users
        });
    },

    "delete": async (req, res) => {
        let dataUser = null,
            users = null;
        if ( req.params.userId && req.params.userId.length != 24 ) {
            return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
        }
        try {
            users = await user.findOne({ "_id": req.params.userId });
            if ( users == null ) {
                return res.status(200).json({
                    "CODE": code.SUCCESS,
                    "MESSAGE": msg.MESSAGE_SUCCESS,
                    "NOTE": "KHÔNG TÌM THẤY USER"
                });
            }
            dataUser = await user.deleteOne( { "_id": req.params.userId } );

        } catch (e) {
            // return res.status(200).json({"id": req.params.userId, "set": condition });
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
        });
    },

    "changeStatus": async (req, res) => {
            let dataUser = null,
                users = null,
                condition = {};
            if ( req.query.status && req.query.status.trim() !== "" ) {
                condition [ "status" ] = parseInt( req.query.status );
            }
            condition [ "update_at" ] = new Date();
            // if ( true ) {
            //     return res.status(200).json({"id": req.params.userId, "set": condition });
            // }
        // return res.status( 200 ).json( { "CODE": req.params.userId, "MESSAGE": condition } );
            if ( req.params.userId && req.params.userId.length != 24 ) {
                return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
            }
            try {
                users = await user.findOne({ "_id": req.params.userId });
                if ( users == null ) {
                    return res.status(200).json({
                        "CODE": code.SUCCESS,
                        "MESSAGE": msg.MESSAGE_SUCCESS,
                        "NOTE": "KHÔNG TÌM THẤY USER"
                    });
                }
                dataUser = await user.updateOne( { "_id": req.params.userId }, { "$set": condition }, { "upsert": true } );

                users = await user.findOne({ "_id": req.params.userId });
            } catch (e) {
                // return res.status(200).json({"id": req.params.userId, "set": condition });
                // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
                return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
            }
            res.status(200).json({
                "CODE": code.SUCCESS,
                "MESSAGE": msg.MESSAGE_SUCCESS,
                "DATA": users
            });
    },

    "resetPassword": async (req, res) => {
        let dataUser = null,
            users = null,
            condition = {};
        const passNew = Math.random().toString(36).substring(2, 10);
        condition [ "password" ] = md5(passNew);
        condition [ "update_at" ] = new Date();
        // if ( true ) {
        //     return res.status(200).json({"id": req.params.userId, "set": condition });
        // }
        // return res.status( 200 ).json( { "pass": condition, "id": passNew } );
        if ( req.params.userId && req.params.userId.length != 24 ) {
            return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
        }
        try {
            users = await user.findOne({ "_id": req.params.userId });
            if ( users == null ) {
                return res.status(200).json({
                    "CODE": code.SUCCESS,
                    "MESSAGE": msg.MESSAGE_SUCCESS,
                    "NOTE": "KHÔNG TÌM THẤY USER"
                });
            }
            dataUser = await user.updateOne( { "_id": req.params.userId }, { "$set": condition }, { "upsert": true } );

            users = await user.findOne({ "_id": req.params.userId });
        } catch (e) {
            // return res.status(200).json({"id": req.params.userId, "set": condition });
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "password new": passNew
            }
        });
    }
};