const newPaper = require( "../../models/new/new.moel" );
const users = require( "../../models/user/user.model" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
const isNumber = require( "../../helpers/product/function/hungvh.function" );

module.exports = {
    "index":  async (req, res) => {
        let dataNews = null,
            condition = {},
            news = null,
            page = 1,
            perPage = 10,
            isExport = "false",
            attributes = {
                newId: 1,
                newTitle: 1,
                newContentShort: 1,
                newContent: 1,
                userId: 1,
                create_at: 1,
                update_at: 1
            };
        if ( req.query.perPage && req.query.page ) {
            if ( isNumber.checkNumber( req.query.page ) || isNumber.checkNumber( req.query.perPage ) ) {
                return res.status(200).json({"CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM});
            } else {
                perPage = parseInt(req.query.perPage);
                page = parseInt(req.query.page);
            }
        }
        if ( req.query.newId && req.query.newId.trim() !== "" ) {
            condition [ "newId" ] = req.query.newId;
        }
        try {
            news = await newPaper.find(condition);
            dataNews = await newPaper.find(condition, attributes).limit(perPage).skip((page - 1) * perPage).sort({"create_at":-1}).sort("newId");
        } catch (e) {
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL});
        }
        if (isExport === "true") {
            return res.status(200).json({
                "CODE": code.SUCCESS,
                "MESSAGE": msg.MESSAGE_SUCCESS,
                "DATA": {
                    "users": news,
                    "pagination": {
                        "total": news.length,
                        "perPage": news.length,
                        "page": 1
                    }
                }
            })
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "user": dataNews,
                "pagination": {
                    "total": news.length,
                    "perPage": perPage,
                    "page": page
                }
            }
        });
    },

    "joinUser": async  ( req, res ) => {
        let datanew = null,
            data = null,
            condition = {},
            new_author = {},
            attributes = {
                [ "newId" ]: 1,
                [ "newTitle" ]: 1,
                [ "newContentShort" ]: 1,
                [ "newContent" ]: 1,
                [ "create_at" ]: 1,
                [ "update_at" ]: 1

            };
        if ( req.query.newId && req.query.newId.trim() !== "" ) {
            condition [ "newId" ] = parseInt( req.query.newId );
        }
        try {
            // users = await user.find(condition);
            datanew = await newPaper.findOne(condition).populate( "author" );

            // data = datanew.newTitle;
        } catch (e) {
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status(200).json({"CODE": code.QUERY_DB_FAIL, "MESSAGE": e});
        }
        try {
            console.log( datanew )
            new_author.newId = datanew.newId;
            new_author.newTitle = datanew.newTitle;
            new_author.newContent = datanew.newContent;
            new_author.create_at = datanew.create_at;
            new_author [ "author.userId" ] = datanew [ "author" ] [ "userId" ];
            new_author [ "author.userName" ] = datanew [ "author" ] [ "userName" ];
        } catch (e) {
            return res.status( 200 ).json( { "CODE":code.FAIL, "MESSAGE": "DATA NULL" } );
        }

        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "news": new_author,
                "pagination": {
                    "total": datanew.length,
                    "perPage": 10,
                    "page": 1
                }
            }
        });
    },

    "update": async ( req, res ) => {
        let dataNew = null,
            news = null,
            condition = {};

            condition [ "update_at"] = new Date();
            if ( req.body.title && req.body.title.trim() !== "" ) {
                condition [ "newTitle" ] = req.body.title;
            }
            if ( req.body.content && req.body.content.trim() !== "" ) {
                condition [ "newContent" ] = req.body.content;
            }
            if ( req.body.author && req.body.author.trim() !== "" ) {
                if (req.body.author.length != 24) {
                    return res.status( 200 ).json( { "CODE":code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
                } else {
                    condition [ "author" ] = req.body.author;
                }
            }

            try {
                news = await newPaper.findOne( { "_id": req.params.newId } );

                console.log( news );
                if ( news == null ) {
                    return res.status(200).json({
                        "CODE": code.SUCCESS,
                        "MESSAGE": msg.MESSAGE_SUCCESS,
                        "NOTE": "KHÔNG TÌM THẤY newss"
                    });
                }
                dataNew = await newPaper.updateOne( { "_id": req.params.newId }, { "$set": condition }, { "upsert": true } );

                news = await newPaper.findOne({ "_id": req.params.newId });
            } catch (e) {
                return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
            }

        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": news
        });
    },

    "create": async ( req, res ) => {
        let condition = {},
            datanew = null;
        condition [ "newId" ] = "100";
        // condition [ "newTitle" ] = "ABC";
        // condition [ "newContentShort" ] = "U22 Việt Nam: Tinh thần thép phát huy, quyết đoạt vé chung kết";
        // condition [ "newContent" ] = "U22 Việt Nam: Tinh thần thép phát huy, quyết đoạt vé chung kết\n U22 Việt Nam dù đã tiến vào bán kết môn bóng đá nam SEA Games 30 và đang tràn trề cơ hội vào chung kết để tiếp tục giấc mơ đoạt HCV nhưng quả thật, hành trình vừa qua ở vòng bảng của chúng ta không hề dễ dàng.";
        condition [ "create_at" ] =  new Date();
        condition [ "update_at" ] = new Date();
        // condition [ "author" ] = ObjectId("5de8d16f39268eaca834ee39");
        condition [ "status" ] = NumberInt(2);

        if ( !req.body.title || !req.body.contentShort || !req.body.content || !req.body.author ) {
            return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
        }
        if ( req.body.title && req.body.title.trim() !== "" ) {
            condition [ "newTitle" ] = req.body.title;
        }
        if ( req.body.contentShort && req.body.contentShort.trim() !== "" ) {
            condition [ "newContentShort" ] = req.body.contentShort;
        }
        if ( req.body.content && req.body.content.trim() !== "" ) {
            condition [ "newContent" ] = req.body.content;
        }
        if ( req.body.author && req.body.author.trim() !== "" ) {
            condition [ "author" ] = ObjectId( req.body.author );
        }

        try {
            datanew = await new newPaper( condition );
            await datanew.save();

        } catch (e) {
            return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
        }
        res.status(200).json({
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "user": datanew,
            }
        });
    }
};