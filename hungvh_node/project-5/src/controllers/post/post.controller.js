const PostNews = require( "../../models/post/post.model" );
// const users = require( "../../models/user/user.model" );
const code = require( "../../configs/ErrorCode" );
const msg = require( "../../configs/ErrorMessage" );
// const isNumber = require( "../../helpers/product/function/hungvh.function" );

module.exports = {
    // "index" : async ( req, res ) => {
    //     let dataNews = null,
    //         condition = {},
    //         news = null,
    //         page = 1,
    //         perPage = 10,
    //         isExport = "false",
    //         attributes = {
    //             newId: 1,
    //             newTitle: 1,
    //             newContentShort: 1,
    //             newContent: 1,
    //             userId: 1,
    //             create_at: 1,
    //             update_at: 1
    //         };
    //     if ( req.query.perPage && req.query.page ) {
    //         if ( isNumber.checkNumber( req.query.page ) || isNumber.checkNumber( req.query.perPage ) ) {
    //             return res.status( 200 ).json( { "CODE": code.INVALID_PARAM, "MESSAGE": msg.MESSAGE_INVALID_PARAM } );
    //         } else {
    //             perPage = parseInt( req.query.perPage );
    //             page = parseInt( req.query.page );
    //         }
    //     }
    //     if ( req.query.newId && req.query.newId.trim() !== "" ) {
    //         condition [ "newId" ] = req.query.newId;
    //     }
    //     try {
    //         news = await newPaper.find( condition );
    //         dataNews = await newPaper.find( condition, attributes ).limit( perPage ).skip( ( page - 1 ) * perPage ).sort( { "create_at":-1 } ).sort( "newId" );
    //     } catch ( e ) {
    //         // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
    //         return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
    //     }
    //     if ( isExport === "true" ) {
    //         return res.status( 200 ).json( {
    //             "CODE": code.SUCCESS,
    //             "MESSAGE": msg.MESSAGE_SUCCESS,
    //             "DATA": {
    //                 "users": news,
    //                 "pagination": {
    //                     "total": news.length,
    //                     "perPage": news.length,
    //                     "page": 1
    //                 }
    //             }
    //         } )
    //     }
    //     res.status( 200 ).json( {
    //         "CODE": code.SUCCESS,
    //         "MESSAGE": msg.MESSAGE_SUCCESS,
    //         "DATA": {
    //             "user": dataNews,
    //             "pagination": {
    //                 "total": news.length,
    //                 "perPage": perPage,
    //                 "page": page
    //             }
    //         }
    //     } );
    // },w

    "newfeed": async ( req, res ) => {
        let dataPosts = null,
            condition = {},
            posts = null,
            page = Math.floor(Math.random() * 10 ),
            perPage = 10,
            attributes = {
                id: 1,
                uuid: 1,
                status: 1,
                type: 1,
                is_hot: 1,
                show_top: 1,
                categoryId: 1,
                authorId: 1,
                name: 1,
                slugs: 1,
                photo: 1,
                thumb: 1,
                photo_data: 1,
                summary: 1,
                content: 1,
                title: 1,
                description: 1,
                tags: 1,
                source: 1,
                viewed: 1,
                created_at: 1,
                updated_at: 1

            };
        try {
            // posts = await PostNews.find( condition );
            dataPosts = await PostNews.find( condition ).limit( perPage ).skip( ( page - 1 ) * perPage );/*.sort( { "created_at":1 } ).sort( "id" )*/
        } catch ( e ) {
            // logger.log("Có vấn đề khi truy vấn tới database!!", {"INFO": req.body, "ERROR": e.original});
            return res.status( 200 ).json( { "CODE": code.QUERY_DB_FAIL, "MESSAGE": msg.MESSAGE_QUERY_DB_FAIL } );
        }

        res.status( 200 ).json( {
            "CODE": code.SUCCESS,
            "MESSAGE": msg.MESSAGE_SUCCESS,
            "DATA": {
                "post": dataPosts,
                "pagination": {
                    // "total": posts.length,
                    "perPage": perPage,
                    "page": page
                }
            }
        } );
    },
};