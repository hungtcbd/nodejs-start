//// -- LEVEL 1
//// -- Tables and References

// Creating tables

Table Roles  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  level string [ note: "superadmin, admin, cp, member"]
  _listPermission array [ ref: < Permissions._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Users as U  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  fullName string
  userName string
  email string
  password string
  phone string
  birthday datetime
  avatar string
  _role ObjectId [ref: > Roles._id,note: ""]
  status int
  address string
  loginType string
  userType string
  _province ObjectId [ref: - Provinces._id]
  _district ObjectId [ref: - Districts._id]
  _ward ObjectId [ref: - Wards._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table UserOtps [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  phoneNumber string
  otpCode string
  expireTime datetime
  _user ObjectId [ref: > Users._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Salons  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  name string
  coverImage string
  avatar string
  detail object [note: "show detail, certificate..."]
  workTime object [note: "open hour, close hour"]
  address string
  view int
  locations array [note: "array object name address, lat, long"]
  _province ObjectId [ref: < Provinces._id]
  _district ObjectId [ref: < Districts._id]
  _ward ObjectId [ref: < Wards._id]
  _userSub ObjectId [ref: > Users._id, note: "owner of salon sub for their salon"]
  status boolean
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Recruitments  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  position string
  detail string
  images string
  num int [note:"number of recruitments"] 
  _salon ObjectId [ref: > Salons._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
 }
 
Table News  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  title string
  name string
  slugs string
  images string
  thumb string
  photoData string [note: "Data ảnh sau khi resize'"]
  summary string
  content string
  description string
  source string
  like int
  comment array
  commentStatus string [note: "default 'open', Trạng thái cho phép comment"]
  status int [note: "default 1 = active"]
  type int [ note: "1 = tin bài, 2 = tin ảnh, 3 = tin video, 4 = infographic, 5 = timeline" ]
  isHot int [note: "0 = Normal, 1 = HOT"]
  showTop int [note: "0 = Không hiển thị trên Top, 1 = Hiển thị trên Top tin"]
  tags string
  viewTotal int
  viewDay int
  viewWeek int
  viewMonth int
  viewYear int
  _author ObjectId [ref: > Users._id]
  _editor ObjectId [ref: > Users._id]
  _category ObjectId [ref: > NewCategories._id]
  releaseTime datetime [note: "Thời gian xuất bản"]
  outdatedAt datetime [note: "Thời gian hết hạn bản tin"]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table NewMetas  [headercolor: #27ae60] {
  _id ObjectIdbjectId [pk,note: "mongodb auto generate"]
  metaKey string
  metaValue string
  metaType int[note: "0: string, 1: number, 2: json"]
  description string
  _newId ObjectId [ref: > News._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table NewCategories  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  name string
  images string
  _author ObjectId [ref: > Users._id]
  _editor ObjectId [ref: > Users._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Provinces  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  code int
  name string
  createdAt datetime [default: `now()`]
  updatedAt datetime
 }

Table Districts  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  code int
  name string
  createdAt datetime [default: `now()`]
  updatedAt datetime
 }
 
 Table Wards  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  code int
  name string
  createdAt datetime [default: `now()`]
  updatedAt datetime
 }
 
Table Albums  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  name string
  slugs string
  detail object
  _salon ObjectId [ref: > Salons._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Services  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  name string
  detail string
  slugs string
  images string
  price int
  salePrice int
  _salon ObjectId [ref: > Salons._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Reviews  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  ratingStar int
  _user ObjectId [ref: > Users._id]
  _salon ObjectId [ref: > Salons._id]
  status boolean
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Messages  [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  contents array [note: "type: text, image"]
  order object
  _sender ObjectId [ref: > Users._id]
  _receiver ObjectId [ref: > Salons._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table LogRequests  [headercolor: #27ae60]{
  _id ObjectId [pk,note: "mongodb auto generate"]
  action array
  header object
  response object
  _user ObjectId [ref: > Users._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table LogActions  [headercolor: #27ae60]{
  _id ObjectId [pk,note: "mongodb auto generate"]
  keySearch array
  transaction object
  login object
  _user ObjectId [ref: > Users._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Permissions [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  name name
  type int
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table UserPermission [headercolor: #27ae60] {
  _id ObjectId [pk,note: "mongodb auto generate"]
  _user ObjectId [ref: - Users._id]
  _permission array [ref: - Permissions._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Statistics  [headercolor: #27ae60] {
  _id ObjectId
  activeUser int
  activeSalon int
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Devices  [headercolor: #27ae60] {
  _id ObjectId
  token string
  os string
  ip string
  _user ObjectId [ref: > Users._id]
  createdAt datetime [default: `now()`]
  updatedAt datetime
}

Table Trackings  [headercolor: #27ae60] {
  _id ObjectId
  currentUser int
  _device ObjectId [ref: > Devices._id]
  data object
  createdAt datetime [default: `now()`]
  updatedAt datetime
}
 
 
// Creating references
// You can also define relaionship separately
// > many-to-one; < one-to-many; - one-to-one

//---------------------------------------------
